-- @component SoundSystem

systemOn = 0

function dyn_res_cons()
  return 2*systemOn
end

-- SoundSystem.on(x)
function on_time(x)
  return 0
end

function on_resource(x)
  return 0
end

function on_definition(x)
  systemOn = 1
  return 1
end

-- SoundSystem.off(x)
function off_time(x)
  return 0
end

function off_resource(x)
  return 0
end

function off_definition()
  systemOn = 0
  return 0
end

-- SoundSystem.playbeep(x)
function playBeep_time(x)
  return 5
end

function playBeep_resource(x)
  return 15
end

function playBeep_definition(x)
  if systemOn then
    x = 1 
  else
    x = 0
  end
  return x
end

-- SoundSystem.sleep(x)
function sleep_time(x)
  return 50
end

function sleep_resource(x)
  return 0
end

function sleep_definition(x)
  return 0
end
#ifndef VALUEBOOL_H
#define VALUEBOOL_H

#include "Value.h"

class ValueBool : public Value
{
	public:
		ValueBool(bool value);
		bool getValue() const;
		virtual ~ValueBool();
	private:
		bool value;
};

#endif //VALUEBOOL_H

#ifndef RULESKIP_H
#define RULESKIP_H

#include "Rule.h"

class RuleSkip : public Rule
{
    public:
        RuleSkip(const AST* ast);
        EvaluationResult evaluateRule(State&, FunctionEnvironment&, map<string, Component*>&) override;
        ~RuleSkip() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
};

#endif // RULESKIP_H

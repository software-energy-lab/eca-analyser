#ifndef FUNCTION_H
#define FUNCTION_H

#include "Rule.h"
#include "EvaluationResult.h"

class Function
{
	public:
		Function(string name, string argumentName, Rule* definition);
		EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components);
        string getArgumentName();
		string getName();
        virtual ~Function();
	private:
		string name;
        string argumentName;
        Rule* definition;
        

};

#endif //FUNCTION_H

#ifndef RULE_H
#define RULE_H

#include <exception>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <string>
#include <vector>
#include "Component.h"
#include "Enums.h"
#include "EvaluationResult.h"
#include "State.h"
#include "ValueBool.h"
#include "ValueFloat.h"
#include "ValueInt.h"
#include "ValueString.h"
//#include "FunctionEnvironment.h"

using namespace std;

class FunctionEnvironment;
class AST;

/**
 * @brief The Rule class is the base class for the data structurs for analysis of an AST node.
 * The Rule classes are separated from the AST classes to keep a clear separation between
 * syntax and analysis/semantics.
 */
class Rule
{
    public:
        virtual ~Rule();
        //Get the original AST object
        const AST* getAST();
        string getASTString(ECAOrLua lang);

        /**
         * evaluateRule calculates the result of evaluating a function on a specific program state, function environment
         * and set of component states. The argument programState is updated in place, so the program state that is
         * the result of evaluating the rule will be stored in the argument programState after evaluateFunction returns.
         * The same holds for the function environment and the component states.
         *
         * @param programState a reference to the initial program state
         * @param env a reference to the initial environment
         * @param components a reference to a mapping from component names to components
         * @return the result of evaluating this rule
         */
        virtual EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) = 0;

    protected:
         // The td_ec function returns the Time Dependent Energy Consumption of the model during
         // the time specified in the argument.
        ResourceAmount td_ec(map<string, Component*>& components, Time t);

		// Gets the subrules that this rule contains
		virtual vector<Rule*>* getChildren() = 0;
		//The original AST object
		const AST* ast;
    private:
};

#endif // RULE_H

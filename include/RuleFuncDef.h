#ifndef RULEFUNCDEF_H
#define RULEFUNCDEF_H

#include "Rule.h"


class RuleFuncDef : public Rule
{
    public:
        RuleFuncDef(string functionname, string argumentname, Rule* body, Rule* nextStatement, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleFuncDef() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        string functionname;
        string argumentname;
        Rule* body;
        Rule* nextStatement;
};

#endif // RULEFUNCDEF_H

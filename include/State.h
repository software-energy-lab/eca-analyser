#ifndef PROGRAMSTATE_H
#define PROGRAMSTATE_H

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "Value.h"

using namespace std;

/**
 * @brief The State class contains mappings from variable names to values. It keeps track of a global scope
 * and a stack of local states. When entering a local scope, a new state is pushed to the stack. When exiting
 * a local scope, the upper local state is popped from the stack.
 *
 * Note: naming this class ProgramState could cause confusion, since the ECA components use this class for their internal state as well.
 * In the literature, a strict distinction is made between a program state and a component state. Naming the variables programState
 * and componentState is a better alternative.
 */
class State
{
	public:
		State();

		// Initializes the global state
		State(map<string, Value*> localState);

		// Returns the value for a variable name. lookup starts with searching in the current scope,
		// then one scope higher, etc.
		Value* lookup(string name);

		// Declares the value for a global variable, possibly overwriting an existing global variable
		void declareGlobalVar(string name, Value* value);

		// Declares the value for a local variable in the current local scope, possibly overwriting an existing local variable
		void declareLocalVar(string name, Value* value);

		// Updates the value for a given variable if it exists in the local state.
		// Returns true iff the variable exists in the local state
		bool localAssign(string name, Value* value);

		// Does the variable exists in the local or global state?
		bool exists(string name);

		// Create a new local scope
		void enterLocalScope();
		// Remove the local scope that is most recently created
		void exitLocalScope();
		// empty the stack of local states
		void emptyLocalScopes();
		~State();

		vector<map<string, Value *> > getLocalState() const;
		void setLocalState(const vector<map<string, Value *> > &value);

	protected:
	private:
		//contains variable -> value mappings
		vector<map<string, Value*>> localState; // The stack of local states
		map<string, Value*> globalState;
};

#endif // PROGRAMSTATE_H

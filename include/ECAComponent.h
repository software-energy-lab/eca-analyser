#ifndef ECACOMPONENT_H
#define ECACOMPONENT_H

#include <map>
#include "Component.h"
#include "ECAComponentFunction.h"

using namespace std;

class ECAComponent : public Component
{
public:
    ECAComponent(string compName, State cState, map<string, ECAComponentFunction*> functions, Rule* dyn_res_cons);
    EvaluationResult evaluateFunction(string funcName, Value* arg) override;
    DynResCons dynResCons() override;
    unordered_set<string> getComponentFunctionNames() override;
private:
    State cState;
    map<string, ECAComponentFunction*> functions;
    Rule* dyn_res_cons;
};

#endif // ECACOMPONENT_H

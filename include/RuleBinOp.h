#ifndef RULEBINOP_H
#define RULEBINOP_H

#include "Rule.h"


class RuleBinOp : public Rule
{
    public:
        RuleBinOp(string binop, Rule* left, Rule* right, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleBinOp() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Value* operate(Value* l, Value* r);
        string binop;
        Rule* left;
        Rule* right;
};

#endif // RULEBINOP_H

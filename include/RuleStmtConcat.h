#ifndef RULESTMTCONCAT_H
#define RULESTMTCONCAT_H

#include "Rule.h"

class RuleStmtConcat : public Rule
{
    public:
        RuleStmtConcat(Rule* left, Rule* right, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleStmtConcat() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Rule* left;
        Rule* right;
};

#endif // RULESTMTCONCAT_H

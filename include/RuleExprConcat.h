#ifndef RULEEXPRCONCAT_H
#define RULEEXPRCONCAT_H

#include "Rule.h"

class RuleExprConcat : public Rule
{
    public:
        RuleExprConcat(Rule* left, Rule* right, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleExprConcat() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Rule* left;
        Rule* right;
};

#endif // RULEEXPRCONCAT_H

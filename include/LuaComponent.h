#ifndef LUACOMPONENT_H
#define LUACOMPONENT_H

#include "Component.h"
#include "lua.hpp"

class LuaComponent : public Component
{
public:
    enum Mode {file, code};
    // When given a file name as argument lua, LuaComponent gives better error messages. A string with the code is more usefull for testing.
    LuaComponent(string compName, string lua, Mode mode);

    EvaluationResult evaluateFunction(string funcName, Value* arg) override;
    DynResCons dynResCons() override;
    unordered_set<string> getComponentFunctionNames() override;
    ~LuaComponent() override;
private:
    lua_State* model; // The state of the component
};

#endif // LUACOMPONENT_H

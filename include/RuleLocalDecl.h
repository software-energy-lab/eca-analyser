#ifndef RULELOCALDECL_H
#define RULELOCALDECL_H

#include <Rule.h>


class RuleLocalDecl : public Rule
{
    public:
        RuleLocalDecl(string variable, Rule* expression, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleLocalDecl() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        string variable;
        Rule* expr;
};

#endif // RULELOCALDECL_H

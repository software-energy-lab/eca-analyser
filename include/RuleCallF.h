#ifndef RULECALLF_H
#define RULECALLF_H

#include <Rule.h>


class RuleCallF : public Rule
{
    public:
        RuleCallF(string functionname, Rule* arg, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleCallF() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        string functionName;
        Rule* arg;
};

#endif // RULECALLF_H

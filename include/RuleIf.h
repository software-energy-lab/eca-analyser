#ifndef RULEIF_H
#define RULEIF_H

#include "Rule.h"

class RuleIf : public Rule
{
    public:
        RuleIf(Rule* c, Rule* t, Rule* e, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleIf() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        Rule* c;
        Rule* t;
        Rule* e;
};

#endif // RULEIF_H

#ifndef EVALUATIONRESULT_H
#define EVALUATIONRESULT_H

#include "State.h"

using Time = uint32_t;               // Time in nanoseconds
using ResourceAmount = uint32_t;     // The resource being consumed in a user-defined quantity and unity
using DynResCons = uint32_t;         // Dynamic resource consumption. Quantity: resource/time; Unity: resource/nanosecond

class EvaluationResult
{
public:
    /**
     * @brief EvaluationResult stores the result of the evaluation of an AST node.
     * @param value The value that the code evaluates to, in case of
     *              an expression. Null pointer for statements.
     * @param time The time it took to execute the AST node.
     * @param resource The amount of resource it took to execute the AST node.
     * @param isRetVal true iff a return statement has been executed.
     *              The returned value will be stored in the 'value' member of the
     *              EvaluationResult object.
     */
    EvaluationResult(Value* value, Time time, ResourceAmount resource, bool isRetVal);

    Value *getValue() const;
    bool getIsRetVal();
    Time getTime() const;
    ResourceAmount getResourceAmount() const;

private:
    Value* value;
    bool isRetVal;
    Time time;
    ResourceAmount resource;
};

//Macros for increased readability
#define ValueResult(val, time, resource) EvaluationResult(val, time, resource, false)
#define ReturnResult(val, time, resource) EvaluationResult(val, time, resource, true)

#endif // EVALUATIONRESULT_H

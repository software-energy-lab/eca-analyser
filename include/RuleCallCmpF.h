#ifndef RULECALLCMPF_H
#define RULECALLCMPF_H

#include "Rule.h"

class RuleCallCmpF : public Rule
{
    public:
        RuleCallCmpF(string componentName, string functionName, Rule* arg, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleCallCmpF() override;
    protected:
        vector<Rule*>* getChildren() override;
    private:
        string componentName;
        string functionName;
        Rule* arg;
};

#endif // RULECALLCMPF_H

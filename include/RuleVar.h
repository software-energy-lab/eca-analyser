#ifndef RULEVAR_H
#define RULEVAR_H

#include "Rule.h"

class RuleVar : public Rule
{
    public:
        RuleVar(string name, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleVar() override;
    protected:
        vector<Rule*>* getChildren() override;
	private:
		string name;
};

#endif // RULEVAR_H

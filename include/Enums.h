#ifndef ENUMS_H
#define ENUMS_H

enum ECAOrLua {ECA, Lua};
enum class Type {Bool, Float, Int, String};

#endif // ENUMS_H

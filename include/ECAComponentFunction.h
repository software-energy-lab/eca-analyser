#ifndef ECACOMPONENTFUNCTION_H
#define ECACOMPONENTFUNCTION_H

#include "Rule.h"

/**
 * @brief The ECAComponentFunction class represents a function of a component
 * defined in a .eca component model file
 */
class ECAComponentFunction
{
    public:
        ECAComponentFunction(string compName, string funcName, string argName, Rule* definition,
        Rule* resourceFunc, Time time);

        /// \param cState the component state here
        EvaluationResult evaluateFunction(State& cState, FunctionEnvironment& env, map<string, Component*>& components);

        string getCompName() const;
        string getFuncName() const;
        string getArgName() const;

        ~ECAComponentFunction();
private:
        string compName, funcName, argName;
        Rule* definition;
        Rule* resourceFunc;
        Time time;
    protected:
         
};

#endif // ECACOMPONENTFUNCTION_H

#ifndef PARSE_H
#define PARSE_H

#include <iostream>
#include <map>
#include <sstream>
#include "memorypool.h"
#include "simplestring.h"
#include "memory.h"
#include "Enums.h"
#include "State.h"
#include "ValueBool.h"
#include "ValueFloat.h"
#include "ValueInt.h"
#include "ValueString.h"
//#include "Rule.h"
class Rule;

using namespace std;
using namespace bitpowder::lib;

class AST {
public:
    int refcount = 0;
    typedef bitpowder::lib::shared_object<AST> Ref;
    virtual ~AST() {
    }

    /* We have two sorts of assignment: Local declaration and 'normal' assignment. For ECA, we need to decide which of the two to use after the parsing step.
     * deriveAssignmentSorts does exactly this. It keeps track of the current scope using the State provided in its argument*/
    virtual void derive_assignment_sorts(State& state) = 0;
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const = 0;
    string toString(ECAOrLua lang) const {
        ostringstream stream;
        print(stream, lang);
        return stream.str();
    }
};

class ASTStatement : public AST {
public:
    typedef bitpowder::lib::shared_object<ASTStatement, AST> Ref;
    virtual bool isExpression() const {
        return false;
    }
    virtual Rule* getTypeRule() const = 0;
};

class ASTExpression : public ASTStatement {
public:
    typedef bitpowder::lib::shared_object<ASTExpression, AST> Ref;
    virtual bool isExpression() const override {
        return true;
    }
};

class ASTSkip : public ASTStatement {
public:
    ASTSkip() {
    }
    virtual void derive_assignment_sorts(State&) override {}
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(lang == ECA)
            out << "skip";
        else if (lang == Lua)
            out << ";";
    }
    Rule* getTypeRule() const override;
};

class ASTConstant : public ASTExpression {
    Value* value;
public:
    ASTConstant(Value* value) : value(value) {
    }
    virtual void derive_assignment_sorts(State&) override {}
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        MemoryPool mp;
        if(ValueString* v = dynamic_cast<ValueString*>(value))
            out << '"' << String(v->getValue()).escape(bitpowder::lib::String::LikeC, mp) << '"';
        else if(ValueBool* v = dynamic_cast<ValueBool*>(value))
            if(lang == Lua)
                out << (v->getValue() ? "true" : "false");
            else
                throw runtime_error("Trying to print boolean as ECA");
        else if(ValueFloat* v = dynamic_cast<ValueFloat*>(value))
            out << v->getValue();
        else if(ValueInt* v = dynamic_cast<ValueInt*>(value))
            out << v->getValue();
        else
            throw runtime_error("Error while printing AST_Constant: unknown value type");
    }
    Rule* getTypeRule() const override;
};

class ASTVariable : public ASTExpression {
    bitpowder::lib::String name;
public:
    ASTVariable(const bitpowder::lib::String& name) : name(name) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        if(!state.exists(name.stl()))
            //throw runtime_error("Warning: Variable name '" + name.stl() + "' may be undeclared at use");
            cout << "Warning: Variable name '" << name << "' may be undeclared at use" << endl;
            // This warning may be triggered when you have global assignments(e.g. Lua), as
            // this will make it impossible to determine locally if a variable has been defined
            // at a variable lookup. Don't use derive_assignment_sorts for Lua, as the sorts of
            // assignments are already known.
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        out << name;
    }
    Rule* getTypeRule() const override;
};

enum AssignmentSort {LocalDecl, Assign, Unknown};

class ASTAssignment : public ASTExpression {
    bitpowder::lib::String name;
    ASTExpression::Ref rhs;
    AssignmentSort sort;
public:
    ASTAssignment(const bitpowder::lib::String& name, ASTExpression::Ref rhs, AssignmentSort sort) : name(name), rhs(rhs), sort(sort) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        rhs->derive_assignment_sorts(state);
        if(sort == Unknown){
            if(state.exists(name.stl()))
                sort = Assign;
            else
                sort = LocalDecl;
        }
        if(sort == LocalDecl)
            state.declareLocalVar(name.stl(), nullptr);

    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(sort == Unknown)
            out << "?UNKNOWNSORT? ";
        if(lang == ECA) {
            out << name << ":=";
            rhs->print(out, lang);
        } else if(lang == Lua) {
            if(sort == LocalDecl)
                out << "local ";
            out << name << " = ";
            rhs->print(out, lang);
        }
    }
    Rule* getTypeRule() const override;
};

class ASTBinary : public ASTExpression {
    ASTExpression::Ref lhs;
    bitpowder::lib::String op;
    ASTExpression::Ref rhs;
public:
    ASTBinary(ASTExpression::Ref lhs, bitpowder::lib::String op, ASTExpression::Ref rhs) : lhs(lhs), op(op), rhs(rhs) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        lhs->derive_assignment_sorts(state);
        rhs->derive_assignment_sorts(state);
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(lang == ECA){
            lhs->print(out, lang);
            out << op;
            rhs->print(out, lang);
        } else if(lang == Lua) {
            out << "(";
            lhs->print(out, lang);
            out << " " << op << " ";
            rhs->print(out, lang);
            out << ")";
        }
    }
    Rule* getTypeRule() const override;
};

class ASTConditional : public ASTStatement {
    ASTExpression::Ref c;
    ASTStatement::Ref t;
    ASTStatement::Ref e;
public:
    ASTConditional(ASTExpression::Ref c, ASTStatement::Ref t, ASTStatement::Ref e) : c(c), t(t), e(e) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        c->derive_assignment_sorts(state);
        state.exitLocalScope();
        state.enterLocalScope();
        t->derive_assignment_sorts(state);
        state.exitLocalScope();
        state.enterLocalScope();
        e->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        out << "if ";
        c->print(out, lang);
        out << " then ";
        t->print(out, lang);
        out << " else ";
        e->print(out, lang);
        out << " end";
    }
    Rule* getTypeRule() const override;
};

class ASTRepeat: public ASTStatement {
    ASTExpression::Ref i;
    ASTStatement::Ref b;
public:
    ASTRepeat(ASTExpression::Ref i, ASTStatement::Ref b) : i(i), b(b) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        i->derive_assignment_sorts(state);
        state.exitLocalScope();
        state.enterLocalScope();
        b->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(lang == ECA){
            out << "repeat ";
            i->print(out, lang);
            out << " begin ";
            b->print(out, lang);
            out << " end";
        } else if(lang == Lua){
            throw runtime_error("Error: Repeat node cannot be printed as Lua");
        }
    }
    Rule* getTypeRule() const override;
};

class ASTWhile : public ASTStatement {
    ASTExpression::Ref c;
    ASTStatement::Ref b;
public:
    ASTWhile(ASTExpression::Ref c, ASTStatement::Ref b) : c(c), b(b) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        c->derive_assignment_sorts(state);
        state.exitLocalScope();
        state.enterLocalScope();
        b->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        out << "while ";
        c->print(out, lang);
        if(lang == ECA)
            out << " begin ";
        else if(lang == Lua)
            out << " do ";
        b->print(out, lang);
        out << " end";
    }
    Rule* getTypeRule() const override;
};

class ASTConcat : public ASTStatement {
    ASTStatement::Ref a;
    ASTStatement::Ref b;
public:
    ASTConcat(ASTStatement::Ref a, ASTStatement::Ref b) : a(a), b(b) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        a->derive_assignment_sorts(state);
        b->derive_assignment_sorts(state);
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        a->print(out, lang);
        out << ";";
        b->print(out, lang);
    }
    virtual bool isExpression() const override {
        return b->isExpression();
    }
    Rule* getTypeRule() const override;
};

class ASTFunctionCall : public ASTExpression {
    bitpowder::lib::String functionName;
    ASTExpression::Ref argument;
public:
    ASTFunctionCall(const bitpowder::lib::String& functionName, ASTExpression::Ref argument) : functionName(functionName), argument(argument) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        argument->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        out << functionName << "(";
        argument->print(out, lang);
        out << ")";
    }
    virtual bool isExpression() const override {
        return argument->isExpression();
    }
    Rule* getTypeRule() const override;
};

class ASTComponentFunctionCall : public ASTExpression {
    bitpowder::lib::String componentName;
    bitpowder::lib::String functionName;
    ASTExpression::Ref argument;
public:
    ASTComponentFunctionCall(const bitpowder::lib::String& componentName, const bitpowder::lib::String& functionName, ASTExpression::Ref argument) : componentName(componentName), functionName(functionName), argument(argument) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        argument->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(lang == ECA){
            out << componentName << "." << functionName << "(";
            argument->print(out, lang);
            out << ")";
        } else if(lang == Lua){
            out << componentName << "_" << functionName << "(";
            argument->print(out, lang);
            out << ")";
        }
    }
    virtual bool isExpression() const override {
        return argument->isExpression();
    }
    Rule* getTypeRule() const override;
};

class ASTFunctionDefinition : public ASTExpression {
    friend class ASTProgram;
    bitpowder::lib::String functionName;
    bitpowder::lib::String argumentName;
    ASTStatement::Ref body;
public:
    typedef bitpowder::lib::shared_object<ASTFunctionDefinition, AST> Ref;
    ASTFunctionDefinition(const bitpowder::lib::String& functionName, const bitpowder::lib::String& argumentName, ASTStatement::Ref body) : functionName(functionName), argumentName(argumentName), body(body) {}
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        state.declareLocalVar(argumentName.stl(), nullptr);
        body->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        out << "function " << functionName << "(" << argumentName << ")" << endl;
        body->print(out, lang);
        out << endl << "end";
    }
    virtual bool isExpression() const override {
        return true;
    }
    Rule* getTypeRule() const override;
};

class ASTReturn : public ASTStatement {
    ASTExpression::Ref expr;
public:
    ASTReturn(ASTExpression::Ref expr) : expr(expr) {}
    virtual void derive_assignment_sorts(State& state) override {
        expr->derive_assignment_sorts(state);
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        if(lang == Lua)
            out << "return ";
        expr->print(out, lang);
    }
    Rule* getTypeRule() const override;
};

class ASTCast : public ASTExpression {
    const Type to;
    ASTExpression::Ref argument;
public:
    ASTCast(Type to, ASTExpression::Ref argument) : to(to), argument(argument) {
    }
    virtual void derive_assignment_sorts(State& state) override {
        state.enterLocalScope();
        argument->derive_assignment_sorts(state);
        state.exitLocalScope();
    }
    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        argument->print(out, lang);
    }
    virtual bool isExpression() const override {
        return argument->isExpression();
    }
    Rule* getTypeRule() const override;
};

class ASTProgram : public AST {
public:
    std::map<bitpowder::lib::String,ASTFunctionDefinition::Ref> functions;
    ASTStatement::Ref main;

    typedef bitpowder::lib::shared_object<ASTProgram, AST> Ref;

    virtual void derive_assignment_sorts(State& state) override {
        for(auto res : functions){
            state.enterLocalScope();
            res.second->derive_assignment_sorts(state);
            state.exitLocalScope();
        }
        main->derive_assignment_sorts(state);
    }

    ASTProgram* deriveAssignmentSorts(){
        State state = State();
        this->derive_assignment_sorts(state);
        return this;
    }

    Rule* getTypeRule() const;

    virtual void print(std::ostream& out, ECAOrLua lang=ECA) const override {
        for (const auto& f : functions) {
            f.second->print(out, lang);
            out << std::endl;
        }
        main->print(out, lang);
    }
};

class ASTParseResult {
    bool success;
		bitpowder::lib::StringLength pos = 0;
    bitpowder::lib::String errorMessage;
    ASTProgram::Ref retval;
public:
    ASTParseResult(){}
    ASTParseResult(bitpowder::lib::StringLength position, bitpowder::lib::String errorMessage) : success(false), pos(position), errorMessage(errorMessage) {
    }
    ASTParseResult(ASTProgram::Ref &&retval) : success(true), retval(std::move(retval)) {
    }
    operator bool() {
        return success;
    }
    ASTProgram::Ref result() {
        return retval;
    }
    bitpowder::lib::String error() {
        return errorMessage;
    }
    size_t position() {
        return pos;
    }
};

// correct lifetime of strings in the structure is limited to the lifetime of the MemoryPool
ASTParseResult ParseECAMemory(const bitpowder::lib::String &ecaString, bitpowder::lib::MemoryPool &mp);

// correct lifetime of strings in the structure is limited to the lifetime of the MemoryPool
ASTParseResult ParseECAFile(const bitpowder::lib::StringT &filename, bitpowder::lib::MemoryPool& mp);


#endif // PARSE_H

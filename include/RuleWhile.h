#ifndef RULEWHILE_H
#define RULEWHILE_H

#include "Rule.h"

class RuleWhile : public Rule
{
    public:
        RuleWhile(Rule* condition, Rule* body, const AST* ast);
        EvaluationResult evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components) override;
        ~RuleWhile() override;
	protected:
		vector<Rule*>* getChildren() override;
	private:
		Rule* condition;
		Rule* body;
};

#endif // RULEWHILE_H

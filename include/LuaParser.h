#pragma once

#ifndef LUAPARSER_H
#define LUAPARSER_H

#include <stdio.h>
#include "LuaLexer.h"
#include "parse.h"
#include "parser.h"
#include "stringparse.h"


using namespace bitpowder::lib;

template <class Lexer>
/**
 * @brief The LuaLanguage struct defines the functions used in parsing Lua
 */
struct LuaLanguage {
    typedef ASTStatement::Ref Stmt;
    typedef ASTFunctionDefinition::Ref FuncDef;
    typedef ASTProgram::Ref Program;

    typedef MemoryPool& UserData;
    typedef Parser<LuaLexer, LUA_LOOKAHEAD, Stmt, UserData>& PStmt;
    typedef Parser<LuaLexer, LUA_LOOKAHEAD, FuncDef, UserData>& PFuncDef;
    typedef Parser<LuaLexer, LUA_LOOKAHEAD, Program, UserData>& PProgram;

    static int constant(Stmt &value, const Token<Value*> &token, UserData){
        if(dynamic_cast<ValueFloat*>(token.value))
            throw runtime_error("Error: ECA does not support Lua floating points yet");
        value = new ASTConstant(token.value);
        return 0;
    }

    // Parse a variable name and store it in the parser
    static int name(String& retval, const Token<LuaName> &token, UserData) {
        retval = token.value;
        return 0;
    }

    //////// Program

    static PProgram funcDef(PProgram cont) {
        String funcName;
        String argName;
        return cont()
                .accept(Literal::FUNCTION)
                .fetch(name, funcName)
                .accept(Literal::BRACKET_OPEN)
                .fetch(name, argName)
                .accept(Literal::BRACKET_CLOSE)
                .process(block, [](Program &a, const String& funcName, const String& argName, Stmt&& body, UserData) {
                        //Don't check if body is not expression: ECA assignments are expressions, but assignments are not considered expressions in Lua
                        a->functions[funcName] = new ASTFunctionDefinition(funcName, argName, body);
                        return 0;
                    }, funcName, argName)
                .accept(Literal::END);
    }

    static PProgram program(PProgram cont){
        return cont.store(new ASTProgram()).repeat(funcDef).perform(progTail);
    }

    static PProgram progTail(PProgram cont) {
        return cont().process(block, [](Program &a, Stmt&& b, UserData) {
            a->main = b;
            return 0;
        });
    }

    //////// Statement

    static PStmt stmt(PStmt cont) {
        return cont().choose(skip, doBlock, assignment, localInit, stmtIf, stmtWhile, functionCall);
    }

    static PStmt block(PStmt cont) {
        return cont().choose(filledBlock, returnStmt, emptyBlock);
    }

    static PStmt filledBlock(PStmt cont) {
        return cont().perform(stmt).repeat(blockTail).opt(optReturn);
    }

    static PStmt optReturn(PStmt cont) {
        return cont()
                .process(returnStmt, [](Stmt &a, Stmt &&ret, UserData) {
                    a = new ASTConcat(a, ret);
                    return 0;
            });
    }

    static PStmt blockTail(PStmt cont) {
        return cont().process(stmt, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTConcat(a, b);
            return 0;
        });
    }

    static PStmt emptyBlock(PStmt cont) {
        return cont().store(new ASTSkip);
    }

    static PStmt skip(PStmt cont) {
        return cont().accept(Literal::SEMICOLON).store(new ASTSkip());
    }

    static PStmt doBlock(PStmt cont) {
        return cont().accept(Literal::DO).perform(block).accept(Literal::END);
    }

    static PStmt assignment(PStmt cont) {
        String var;
        return cont()
                .fetch(name, var)
                .accept(Literal::ASSIGNMENT)
                .perform(expr)
                .modify([](Stmt &a, const String& var, UserData) {
                                if (!a->isExpression())
                                    return -1;
                                a = new ASTAssignment(var, a, AssignmentSort::Assign);
                                return 0;
                            }, var);
    }

    static PStmt localInit(PStmt cont) {
        String var;
        return cont()
                .accept(Literal::LOCAL)
                .fetch(name, var)
                .accept(Literal::ASSIGNMENT)
                .perform(expr)
                .modify([](Stmt &a, const String& var, UserData) {
                                if (!a->isExpression())
                                    return -1;
                                a = new ASTAssignment(var, a, AssignmentSort::LocalDecl);
                                return 0;
                            }, var);
    }

    static PStmt stmtIf(PStmt cont) {
        return cont().accept(Literal::IF).perform(afterIf).accept(Literal::END);
    }

    /**
     * Parses the rest of an if after either an if or an elseif has been read
     */
    static PStmt afterIf(PStmt cont) {
        Stmt c, t;
        return cont()
                .fetch(expr, c)
                .accept(Literal::THEN)
                .fetch(block, t)
                .store(new ASTSkip())
                .optChoose(stmtElseif, stmtElse)
                .modify([](Stmt& e, Stmt& c, Stmt& t, UserData){
                            if(!c->isExpression())
                                return -1;
                            e = new ASTConditional(c, t, e);
                            return 0;
        }, c, t);
    }

    static PStmt stmtElseif(PStmt cont){
        return cont().accept(Literal::ELSEIF).perform(afterIf);
    }
    static PStmt stmtElse(PStmt cont){
        return cont().accept(Literal::ELSE).perform(block);
    }

    static PStmt stmtWhile(PStmt cont) {
        Stmt c;
        return cont()
                .accept(Literal::WHILE)
                .fetch(expr, c)
                .accept(Literal::DO)
                .perform(block)
                .accept(Literal::END)
                .modify([](Stmt &a, Stmt& c, UserData) {
                                if (!c->isExpression())
                                    return -1;
                                a = new ASTWhile(c, a);
                                return 0;
                            }, c);
    }

    /**
     * @brief functionCall Parse either a function call or a component function call
     */
    static PStmt functionCall(PStmt cont) {
        String functionName;
        return cont()
            .debug("functionCall")
            .fetch(name, functionName)
            .accept(Literal::BRACKET_OPEN)
            .perform(expr)
            .modify([](Stmt &a, const String& functionName, UserData) {
                if (!a->isExpression())
                    return -1;
                if (functionName.contains("_")){
                    String funcName;
                    String compName = functionName.split("_", &funcName);
                    a = new ASTComponentFunctionCall(compName, funcName, a);
                } else {
                    a = new ASTFunctionCall(functionName, a);
                }
                return 0;
            }, functionName)
            .accept(Literal::BRACKET_CLOSE);
    }

    static PStmt returnStmt(PStmt cont) {
        return cont()
            .accept(Literal::RETURN)
            .process(expr, [](Stmt &a, Stmt&& b, UserData) {
                a = new ASTReturn(b);
                return 0;
            });
    }

    //////// Variable

    // Parse a variable name and store an ECAVariable in the parser
    static int varName(Stmt &value, const Token<LuaName> &token, UserData) {
        value = new ASTVariable(token.value);
        return 0;
    }

    static PStmt tableVar(PStmt cont) {
        return cont().error({"Tables not supported yet"});
    }

    //////// Expression

    // Binary operators are parsed with precedence as in http://www.lua.org/manual/5.3/manual.html#3.4.8
    // All operators are parsed, but not all of them are supported in execution

    static PStmt expr(PStmt cont) {
        return cont().perform(exprAnd).repeat(orOp);
    }

    static PStmt orOp(PStmt cont) {
        return cont().accept(Literal::OR).process(exprAnd, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "or", b);
            return 0;
        });
    }

    static PStmt exprAnd(PStmt cont) {
        return cont().perform(exprComp).repeat(andOp);
    }

    static PStmt andOp(PStmt cont) {
        return cont().accept(Literal::AND).process(exprComp, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "and", b);
            return 0;
        });
    }

    static PStmt exprComp(PStmt cont) {
        return cont().perform(exprBitwiseOr).repeatChoose(compLT, compGT, compLEQ, compGEQ, compEQ, compNEQ);
    }

    static PStmt compLT(PStmt cont) {
        return cont().accept(Literal::LESS).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "<", b);
            return 0;
        });
    }

    static PStmt compGT(PStmt cont) {
        return cont().accept(Literal::GREATER).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, ">", b);
            return 0;
        });
    }

    static PStmt compLEQ(PStmt cont) {
        return cont().accept(Literal::LESS_EQ).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "<=", b);
            return 0;
        });
    }

    static PStmt compGEQ(PStmt cont) {
        return cont().accept(Literal::GREATER_EQ).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, ">=", b);
            return 0;
        });
    }

    static PStmt compEQ(PStmt cont) {
        return cont().accept(Literal::EQUAL).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "==", b);
            return 0;
        });
    }

    static PStmt compNEQ(PStmt cont) {
        return cont().accept(Literal::NOT_EQUAL).process(exprBitwiseOr, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "~=", b);
            return 0;
        });
    }

    static PStmt exprBitwiseOr(PStmt cont) {
        return cont().perform(exprBitwiseXor).repeat(bitwiseOrOp);
    }

    static PStmt bitwiseOrOp(PStmt cont) {
        return cont().accept(Literal::PIPE).process(exprBitwiseXor, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "|", b);
            return 0;
        });
    }

    static PStmt exprBitwiseXor(PStmt cont) {
        return cont().perform(exprBitwiseAnd).repeat(bitwiseXorOp);
    }

    static PStmt bitwiseXorOp(PStmt cont) {
        return cont().accept(Literal::NEG).process(exprBitwiseAnd, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "~", b);
            return 0;
        });
    }

    static PStmt exprBitwiseAnd(PStmt cont) {
        return cont().perform(exprBitshift).repeat(bitwiseAndOp);
    }

    static PStmt bitwiseAndOp(PStmt cont) {
        return cont().accept(Literal::AMPERSAND).process(exprBitshift, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "&", b);
            return 0;
        });
    }

    static PStmt exprBitshift(PStmt cont) {
        return cont().perform(exprConcat).repeatChoose(opBitshift_L, opBitshift_R);
    }

    static PStmt opBitshift_L(PStmt cont) {
        return cont().accept(Literal::BITSHIFT_L).process(exprConcat, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "<<", b);
            return 0;
        });
    }

    static PStmt opBitshift_R(PStmt cont) {
        return cont().accept(Literal::BITSHIFT_R).process(exprConcat, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, ">>", b);
            return 0;
        });
    }

    static PStmt exprConcat(PStmt cont) {
        return cont().perform(exprPlus).opt(opConcat);
    }

    static PStmt opConcat(PStmt cont) {
        return cont().accept(Literal::DOTS_2).process(exprConcat, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "..", b);
            return 0;
        });
    }

    static PStmt exprPlus(PStmt cont) {
        return cont().perform(exprMul).repeatChoose(opPlus, opMin);
    }

    static PStmt opPlus(PStmt cont) {
        return cont().accept(Literal::PLUS).process(exprMul, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "+", b);
            return 0;
        });
    }

    static PStmt opMin(PStmt cont) {
        return cont().accept(Literal::MIN).process(exprMul, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "-", b);
            return 0;
        });
    }

    static PStmt exprMul(PStmt cont) {
        return cont().perform(exprPow).repeatChoose(opMul, opIntDiv);
    }

    static PStmt opMul(PStmt cont) {
        return cont().accept(Literal::MUL).process(exprPow, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "*", b);
            return 0;
        });
    }

    static PStmt opIntDiv(PStmt cont) {
        return cont().accept(Literal::INT_DIV).process(exprPow, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "/", b);
            return 0;
        });
    }

    static PStmt exprPow(PStmt cont) {
        return cont().perform(exprBase).opt(opPow);
    }

    static PStmt opPow(PStmt cont) {
        return cont().accept(Literal::POW).process(exprPow, [](Stmt &a, Stmt&& b, UserData) {
            a = new ASTBinary(a, "^", b);
            return 0;
        });
    }

    static PStmt exprBase(PStmt cont) {
        return cont().choose(
                    functionCall, varName, parenthesis,
                    constant
                    );
    }

    //////// PrefixExp (not yet implemented)

    static PStmt prefixExp(PStmt cont) {
        return cont().setError({"Stub"}); //stub
    }

    static PStmt prefixExpBase(PStmt cont){
        return cont.debug("prefixexpBase"_S).choose(varName, parenthesis);
    }

    static PStmt parenthesis(PStmt cont) {
        return cont().debug("parenthesis"_S).accept(Literal::BRACKET_OPEN).perform(expr).accept(Literal::BRACKET_CLOSE);
    }

    static PStmt prefixexpTail(PStmt cont){
        return cont.debug("prefixexpTail"_S).choose(tableSelector, args);
    }

    static PStmt tableSelector(PStmt cont){
        return cont.debug("selector"_S)
                .accept(Literal::SQUARE_OPEN)
                .perform(expr)
                .accept(Literal::SQUARE_CLOSE)
                .setError({"Tables not supported yet"});
    }

    static PStmt args(PStmt cont){
        return cont.debug("args"_S)
                .accept(Literal::BRACKET_OPEN)
                .opt(expr)//Stub
                .accept(Literal::BRACKET_CLOSE)
                .setError({"Not supported yet"})
                .modify([](Stmt& a, Stmt&& exp, UserData) {
            //Stub
            return 0;
        });
    }
};

/**
 * @brief ParseLuaMemory parses a Lua program into an ECA AST
 * @param str the string to parse
 * @return the ECA parse result
 */
ASTParseResult ParseLuaMemory(const String &str, MemoryPool& mp) ;

ASTParseResult ParseLuaFile(const bitpowder::lib::StringT &filename, bitpowder::lib::MemoryPool& mp);

#endif // LUAPARSER_H

#include "ECAComponentFunction.h"

ECAComponentFunction::ECAComponentFunction(string compName, string funcName, string argName,
        Rule* definition, Rule* resourceFunc, Time time)
{
    this->compName = compName;
    this->funcName = funcName;
    this->argName = argName;
    this->definition = definition;
    this->resourceFunc = resourceFunc;
    this->time = time;
}

EvaluationResult ECAComponentFunction::evaluateFunction(State& cState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult defRes = definition->evaluateRule(cState, env, components);
    EvaluationResult resourceRes = resourceFunc->evaluateRule(cState, env, components);
    ValueInt* resourceInt = dynamic_cast<ValueInt*>(resourceRes.getValue());
    if(resourceInt)
        return ValueResult(defRes.getValue(), time, resourceInt->getValue());
    else throw runtime_error("Exception: resource function " + compName + "." + funcName + "in component function does not return value of type int\n");
}

string ECAComponentFunction::getCompName() const
{
    return compName;
}

string ECAComponentFunction::getFuncName() const
{
    return funcName;
}

string ECAComponentFunction::getArgName() const
{
    return argName;
}

ECAComponentFunction::~ECAComponentFunction()
{
    //dtor
}

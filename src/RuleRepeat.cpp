#include "RuleRepeat.h"
#include "FunctionEnvironment.h"

RuleRepeat::RuleRepeat(Rule* count, Rule* body, const AST* ast)
{
    this->count = count;
    this->body = body;
    this->ast = ast;
}

EvaluationResult RuleRepeat::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    programState.enterLocalScope();
    EvaluationResult e_count = count->evaluateRule(programState, env, components);
    programState.exitLocalScope();
    if(e_count.getIsRetVal())
        return e_count;
    ResourceAmount resource = e_count.getResourceAmount();
    Time time = e_count.getTime();
    ValueInt* valueInt = dynamic_cast<ValueInt*>(e_count.getValue());

    if(valueInt)
    {
        for(int i = 0; i < valueInt->getValue(); i++)
        {
            programState.enterLocalScope();
            EvaluationResult e = body->evaluateRule(programState, env, components);
            programState.exitLocalScope();
            resource += e.getResourceAmount();
            time += e.getTime();
            if(e.getIsRetVal())
                return ReturnResult(e.getValue(), time, resource);
        }
        return ValueResult(nullptr, time, resource);
    }
    else throw runtime_error("Exception: Used non-integer in statement: \n" + ast->toString(ECA) + "\n");
}

vector<Rule*>* RuleRepeat::getChildren()
{
    return new vector<Rule*>({count, body});
}

RuleRepeat::~RuleRepeat()
{
    //dtor
}

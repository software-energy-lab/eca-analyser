#include "RuleAssign.h"
#include "FunctionEnvironment.h"

RuleAssign::RuleAssign(string variable, Rule* expression, const AST* ast)
{
    this->variable = variable;
    this->expr = expression;
    this->ast = ast;
}

EvaluationResult RuleAssign::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e = expr->evaluateRule(programState, env, components);
    if(e.getIsRetVal())
        return e;
    if(!programState.localAssign(variable, e.getValue()))
        programState.declareGlobalVar(variable, e.getValue());
    return ValueResult(
                e.getValue(),
                e.getTime() + env.getTAssign(),
                e.getResourceAmount() + td_ec(components, env.getTAssign())
    );
}

vector<Rule*>* RuleAssign::getChildren()
{
    return new vector<Rule*>({expr});
}

RuleAssign::~RuleAssign()
{
    //dtor
}

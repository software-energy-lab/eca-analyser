#include "parse.h"
#include "State.h"
#include "ReadComponent.h"
#include "Rule.h"

using namespace std;
using namespace bitpowder::lib;

ECAComponent* readECAComponent(istream &compFile)
{
	/* This function is inherited from a previous version of the project and contains a lot of boilerplate code.
	 * Until now, it did not cause any problems, but it might be necessary to rewrite it in the future.
	 */
	string compName;
	State cState = State();
	State derivedAssignmentCheckState = State();
	map<string, ECAComponentFunction*> functions;
	Rule* dyn_res_cons;

	string line;
	getline(compFile, line);
	while(line.empty())
		getline(compFile, line);
	size_t pos = line.find("component");
	if(pos == string::npos)
	{
		cerr << "Missing component name in file." << endl;
		exit(-1);
	}
	compName = line.substr(pos+10);
	cout << "Component name: " << compName << endl;

	getline(compFile, line);
	while(line.empty())
		getline(compFile, line);

	pos = line.find("variable");
	while(pos !=  string::npos)
	{
		line.erase(pos, 9);
		pos = line.find(":=");
		if(pos == string::npos)
		{
			cerr << "Variable incorrectly defined in file." << endl;
			exit(-1);
		}

		string varName = line.substr(0, pos-1);
		line.erase(0, pos+2);
		stringstream ss;
		ss << line;
		int val = -1;
		ss >> val;
		ss.str("");
		ss.clear();
		cState.declareLocalVar(varName, new ValueInt(val));
		derivedAssignmentCheckState.declareLocalVar(varName, nullptr);

		cout << "Assigned component variable: " << varName << " := " << val << endl;

		getline(compFile, line);
		while(line.empty())
			getline(compFile, line);
		pos = line.find("variable");
	}

	pos = line.find("phi:");
	if(pos != string::npos)
	{
		getline(compFile, line);
		string phiString = "";
		pos = line.find("end phi");
		while(pos == string::npos)
		{
			phiString += line;
			getline(compFile, line);
			while(line.empty())
				getline(compFile, line);
			pos = line.find("end phi");
		}
		bitpowder::lib::MemoryPool mpPhi;
		auto phiResult = ParseECAMemory(phiString, mpPhi);
		if(!phiResult)
		{
			cerr << "Invalid phi function." << endl;
			exit(-1);
		}
		derivedAssignmentCheckState.enterLocalScope();
		phiResult.result()->derive_assignment_sorts(derivedAssignmentCheckState);
		derivedAssignmentCheckState.exitLocalScope();
		dyn_res_cons = phiResult.result()->getTypeRule();
		cout << "Added phi function." << endl << dyn_res_cons->getASTString(ECA) << endl;
	}

	getline(compFile, line);
	while(line.empty())
		getline(compFile, line);

	pos = line.find("component function");
	while(pos != string::npos)
	{
		derivedAssignmentCheckState.enterLocalScope();
		Rule* resourceRule;
		Rule* definitionRule;

		bool error = false;
		line.erase(0, 19);
		pos = line.find('(');
		if(pos == string::npos)
			error = true;
		string funcName = line.substr(0, pos);
		line.erase(0, pos+1);
		pos = line.find(')');
		if(pos == string::npos)
			error = true;
		line.erase(pos, 1);
		string argument = line;
		derivedAssignmentCheckState.declareLocalVar(argument, nullptr);
		getline(compFile, line);
		while(line.empty())
			getline(compFile, line);
		pos = line.find("time");
		if(pos == string::npos)
			error = true;
		line.erase(0, pos+4);
		stringstream ss;
		ss << line;
		Time time = 0;
		ss >> time;

		getline(compFile, line);
		while(line.empty())
			getline(compFile, line);
		pos = line.find("resource:");
		if(pos != string::npos && !error)
		{
			getline(compFile, line);
			while(line.empty())
				getline(compFile, line);
			string resourceString = "";
			pos = line.find("end resource");
			while(pos == string::npos)
			{
				resourceString += line;
				getline(compFile, line);
				while(line.empty())
					getline(compFile, line);
				pos = line.find("end resource");
			}
			bitpowder::lib::MemoryPool mpResource;
			auto resourceResult = ParseECAMemory(resourceString, mpResource);
			if(!resourceResult)
			{
				cerr << "Invalid resource function." << endl;
				exit(-1);
			}
			derivedAssignmentCheckState.enterLocalScope();
			resourceResult.result()->derive_assignment_sorts(derivedAssignmentCheckState);
			derivedAssignmentCheckState.exitLocalScope();
			resourceRule = resourceResult.result()->getTypeRule();
		}
		else
			error = true;

		getline(compFile, line);
		while(line.empty())
			getline(compFile, line);
		pos = line.find("definition:");
		if(pos != string::npos && !error)
		{
			getline(compFile, line);
			while(line.empty())
				getline(compFile, line);
			string definitionString = "";
			pos = line.find("end definition");
			while(pos == string::npos)
			{
				definitionString += line;
				getline(compFile, line);
				while(line.empty())
					getline(compFile, line);
				pos = line.find("end definition");
			}
			bitpowder::lib::MemoryPool mpDefinition;
			auto definitionResult = ParseECAMemory(definitionString, mpDefinition);
			if(!definitionResult)
			{
				cerr << "Invalid definition function." << endl;
				exit(-1);
			}
			derivedAssignmentCheckState.enterLocalScope();
			definitionResult.result()->derive_assignment_sorts(derivedAssignmentCheckState);
			derivedAssignmentCheckState.exitLocalScope();
			definitionRule = definitionResult.result()->getTypeRule();
		}
		else
			error = true;

		if(error)
		{
			cerr << "Component function incorrectly defined." << endl;
			exit(-1);
		}

		ECAComponentFunction* compFunc = new ECAComponentFunction(compName, funcName, argument, definitionRule, resourceRule, time);
		functions.insert(make_pair(funcName, compFunc));
		derivedAssignmentCheckState.exitLocalScope();

		cout << "Added component function: " << funcName << endl;

		getline(compFile, line);
		while(line.empty())
			getline(compFile, line);
		pos = line.find("component function");
	}
	pos = line.find("end component");
	if(pos == string::npos)
	{
		cerr << "Missing \"end component\"." << endl;
		exit(-1);
	}
	return new ECAComponent(compName, cState, functions, move(dyn_res_cons));
}

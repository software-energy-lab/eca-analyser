#include "FunctionEnvironment.h"

FunctionEnvironment::FunctionEnvironment()
{
    t_input = t_const = t_var = t_assign = t_binop = t_if = t_while = 0;
    functions = vector<Function*>();
}

FunctionEnvironment::FunctionEnvironment(Time t_input, Time t_const, Time t_var, Time t_assign, Time t_binop, Time t_if,
                Time t_while,
                vector<Function*> functions)
{
    this->t_input = t_input;
    this->t_const = t_const;
    this->t_var = t_var;
    this->t_assign = t_assign;
    this->t_binop = t_binop;
	this->t_if = t_if;
	this->t_while = t_while;
	this->functions = functions;
}



Time FunctionEnvironment::getTInput()
{
    return t_input;
}

Time FunctionEnvironment::getTConst()
{
    return t_const;
}

Time FunctionEnvironment::getTVar()
{
    return t_var;
}

Time FunctionEnvironment::getTAssign()
{
    return t_assign;
}

Time FunctionEnvironment::getTBinop()
{
    return t_binop;
}

Time FunctionEnvironment::getTIf()
{
	return t_if;
}

Time FunctionEnvironment::getTWhile()
{
    return t_while;
}

Function* FunctionEnvironment::getFunction(string name)
{
	for(size_t i=0;i<functions.size();i++)
	{
		Function* function = functions.at(i);
		if(function->getName() == name)
		{
			return function;
		}
	}
	throw runtime_error("Exception: No function found with name: " + name);
}

void FunctionEnvironment::addFunction(Function* function)
{
	functions.push_back(function);
}

FunctionEnvironment::~FunctionEnvironment()
{
}


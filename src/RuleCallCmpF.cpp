#include "RuleCallCmpF.h"
#include "FunctionEnvironment.h"

RuleCallCmpF::RuleCallCmpF(string componentName, string functionName, Rule* arg, const AST* ast)
{
    this->componentName = componentName;
    this->functionName = functionName;
    this->arg = arg;
    this->ast = ast;
}

EvaluationResult RuleCallCmpF::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e_arg = arg->evaluateRule(programState, env, components);
    if(e_arg.getIsRetVal())
        return e_arg;
    auto res = components.find(componentName);
    if(res == components.end())
        throw runtime_error("Error: component \"" + componentName + "\" not found for function call " + ast->toString(ECA) + "\n");
    EvaluationResult e_func = res->second->evaluateFunction(functionName, e_arg.getValue());
    return ValueResult(
                e_func.getValue(),
                e_arg.getTime() + e_func.getTime(),
                e_arg.getResourceAmount() + e_func.getResourceAmount() + td_ec(components, e_func.getTime())
                );
}

vector<Rule*>* RuleCallCmpF::getChildren()
{
    return new vector<Rule*>({arg});
}

RuleCallCmpF::~RuleCallCmpF()
{
    //dtor
}

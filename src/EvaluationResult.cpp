#include "EvaluationResult.h"

EvaluationResult::EvaluationResult(Value* value, Time time, ResourceAmount resource, bool isRetVal)
    : value(value), isRetVal(isRetVal), time(time), resource(resource){}

Value *EvaluationResult::getValue() const
{
    return value;
}

bool EvaluationResult::getIsRetVal()
{
    return isRetVal;
}

Time EvaluationResult::getTime() const
{
    return time;
}

ResourceAmount EvaluationResult::getResourceAmount() const
{
    return resource;
}

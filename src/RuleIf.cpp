#include "RuleIf.h"
#include "FunctionEnvironment.h"

RuleIf::RuleIf(Rule* c, Rule* t, Rule* e, const AST* ast)
{
    this->c = c;
    this->t = t;
    this->e = e;
    this->ast = ast;
}

EvaluationResult RuleIf::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    programState.enterLocalScope();
    EvaluationResult e_cond = c->evaluateRule(programState, env, components);
    programState.exitLocalScope();
    if(e_cond.getIsRetVal())
        return e_cond;
    ResourceAmount tdec = td_ec(components, env.getTIf());
    ValueBool* valueBool = dynamic_cast<ValueBool*>(e_cond.getValue());
    if(valueBool)
    {
        EvaluationResult e_branch = EvaluationResult(nullptr, 0, 0, false);
        if(valueBool->getValue() != 0){
            programState.enterLocalScope();
            e_branch = t->evaluateRule(programState, env, components);
            programState.exitLocalScope();
        } else {
            programState.enterLocalScope();
            e_branch = e->evaluateRule(programState, env, components);
            programState.exitLocalScope();
        }
        Value* val = e_branch.getIsRetVal() ? e_branch.getValue() : nullptr;

        return EvaluationResult(
                    val,
                    e_cond.getTime() + env.getTIf() + e_branch.getTime(),
                    e_cond.getResourceAmount() + tdec + e_branch.getResourceAmount(),
                    e_branch.getIsRetVal());
    }
    else throw runtime_error("Exception: Used non-boolean value in if statement: \n" + ast->toString(ECA) + "\n");
}

vector<Rule*>* RuleIf::getChildren()
{
    return new vector<Rule*>({c, t, e});
}

RuleIf::~RuleIf()
{
    //dtor
}

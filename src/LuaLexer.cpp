#include <iostream>
#include "LuaLexer.h"
//#include "parser.h"

std::ostream& operator<<(std::ostream& os, const Literal& t)
{
    switch (t) {
    case Literal::AND : os << "and"; break;
    case Literal::BREAK : os << "break"; break;
    case Literal::DO : os << "do"; break;
    case Literal::ELSE : os << "else"; break;
    case Literal::ELSEIF : os << "elseif"; break;
    case Literal::END : os << "end"; break;
    case Literal::FOR : os << "for"; break;
    case Literal::FUNCTION : os << "function"; break;
    case Literal::GOTO : os << "goto"; break;
    case Literal::IF : os << "if"; break;
    case Literal::IN : os << "in"; break;
    case Literal::LOCAL : os << "local"; break;
    case Literal::NIL : os << "nil"; break;
    case Literal::NOT : os << "not"; break;
    case Literal::OR : os << "or"; break;
    case Literal::REPEAT : os << "repeat"; break;
    case Literal::RETURN : os << "return"; break;
    case Literal::THEN : os << "then"; break;
    case Literal::UNTIL : os << "until"; break;
    case Literal::WHILE : os << "while"; break;
    case Literal::PLUS : os << "+"; break;
    case Literal::MIN : os << "-"; break;
    case Literal::MUL : os << "*"; break;
    case Literal::INT_DIV : os << "/"; break;
    case Literal::DIV : os << "/"; break;
    case Literal::MOD : os << "%"; break;
    case Literal::POW : os << "^"; break;
    case Literal::HASH : os << "#"; break;
    case Literal::AMPERSAND : os << "&"; break;
    case Literal::NEG : os << "~"; break;
    case Literal::PIPE : os << "|"; break;
    case Literal::BITSHIFT_L : os << "<<"; break;
    case Literal::BITSHIFT_R : os << ">>"; break;
    case Literal::EQUAL : os << "=="; break;
    case Literal::NOT_EQUAL : os << "~="; break;
    case Literal::LESS_EQ : os << "<="; break;
    case Literal::GREATER_EQ : os << ">="; break;
    case Literal::LESS : os << "<"; break;
    case Literal::GREATER : os << ">"; break;
    case Literal::ASSIGNMENT : os << "="; break;
    case Literal::BRACKET_OPEN : os << "("; break;
    case Literal::BRACKET_CLOSE : os << ")"; break;
    case Literal::CURLY_OPEN : os << "{"; break;
    case Literal::CURLY_CLOSE : os << "}"; break;
    case Literal::SQUARE_OPEN : os << "["; break;
    case Literal::SQUARE_CLOSE : os << "]"; break;
    case Literal::DOUBLE_COLON : os << "::"; break;
    case Literal::SEMICOLON : os << ";"; break;
    case Literal::COLON : os << ":"; break;
    case Literal::COMMA : os << ","; break;
    case Literal::DOTS_1 : os << "."; break;
    case Literal::DOTS_2 : os << ".."; break;
    case Literal::DOTS_3 : os << "..."; break;
    }
    return os;
}


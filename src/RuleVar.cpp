#include "RuleVar.h"
#include "FunctionEnvironment.h"

RuleVar::RuleVar(string name, const AST* ast)
{
    this->name = name;
    this->ast = ast;
}

EvaluationResult RuleVar::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
	return ValueResult(
				programState.lookup(name),
				env.getTVar(),
				td_ec(components, env.getTVar())
	);
}

vector<Rule*>* RuleVar::getChildren()
{
	return new vector<Rule*>();
}

RuleVar::~RuleVar()
{
    //dtor
}

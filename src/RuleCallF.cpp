#include "RuleCallF.h"
#include "FunctionEnvironment.h"

RuleCallF::RuleCallF(string functionName, Rule* arg, const AST* ast)
{
    this->functionName = functionName;
    this->arg = arg;
    this->ast = ast;
}

EvaluationResult RuleCallF::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e_arg = arg->evaluateRule(programState, env, components);
    if(e_arg.getIsRetVal())
        return e_arg;
    auto localStateBackup = programState.getLocalState();
    programState.emptyLocalScopes();
    Function* function = env.getFunction(functionName);
    programState.declareLocalVar(function->getArgumentName(), e_arg.getValue());
    EvaluationResult e_func = function->evaluateRule(programState, env, components);
    programState.setLocalState(localStateBackup);
    // The function call should only return a value to its context if a return statement
    // was explicitely evaluated, not if the body of the function was an expression
    Value* retVal = e_func.getIsRetVal() ? e_func.getValue() : nullptr;
    return ValueResult(
                retVal,
                e_arg.getTime() + e_func.getTime(),
                e_arg.getResourceAmount() + e_func.getResourceAmount()
    );
}

vector<Rule*>* RuleCallF::getChildren()
{
    return new vector<Rule*>({arg});
}

RuleCallF::~RuleCallF()
{
    //dtor
}

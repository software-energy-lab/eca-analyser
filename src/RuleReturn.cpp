#include "RuleReturn.h"
#include "FunctionEnvironment.h"

RuleReturn::RuleReturn(Rule* retExpr, const AST* ast) : retExpr(retExpr)
{
    this->ast = ast;
}

EvaluationResult RuleReturn::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e = retExpr->evaluateRule(programState, env, components);
    return ReturnResult(e.getValue(), e.getTime(), e.getResourceAmount());
}

vector<Rule*>* RuleReturn::getChildren()
{
    return new vector<Rule*>({retExpr});
}

RuleReturn::~RuleReturn()
{
    //dtor
}

#include "RuleCast.h"

RuleCast::RuleCast(const Type& to, Rule* arg, const AST* ast) : to(to)
{
    this->arg = arg;
    this->ast = ast;
}

EvaluationResult RuleCast::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e = arg->evaluateRule(programState, env, components);
    if(e.getIsRetVal())
        return e;
    Value* v = e.getValue();
    ValueBool* vbool;
    ValueFloat* vfloat;
    ValueInt* vint;
    ValueString* vstring;

    if((vbool = dynamic_cast<ValueBool*>(v)))
        switch (to) {
        case Type::Int: v = new ValueInt(vbool->getValue() ? 1 : 0); break;
        default: throw runtime_error("Only conversion between bool and int supported");
        }
    else if((vint = dynamic_cast<ValueInt*>(v)))
        switch (to) {
        case Type::Bool: v = new ValueBool(vint->getValue() != 0); break;
        default: throw runtime_error("Only conversion between bool and int supported");
        }
    else throw runtime_error("Only conversion between bool and int supported");

    return ValueResult(v, e.getTime(), e.getResourceAmount());
}

vector<Rule*>* RuleCast::getChildren()
{
    return new vector<Rule*>({arg});
}

RuleCast::~RuleCast()
{
    //dtor
}

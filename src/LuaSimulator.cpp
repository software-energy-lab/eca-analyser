//#define LUA_SIMULATOR_DEBUG

#include <chrono>
#include "LuaSimulator.h"
#include "ValueBool.h"
#include "ValueFloat.h"
#include "ValueInt.h"
#include "ValueString.h"

LuaSimulator* LuaSimulator::theSimulator;

LuaSimulator* LuaSimulator::createSimulator(string &lua_code, Mode mode){
    if(theSimulator)
        throw runtime_error("Error while creating simulator: simulator already exists");
    theSimulator = new LuaSimulator(lua_code, mode);
    return theSimulator;
}

LuaSimulator* LuaSimulator::getInstance(){
    return theSimulator;
}

LuaSimulator::LuaSimulator(string &lua, Mode mode)
{
    progState = luaL_newstate();
    luaL_openlibs(progState);
    // Push the code as a lua function on the stack
    int err;
    if(mode == file)
        err = luaL_loadfile(progState, lua.c_str());
    else // mode == code
        err = luaL_loadstring(progState, lua.c_str());
    if(err)
        throw runtime_error("Error while initializing lua simulator:\n\t" + string(lua_tostring(progState, 1)));
}

/**
 * @brief LuaSimulator::registerComponents registers the component functions of all components into the lua state.
 *
 * All component functions need to be added to the state as Lua functions with name 'compname_compfuncname'.
 * Lua C functions need to be static functions. The component name and function name are added to the Lua
 * function as upvalues to be able to retrieve the original component function from a static context.
 *
 * @param components
 */
void LuaSimulator::registerComponents(map<string, Component *> *components){
    this->components = components;
    for(auto c: *components){
        unordered_set<string> funcs = c.second->getComponentFunctionNames();
        for(string func : funcs){
            string lua_func_name = c.first + "_" + func;
//#ifdef LUA_SIMULATOR_DEBUG
            cout << "Registering component function " << lua_func_name << endl;
//#endif
            // Push the component name and the function name on the stack
            lua_pushstring(progState, c.first.c_str());
            lua_pushstring(progState, func.c_str());
            // Push a C closure on the stack, containing these two names as upvalues
            lua_pushcclosure(progState, LuaSimulator::callCompFunc, 2);
            // Set the function in the global environment
            lua_setglobal(progState, lua_func_name.c_str());
        }
    }
}


void LuaSimulator::declareVar(string name, Value *value){
    ValueBool* vbool = dynamic_cast<ValueBool*>(value);
    ValueFloat* vfloat = dynamic_cast<ValueFloat*>(value);
    ValueInt* vint = dynamic_cast<ValueInt*>(value);
    ValueString* vstring = dynamic_cast<ValueString*>(value);
    if(vbool){
        lua_pushboolean(progState, vbool->getValue());
    } else if(vfloat){
        lua_pushnumber(progState, vfloat->getValue());
    } else if(vint){
        lua_pushinteger(progState, vint->getValue());
    } else if(vstring){
        lua_pushstring(progState, vstring->getValue().c_str());
    } else {
        throw runtime_error("Error: unknown type of value");
    }
    lua_setglobal(progState, name.c_str());
}


int LuaSimulator::callCompFunc(lua_State* state){

    /** Resource usage administration **/
        LuaSimulator* simulator = LuaSimulator::getInstance();
        DynResCons totalPowerDraw = simulator->pause();

    /** Invoke the model **/
        // Get the component and function names as upvalues
        string compName(lua_tostring(state, lua_upvalueindex(1)));
        string funcName(lua_tostring(state, lua_upvalueindex(2)));
#ifdef LUA_SIMULATOR_DEBUG
        cout << "Function '" << funcName << "' of component '" << compName << "' is being simulated " << endl;
#endif
        auto res = simulator->components->find(compName);
        if(res == simulator->components->end())
            throw runtime_error("Component " + compName + " could not be found");

        // Convert the argument to a Value and let the component model evaluate the function
        EvaluationResult s(nullptr, 0, 0, false);
        int nr_args = lua_gettop(state);
        if(nr_args != 1)
            throw runtime_error("Function '" + funcName + "' of component '" + compName + "' is called with " + to_string(nr_args) + " arguments in stead of 1");
        if(lua_isnumber(state, 1)){
            ValueInt val(int(lua_tointeger(state, 1)));
            s = res->second->evaluateFunction(funcName, &val);
        }
        else if(lua_isstring(state, 1)){
            ValueString val(lua_tostring(state, 1));
            s = res->second->evaluateFunction(funcName, &val);
        }
        else {
            throw runtime_error("Argument of " + funcName + " of component " + compName + " is not a string or an integer");
        }

    /** Resource usage administration **/
        simulator->resource += s.getResourceAmount() + totalPowerDraw * s.getTime(); //Don't forget the time dependent resource consumption during compFunc evaluation
        simulator->duration += chrono::nanoseconds(s.getTime());

    /** Return to Lua **/
        // Push the result to the stack
        ValueInt* svalint = dynamic_cast<ValueInt*>(s.getValue());
        if(svalint)
            lua_pushinteger(state, svalint->getValue());
        else //value of type string
            lua_pushstring(state, dynamic_cast<ValueString*>(s.getValue())->getValue().c_str());

        simulator->unpause();
        // Return 1 result from the stack
        return 1;
}

SimulationResult LuaSimulator::run(){
    resource = 0;
    duration = chrono::nanoseconds(0);
    unpause();
    
    // Call the program that is still on the stack
    lua_call(progState, 0, 0);

    pause();

    return SimulationResult(duration.count(), resource);
}

/**
 * Calculates the execution time and time dependent resource consumption since last unpause() call and adds it to the corresponding class members
 * Needs to be called directly after exiting the C API
 * @return the current dynamic resource consumption of all components
 */
DynResCons LuaSimulator::pause(){
    //calculate how much time has passed
    chrono::time_point tStart = chrono::high_resolution_clock::now();

    chrono::nanoseconds timePassed = chrono::duration_cast<chrono::nanoseconds>(tStart - unpauseTime);
    duration += timePassed;

    //Get the total dynamic resource consumption
    DynResCons totalDynResCons = 0;
    for(auto p : *components){
        totalDynResCons += p.second->dynResCons();
    }
    // Determine total time dependent resource consumption and add to resource
    resource += totalDynResCons * timePassed.count();
    return totalDynResCons;
}

// Needs to be called directly before entering the C API
void LuaSimulator::unpause(){
    unpauseTime = chrono::high_resolution_clock::now();
}

LuaSimulator::~LuaSimulator(){
    lua_close(progState);
    theSimulator = nullptr;
}

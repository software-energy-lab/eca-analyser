#include "RuleLocalDecl.h"
#include "FunctionEnvironment.h"

RuleLocalDecl::RuleLocalDecl(string variable, Rule* expression, const AST* ast)
{
    this->variable = variable;
    this->expr = expression;
    this->ast = ast;
}

EvaluationResult RuleLocalDecl::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    EvaluationResult e = expr->evaluateRule(programState, env, components);
    if(e.getIsRetVal())
        return e;
    programState.declareLocalVar(variable, e.getValue());
    return ValueResult(
                e.getValue(),
                e.getTime() + env.getTAssign(),
                e.getResourceAmount() + td_ec(components, env.getTAssign())
    );
}

vector<Rule*>* RuleLocalDecl::getChildren()
{
    return new vector<Rule*>({expr});
}

RuleLocalDecl::~RuleLocalDecl()
{
    //dtor
}

#include "Function.h"

Function::Function(string name, string argumentName, 
Rule* definition)
{
	this->name = name;
    this->argumentName = argumentName;
    this->definition = definition;
}

EvaluationResult Function::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    return definition->evaluateRule(programState, env, components);
}

string Function::getArgumentName()
{
    return argumentName;
}

string Function::getName()
{
	return name;
}

Function::~Function()
{
    //dtor
}


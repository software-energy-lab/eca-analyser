#include "LuaParser.h"

ASTParseResult ParseLuaMemory(const String &str,  MemoryPool& mp) {
    LuaLexer lexer(str, mp);
    auto p = ParserState<LuaLexer, LUA_LOOKAHEAD>(lexer);
    LuaLanguage<LuaLexer>::Program result;
    ParserError retval = Parser<LuaLexer, LUA_LOOKAHEAD, LuaLanguage<LuaLexer>::Program, MemoryPool&>(&p, mp)
            .perform(LuaLanguage<LuaLexer>::program)
            .end()
            .retreive(result);
    if (retval) {
        TokenBase *token = p.getToken();
        std::cout << "error: " << retval.error() << std::endl;
        std::cout << "token: " << token << std::endl;
        std::cout << "token representing: " << lexer.getInputString(token) << std::endl;
        //std::cout << "remaining: " << lexer.remaining() << std::endl;
        return {token ? token->start : 0, lexer.getInputString(token)};
    }
    // std::cout << "result of parse is " << result << std::endl;
    return {std::move(result)};
}

ASTParseResult ParseLuaFile(const StringT& filename, MemoryPool& mp) {
    struct stat st;
    if (stat(filename.c_str(), &st)) {
        std::cerr << "error: " << strerror(errno) << std::endl;
        return {0, "File not found (stat() error)"};
    }
    int fd = open(filename.c_str(), O_RDONLY
              #ifdef O_BINARY
                  | O_BINARY
              #endif
                  );
    if (!fd) {
        std::cerr << "error: " << strerror(errno) << std::endl;
        return {0, "Could not open file (open() error)"};
    }
    char *buffer = mp.allocBytes<char>(st.st_size);
    int size = read(fd, buffer, st.st_size);
    if (size != st.st_size) {
        std::cerr << "wrong number of bytes read: " << size << " instead of "
            << st.st_size << std::endl;
        abort();
    }
    close(fd);

    String current(buffer, size);

    auto result = ParseLuaMemory(current, mp);
    if (!result) {
        std::cerr << "error: " << result.error() << " at " << result.position() << std::endl;
    }
    return result;
}

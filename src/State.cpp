#include "State.h"

State::State()
{
	this->localState.push_back(map<string, Value*>());
}

State::State(map<string, Value*> state)
{
	this->localState.push_back(map<string, Value*>());
	this->globalState = state;
}

Value* State::lookup(string name)
{
	for(auto it = localState.rbegin(); it != localState.rend(); it++){
		auto result = it->find(name);
		if(result != it->end())
			return result->second;
	}

	auto result = globalState.find(name);
	if(result != globalState.end())
		return result->second;

	throw runtime_error("Exception: Program value '" + name + "' has not been declared.");
}

void State::declareGlobalVar(string name, Value* value)
{
	globalState[name] = value;
}

void State::declareLocalVar(string name, Value* value)
{
	localState.back()[name] = value;
}

bool State::localAssign(string name, Value* value)
{
	for(auto it = localState.rbegin(); it != localState.rend(); it++){
		auto result = it->find(name);
		if(result != it->end()){
			result->second = value;
			return true;
		}
	}
	return false;
}

bool State::exists(string name){
	for(auto it = localState.rbegin(); it != localState.rend(); it++){
		auto result = it->find(name);
		if(result != it->end()){
			return true;
		}
	}
	auto result = globalState.find(name);
	if(result != globalState.end())
		return true;
	return false;
}

void State::enterLocalScope()
{
	localState.push_back(map<string, Value*>());
}

void State::exitLocalScope()
{
	localState.pop_back();
}

void State::emptyLocalScopes()
{
	localState = vector<map<string, Value*>>( {map<string, Value*>()} );
}

vector<map<string, Value *> > State::getLocalState() const
{
	return localState;
}

void State::setLocalState(const vector<map<string, Value *> > &value)
{
	localState = value;
}

State::~State()
{}

#include "RuleFuncDef.h"
#include "FunctionEnvironment.h"

RuleFuncDef::RuleFuncDef(string functionname, string argumentname, Rule* left, Rule* right, const AST* ast)
{
    this->functionname = functionname;
    this->argumentname = argumentname;
    this->body = left;
    this->nextStatement = right;
    this->ast = ast;
}

EvaluationResult RuleFuncDef::evaluateRule(State& programState, FunctionEnvironment& env, map<string, Component*>& components)
{
    FunctionEnvironment envCopy = env;
    envCopy.addFunction(new Function(functionname, argumentname, body));
    EvaluationResult r = nextStatement->evaluateRule(programState, envCopy, components);
    if(r.getIsRetVal())
        return r;
    return ValueResult(nullptr, r.getTime(), r.getResourceAmount());
}

vector<Rule*>* RuleFuncDef::getChildren()
{
    return new vector<Rule*>({body, nextStatement});
}

RuleFuncDef::~RuleFuncDef()
{
    //dtor
}

#include "ValueFloat.h"

ValueFloat::ValueFloat(double value)
{
	this->value = value;
}

double ValueFloat::getValue() const
{
	return value;
}

ValueFloat::~ValueFloat(){}

#!/bin/bash
SOURCE=$HOME/dev/indigo
for file in {*.h,*.cpp}
do
		echo checking $file
    [ -f "$SOURCE/corelib/$file" ] && echo $file && cp $SOURCE/corelib/$file .
    for subdir in concurrency container crypto db io memory parser rx script streamingstore sync
    do
			[ -f "$SOURCE/corelib/include/$subdir/$file" ] && echo corelib/include/$subdir/$file && cp $SOURCE/corelib/include/$subdir/$file .
			[ -f "$SOURCE/corelib/src/$subdir/$file" ] && echo corelib/src/$subdir/$file && cp $SOURCE/corelib/src/$subdir/$file .
			[ -f "$SOURCE/lib/include/$subdir/$file" ] && echo lib/include/$subdir/$file && cp $SOURCE/lib/include/$subdir/$file .
			[ -f "$SOURCE/lib/src/$subdir/$file" ] && echo lib/src/$subdir/$file && cp $SOURCE/lib/src/$subdir/$file .
    done
done

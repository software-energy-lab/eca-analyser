/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

//#define DEBUG_EXCEPTIONS

#include "exception.h"
#include "simplestring.h"

namespace bitpowder {
namespace lib {

std::ostream& operator <<(std::ostream& out, const Exception& e) {
	out << "Exception " << e.err << " (" << strerror(e.err) << ")";
	if (e.where) {
		out << " [" << e.where;
		if (e.file)
			out << " in " << e.file << ":" << e.lineNo;
		out << "]";
	}
	if (e.desc.size() > 0)
		out << " due to " << e.desc;
	return out;
}

Exception::Exception(int _err) : desc(""), err(_err) {
#ifdef DEBUG_EXCEPTIONS
	std::cerr << *this << std::endl;
#endif
}

Exception::Exception(int _err, const char* _where, const char* _sourceFile, const int _lineNo) : where(_where), file(_sourceFile), lineNo(_lineNo), err(_err) {
#ifdef DEBUG_EXCEPTIONS
	std::cerr << *this << std::endl;
#endif
}

Exception::Exception(const char* _desc, int _err, const char* _where, const char* _sourceFile, const int _lineNo) : desc(_desc), where(_where), file(_sourceFile), lineNo(_lineNo), err(_err) {
#ifdef DEBUG_EXCEPTIONS
	std::cerr << *this << std::endl;
#endif
}

Exception::Exception(std::string&& _desc, int _err, const char* _where, const char* _sourceFile, const int _lineNo) : desc(std::move(_desc)), where(_where), file(_sourceFile), lineNo(_lineNo), err(_err) {
#ifdef DEBUG_EXCEPTIONS
	std::cerr << *this << std::endl;
#endif
}

Exception::Exception(StringOperation&& _desc, int _err, const char* _where, const char* _sourceFile, const int _lineNo) : desc(_desc), where(_where), file(_sourceFile), lineNo(_lineNo), err(_err) {
#ifdef DEBUG_EXCEPTIONS
	std::cerr << *this << std::endl;
#endif
}

Exception::Exception(const Exception& e) : desc(e.desc), where(e.where), file(e.file), lineNo(e.lineNo), err(e.err) {
}

}
}

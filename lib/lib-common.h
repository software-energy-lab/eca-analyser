/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

extern "C" {
#include <stdlib.h>
#include <stdio.h>
#if defined(_WIN32)
#include <io.h>
#else
#include <unistd.h>
#endif
#include <errno.h>
#include <string.h>

#if defined(_WIN32) || defined(__MINGW32__)
	inline int strncasecmp(const char* s1, const char* s2, size_t n) {
		return _strnicmp(s1, s2, n);
	}
#endif

#undef NULL
#define NULL nullptr

#if defined(__linux__)
#  include <arpa/inet.h>
#  include <endian.h>
#  define ntohll(x) be64toh(x)
#  define htonll(x) htobe64(x)
#elif defined(__FreeBSD__) || defined(__NetBSD__)
#  include <arpa/inet.h>
#  include <sys/endian.h>
#  define ntohll(x) be64toh(x)
#  define htonll(x) htobe64(x)
#elif defined(__OpenBSD__)
#  include <sys/types.h>
#  define ntohll(x) betoh64(x)
#  define htonll(x) htobe64(x)
#elif defined(__APPLE__)
#  include <arpa/inet.h>
#elif defined(_WIN32) || defined(__CYGWIN__)
#define IS_LITTLE_ENDIAN (((struct { union { unsigned int x; unsigned char c; }}){1}).c)
#define LITTLE_ENDIAN 1234
#define BIG_ENDIAN 4321
#ifdef IS_LITTLE_ENDIAN
#define BYTE_ORDER LITTLE_ENDIAN
#else
#define BYTE_ORDER BIG_ENDIAN
#endif
#  if BYTE_ORDER == LITTLE_ENDIAN
#    define ntohs(x) _byteswap_ushort(x)
#    define htons(x) _byteswap_ushort(x)
#    define ntohl(x) _byteswap_ulong(x)
#    define htonl(x) _byteswap_ulong(x)
#    define ntohll(x) _byteswap_uint64(x)
#    define htonll(x) _byteswap_uint64(x)
#  else
#    define ntohs(x) (x)
#    define htons(x) (x)
#    define ntohl(x) (x)
#    define htonl(x) (x)
#    define ntohll(x) (x)
#    define htonll(x) (x)
#  endif
#else
#  warning "no 64-bits ntoh/hton byte operations"
#endif

// not possible to define as sizeof(long)*8 because "#if __WORDSIZE == 64" is used
#ifndef __WORDSIZE
#if defined(_WIN64)
#define __WORDSIZE 64
#elif defined(_WIN32)
#define __WORDSIZE 32
#elif defined(__linux__)
// needed for Alpine Linux, as it uses uLibC
#include <sys/user.h>
#endif
#endif

}

#include <memory>
#include <chrono>
#include <functional>
#include <utility>
#include <type_traits>
#include <algorithm>
#include <tuple>
#include <cinttypes>

namespace bitpowder {
namespace lib {

#if defined(__clang__) || defined(__GNUC__)
#define FORMAT(x, y, z) __attribute__ ((format (x, y, z)))
#define USING(x) ((void)(x))
#define UNUSED __attribute__((unused))
#define WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#define NO_RETURN __attribute__((noreturn))
#define UNREACHABLE_CODE(x) \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Wunreachable-code\"") \
      x \
      _Pragma("clang diagnostic pop")
#define FLOAT_EQUAL(x) \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Wfloat-equal\"") \
      x \
      _Pragma("clang diagnostic pop")
#define SIGN_CONVERSION(x) \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Wsign-conversion\"") \
      x \
      _Pragma("clang diagnostic pop")
#define OLD_STYLE_CAST(x) \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Wold-style-cast\"") \
      x \
      _Pragma("clang diagnostic pop")
#define COMMA(x) \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Wcomma\"") \
      x \
      _Pragma("clang diagnostic pop")
// specifying every option by hand is needed for Clang 3.8 on Ubuntu 16.04 LTS
#define IGNORE_WARNINGS_START \
      _Pragma("clang diagnostic push") \
      _Pragma("clang diagnostic ignored \"-Weverything\"") \
      _Pragma("clang diagnostic ignored \"-Wshift-sign-overflow\"") \
      _Pragma("clang diagnostic ignored \"-Wmissing-noreturn\"") \
      _Pragma("clang diagnostic ignored \"-Wdeprecated\"") \
      _Pragma("clang diagnostic ignored \"-Wused-but-marked-unused\"") \
      _Pragma("clang diagnostic ignored \"-Wextra-semi\"") \
      _Pragma("clang diagnostic ignored \"-Wshorten-64-to-32\"") \
      _Pragma("clang diagnostic ignored \"-Wshadow\"") \
      _Pragma("clang diagnostic ignored \"-Wold-style-cast\"") \
      _Pragma("clang diagnostic ignored \"-Wsign-conversion\"") \
      _Pragma("clang diagnostic ignored \"-Wheader-hygiene\"") \

#define IGNORE_WARNINGS_END \
      _Pragma("clang diagnostic pop")

#else

#define FORMAT(x, y, z)
#define USING(x) ((void)(x))
#define UNUSED
#define WARN_UNUSED_RESULT
#define NO_RETURN
#define UNREACHABLE_CODE(x) x
#define FLOAT_EQUAL(x) x
#define SIGN_CONVERSION(x) x
#define OLD_STYLE_CAST(x) x
#define COMMA(x) x
// specifying every option by hand is needed for Clang 3.8 on Ubuntu 16.04 LTS
#define IGNORE_WARNINGS_START
#define IGNORE_WARNINGS_END
#endif

#if defined(__clang__)
#define FALLTHROUGH [[clang::fallthrough]]
#else
#define FALLTHROUGH [[gnu::fallthrough]]
#endif
#define UNREACHABLE __builtin_unreachable()


#ifdef EXTENDED_DEBUG
#define CRASH {*((int*)0x02) = 1;}
#else
#define CRASH
#endif

// invoke is alternative to mem_fun, because this is statically, so it interfaces better with C (e.g. thread.cpp / pthread_create).
// For the same reason uses less memory (8 bytes instead of 16 bytes of a std::mem_fn)
template <class T, void (T::*method)()>
inline void invoke() {
	T::method();
}

template <class T, void (T::*method)()>
inline void invoke(T* object) {
	(object->*method)();
}

template <class T, class A, void (T::*method)(A)>
inline void invoke(T* object, A arg) {
	(object->*method)(arg);
}

template <class T, class A, class B, void (T::*method)(A, B)>
inline void invoke(T* object, A arg, B arg2) {
	(object->*method)(arg, arg2);
}

template <class T, class A, class B, class C, void (T::*method)(A, B, C)>
inline void invoke(T object, A arg, B arg2, C arg3) {
	(object->*method)(arg, arg2, arg3);
}

template <class T>
inline void destroy(T* object) {
	object->~T();
}

template <class T>
inline void performDelete(T* object) {
	delete object;
}

template <class... Args>
inline void nothing(Args...) {
}

template <class T, class A>
inline void deleteSecond(T, A* snd) {
	delete snd;
}

#define INTERFACE(Class) \
    Class() = default; \
    Class(const Class&) = default; \
    Class& operator=(const Class&) = default; \
    virtual ~Class() = default;

#define INTERFACE_UNIQUE(Class) \
    Class() = default; \
    Class(const Class&) = delete; \
    Class& operator=(const Class&) = delete; \
    virtual ~Class() = default;

#define NO_COPY(Class) \
    Class(const Class&) = delete; \
    Class& operator=(const Class&) = delete;

#define NO_MOVE(Class) \
    Class(Class&&) = delete; \
    Class& operator=(Class&&) = delete;

struct evaluateHelper {
	evaluateHelper(std::initializer_list<int>) {
	}
};
#define evaluate(x) evaluateHelper{(x,1)...}

template <typename CallArgs, typename T, typename... Args, size_t... I>
void callMethodMoveHelper(void (T::*f)(Args...), T* t, CallArgs&& args, std::index_sequence<I...>) {
	(t->*f)(std::move(std::get<I>(args))...);
}
template <typename CallArgs, typename T, typename... Args>
void callMethodMove(void (T::*f)(Args...), T* t, CallArgs&& args) {
	callMethodMoveHelper<CallArgs, T, Args...>(f, t, std::move(args), std::make_index_sequence<std::tuple_size<CallArgs>::value> {});
}

template <typename Func, typename Args, size_t... I>
void callFunctionMoveHelper(Func&& f, Args&& args, std::index_sequence<I...>) {
	f(std::move(std::get<I>(args))...);
}
template <typename Func, typename... Args>
void callFunctionMove(Func&& f, std::tuple<Args...>&& args) {
	callFunctionMoveHelper(f, std::move(args), std::index_sequence_for<Args...> {});
}

template <typename Func, typename Args, size_t... I>
void callFunctionHelper(Func&& f, const Args& args, std::index_sequence<I...>) {
	f(std::get<I>(args)...);
}
template <typename Func, typename... Args>
void callFunction(Func&& f, const std::tuple<Args...>& args) {
	callFunctionHelper(f, args, std::index_sequence_for<Args...> {});
}

template <typename Func, typename Args, size_t... I>
auto evaluateFunctionHelper(Func&& f, const Args& args, std::index_sequence<I...>) {
	return f(std::get<I>(args)...);
}
template <typename Func, typename... Args>
auto evaluateFunction(Func&& f, const std::tuple<Args...>& args) {
	return evaluateFunctionHelper(f, args, std::index_sequence_for<Args...> {});
}

template <size_t... I, typename... Args>
std::tuple<Args& ...> convertToReferencesHelper(std::tuple<Args...>& args, std::index_sequence<I...>) {
	return std::tuple<Args& ...>(std::ref(std::get<I>(args))...);
}

template <typename... Args>
std::tuple<Args& ...> convertToReferences(std::tuple<Args...>& args) {
	return convertToReferencesHelper(args, std::index_sequence_for<Args...> {});
}



/**
  Usage:
        template <typename In>
        static typename In::type getArg() {
            return Type<typename In::type>::from(L, In::index);
        }
        template <typename... Args>
        static std::tuple<In...> convert(TypeWrapper<Args...> &&type) {
            return std::tuple<In...>(getArg<Args>()...);
        }

        convert(typename Zip<In>::type());
*/

template <int current, typename T>
struct ZippedNumber {
	typedef T type;
	static const int index = current;
};
template <typename... Args>
struct TypeWrapper {
};

template <typename T, typename... Args>
struct Unpack {
};
template <typename T, typename Tail, typename... Args>
struct Unpack<TypeWrapper<T, Tail>, Args...> : Unpack<Tail, T, Args...> {
};
template <typename... Args>
struct Unpack<TypeWrapper<>, Args...> {
	typedef TypeWrapper<Args...> type;
};

template <int current, typename Acc, typename... Args>
struct ZipNumbers {
};
template <int current, typename Acc, typename First, typename... Args>
struct ZipNumbers<current, Acc, First, Args...> : ZipNumbers < current + 1, TypeWrapper<ZippedNumber<current, First>, Acc>, Args... > {
};
template <int current, typename Acc, typename First>
struct ZipNumbers<current, Acc, First> {
	//typedef Reverse<Acc> type;
	typedef typename Unpack<TypeWrapper<ZippedNumber<current, First>, Acc>>::type type;
};

template <typename... Args>
struct Zip : ZipNumbers<0, TypeWrapper<>, Args...> {
};

template <>
struct Zip<> {
	typedef typename Unpack<TypeWrapper<>>::type type;
};




///// select only one value of the arguments and throw away the others
template <size_t N, class Arg, class... Args>
struct nth_wrap {
	static auto get(Arg, Args... args) -> decltype(nth_wrap < N - 1, Args... >::get(std::forward<Args>(args)...)) {
		return nth_wrap < N - 1, Args... >::get(std::forward<Args>(args)...);
	}
};

template <class Arg, class... Args>
struct nth_wrap<0, Arg, Args...> {
	static Arg get(Arg a, Args...) {
		return std::forward<Arg>(a);
	}
};

template <class Arg>
struct nth_wrap<0, Arg> {
	static Arg get(Arg a) {
		return std::forward<Arg>(a);
	}
};

template <size_t N, class... Args>
auto nth(Args... a) -> decltype(nth_wrap<N, Args...>::get(std::forward<Args>(a)...)) {
	return nth_wrap<N, Args...>::get(std::forward<Args>(a)...);
}



template <class T>
T hton(T n);

template <>
inline uint8_t hton<uint8_t>(uint8_t n) {
	return n;
}

template <>
inline uint16_t hton<uint16_t>(unsigned short n) {
	return OLD_STYLE_CAST(htons(n));
}

template <>
inline uint32_t hton<uint32_t>(uint32_t n) {
	return OLD_STYLE_CAST(htonl(n));
}

template <>
inline uint64_t hton<uint64_t>(uint64_t n) {
	return OLD_STYLE_CAST(htonll(n));
}


template <class T>
T ntoh(T n);

template <>
inline uint8_t ntoh<uint8_t>(uint8_t n) {
	return n;
}

template <>
inline uint16_t ntoh<uint16_t>(uint16_t n) {
	return OLD_STYLE_CAST(ntohs(n));
}

template <>
inline uint32_t ntoh<uint32_t>(uint32_t n) {
	return OLD_STYLE_CAST(ntohl(n));
}

template <>
inline uint64_t ntoh<uint64_t>(uint64_t n) {
	return OLD_STYLE_CAST(ntohll(n));
}


// overloaded lambda's
// overloaded {
//     [](auto arg) { std::cout << arg << ' '; },
//     [](double arg) { std::cout << std::fixed << arg << ' '; },
//     [](const std::string& arg) { std::cout << std::quoted(arg) << ' '; },
// }
// from: http://en.cppreference.com/w/cpp/utility/variant/visit
//template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
//template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

// http://stackoverflow.com/questions/4163126/dereferencing-type-punned-pointer-will-break-strict-aliasing-rules-warning
template<typename T, typename F>
struct alias_cast_t {
	union {
		F raw;
		T data;
	};
	alias_cast_t(F raw_data) : raw(raw_data) { }
	operator T() {
		return data;
	}
	static_assert(sizeof(F) == sizeof(T), "Cannot cast types of different sizes");
};
template<typename T, typename F>
T alias_cast(F raw_data) {
	return alias_cast_t<T, F> {raw_data};
}

struct DynamicCastableObject {
	INTERFACE(DynamicCastableObject)
};

}
}

#if defined(__has_feature)
#if __has_feature(thread_sanitizer)
#define TSAN_ENABLED
#endif
#endif

#ifdef TSAN_ENABLED
extern "C" void AnnotateThreadName(const char* f, int l, const char* name);
extern "C" void AnnotateHappensBefore(const char* f, int l, void* addr);
extern "C" void AnnotateHappensAfter(const char* f, int l, void* addr);
#define TSAN_ANNOTATE_THREAD_NAME(addr) AnnotateThreadName(__FILE__, __LINE__, addr)
#define TSAN_ANNOTATE_HAPPENS_BEFORE(addr) AnnotateHappensBefore(__FILE__, __LINE__, static_cast<void*>(addr))
#define TSAN_ANNOTATE_HAPPENS_AFTER(addr) AnnotateHappensAfter(__FILE__, __LINE__, static_cast<void*>(addr))
#else
#define TSAN_ANNOTATE_THREAD_NAME(name)
#define TSAN_ANNOTATE_HAPPENS_BEFORE(addr)
#define TSAN_ANNOTATE_HAPPENS_AFTER(addr)
#endif

// Enable thread safety attributes only with clang.
// The attributes can be safely erased when compiling with other compilers.
#if (!defined(THREAD_ANNOTATION_ATTRIBUTE__)) && defined(__clang__) && (!defined(SWIG))
#define THREAD_ANNOTATION_ATTRIBUTE__(x)   __attribute__((x))
#else
#define THREAD_ANNOTATION_ATTRIBUTE__(x)   // no-op
#endif

#define CAPABILITY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(capability(x))

#define SCOPED_CAPABILITY \
  THREAD_ANNOTATION_ATTRIBUTE__(scoped_lockable)

#define GUARDED_BY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(guarded_by(x))

#define PT_GUARDED_BY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(pt_guarded_by(x))

#define ACQUIRED_BEFORE(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(acquired_before(__VA_ARGS__))

#define ACQUIRED_AFTER(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(acquired_after(__VA_ARGS__))

#define REQUIRES(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(requires_capability(__VA_ARGS__))

#define REQUIRES_SHARED(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(requires_shared_capability(__VA_ARGS__))

#define ACQUIRE(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(acquire_capability(__VA_ARGS__))

#define ACQUIRE_SHARED(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(acquire_shared_capability(__VA_ARGS__))

#define RELEASE(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(release_capability(__VA_ARGS__))

#define RELEASE_SHARED(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(release_shared_capability(__VA_ARGS__))

#define TRY_ACQUIRE(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(try_acquire_capability(__VA_ARGS__))

#define TRY_ACQUIRE_SHARED(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(try_acquire_shared_capability(__VA_ARGS__))

#define EXCLUDES(...) \
  THREAD_ANNOTATION_ATTRIBUTE__(locks_excluded(__VA_ARGS__))

#define ASSERT_CAPABILITY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(assert_capability(x))

#define ASSERT_SHARED_CAPABILITY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(assert_shared_capability(x))

#define RETURN_CAPABILITY(x) \
  THREAD_ANNOTATION_ATTRIBUTE__(lock_returned(x))

#define NO_THREAD_SAFETY_ANALYSIS \
  THREAD_ANNOTATION_ATTRIBUTE__(no_thread_safety_analysis)

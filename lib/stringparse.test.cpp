/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stringparse.h"
#include <functional>
#include <iterator>

#include <map>
#include <string>
#include <ostream>
#include <iostream>

#include <math.h>

IGNORE_WARNINGS_START
#include <gtest/gtest.h>
IGNORE_WARNINGS_END

namespace tests {
using namespace bitpowder::lib;

TEST(StringParse, Accept) {
	EXPECT_TRUE(StringParser("Hello Hello").accept("Hello").span(Char<' '>()).accept("Hello").empty());
}

TEST(StringParse, Number) {
	int parsedInt = -1;
	EXPECT_TRUE(StringParser("number=0;something").accept("number=").number(parsedInt).accept(";something").empty());
	EXPECT_EQ(parsedInt, 0);
	// try to parse a letter
	EXPECT_FALSE(StringParser("number=a").accept("number=").number(parsedInt).empty());
	// on failure, the int is not modified
	EXPECT_EQ(parsedInt, 0);
}

TEST(StringParse, Opt) {
	int error = 0;
	StringParser::Optional optionalErrorFile;
	auto c = [&error, &optionalErrorFile](const String & str) -> bool {
		int errorCode;
		int errorFile;
		StringLength pos;
		return str.parse().accept("FAILED[").error(1).number(errorCode).error(2).begin(optionalErrorFile).accept(":").error(3).number(errorFile).error(4).end(optionalErrorFile).accept("]").error(5).position(pos, error);
	};
	EXPECT_TRUE(c("FAILED[-1:1]"));
	EXPECT_TRUE(optionalErrorFile);
	EXPECT_TRUE(c("FAILED[-1]"));
	EXPECT_FALSE(optionalErrorFile);
	EXPECT_FALSE(c("OK[]"));
	EXPECT_FALSE(optionalErrorFile);
	EXPECT_EQ(1, error);
	EXPECT_FALSE(c("FAILED[no-number]"));
	EXPECT_FALSE(optionalErrorFile);
	EXPECT_EQ(2, error);
	EXPECT_FALSE(c("FAILED[-1"));
	EXPECT_FALSE(optionalErrorFile);
	EXPECT_EQ(5, error);
	EXPECT_FALSE(c("FAILED[-1:lala]"));
	EXPECT_FALSE(optionalErrorFile);
	EXPECT_EQ(5, error);
}

TEST(StringParse, Jail) {
	String result = "build/bpjail:\n"
									"\tlibthr.so.3 => /lib/libthr.so.3 (0x800948000)\n"
									"\tlibiconv.so.2 => /usr/local/lib/libiconv.so.2 (0x800b70000)\n"
									"\tlibsqlite3.so.0 => /usr/local/lib/libsqlite3.so.0 (0x800e6b000)\n";
	const size_t expectedCount = 3;
	String expected[expectedCount] = {"/lib/libthr.so.3", "/usr/local/lib/libiconv.so.2", "/usr/local/lib/libsqlite3.so.0"};
	size_t current = 0;

	StringParser p = {result};
	p.search("\n");
	String line;
	auto space = C(' ') + C('\t');
	while (p.nonEmpty() && p.splitOn(line, C('\n'))) {
		String library;
		//std::cout << "line before: " << line << std::endl;
		if (line.parse().search("=>").span(space).splitOn(library, space + C('(')).remainder(line)) {
			//std::cout << "copy library dependency " << library << std::endl;
			//String libraryDir = library.dir();
			ASSERT_LE(current, expectedCount);
			EXPECT_EQ(expected[current], library);
			++current;
		}
		//std::cout << "line after : " << line << std::endl;
	}
	EXPECT_EQ(expectedCount, current);
}

}

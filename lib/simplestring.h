/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cctype>
#include <cmath>
#include <limits>
#include <cstddef>
#include <string>
#include <tuple>
#include <vector>
#include <exception>
#include "lib-common.h"
#include "memorypool.h"
#include "memory.h"
#include "shared_object.h"

#define STRING_TAGGED_POINTER

namespace bitpowder {
namespace lib {

class String;
class StringT;
class StringParser;
class RawWriter;

typedef size_t StringLength;
typedef ptrdiff_t SignedStringLength;
typedef unsigned int StringHashShort;
// mostly used for keys in hash maps, so long hash is of no additional (performance, validity) value. Short hash will save some memory. A long hash should be explicitly indicated.
typedef unsigned int StringHash;
typedef unsigned long long StringHashLong;
typedef unsigned int StringHashIgnoreCase;
typedef unsigned int StringStableHashShort;
typedef unsigned long long StringStableHashLong;

class StringUtil {
 public:
	static unsigned char FromHex(char _c) {
		unsigned char c = static_cast<unsigned char>(_c);
		if ((c >= '0') && (c <= '9')) {
			return c - '0';
		} else if ((c >= 'a') && (c <= 'f')) {
			return c - 'a' + 10;
		} else if ((c >= 'A') && (c <= 'F')) {
			return c - 'A' + 10;
		}
		return 0;
	}
	static unsigned char FromHex(char first, char second) {
		return static_cast<unsigned char>(FromHex(first) << 4) | FromHex(second);
	}

	inline static char ToSame(char c) {
		return c;
	}

	inline static char ToUpper(char c) {
		return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
	}

	inline static char ToLower(char c) {
		return (c >= 'A' && c <= 'Z') ? c - 'A' + 'a' : c;
	}

	static uint64_t HashSeed();
	static StringHashShort HashShort(const String& s);
	static StringHashLong HashLong(const String& s);
	static StringHash Hash(const String& s);
	static StringHashIgnoreCase HashIgnoreCase(const String& s);
	static StringStableHashShort StableHashShort(const String& s);
	static StringStableHashLong StableHashLong(const String& s);
};

template <class Content> class StringC;
using StringContainer = StringC<String>;
using StringContainerT = StringC<StringT>;

// Basic 'string'
class StringOperation {
 public:
	StringLength _size;

	explicit constexpr StringOperation(StringLength size = 0) : _size(size) {
	}
	virtual ~StringOperation() = default;
	StringOperation(const StringOperation&) = default;
	StringOperation(StringOperation&&) = default;
	inline StringLength size() const {
		return _size;
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const = 0;

	String operator()(MemoryPool& mp) const;
	StringT c_str(MemoryPool& mp) const;

	template <StringLength N> String operator()(char (&buffer)[N]) const;
	String operator()(char* buffer, StringLength size) const;

	template <StringLength N> StringT c_str(char (&buffer)[N]) const;
	StringT c_str(char* buffer, StringLength size) const;

	String to(char*& buffer, StringLength& size) const;
	String to(String& str) const;

	// some compile errors, like "lala" + String are harder to spot, as by autocasting this becomes "lala" + 1 -> "ala", instead of "lala"_S + String
	explicit operator bool() const {
		return _size > 0;
	}
	StringContainer c() const;
	StringContainerT ct() const;
	explicit operator std::string() const;
	std::string stl() const {
		return static_cast<std::string>(*this);
	}
	const StringOperation& op() const {
		return *this;
	}
	static StringLength writeToHelper(char*& buffer, StringLength& bufferLength, const void* src, StringLength len);
	static StringLength writeToHelper(char*& buffer, StringLength& bufferLength, const StringOperation& op);
	static StringLength writeToHelper(char*& buffer, StringLength& bufferLength, const String& str);
	static StringLength writeToHelper(char*& buffer, StringLength& bufferLength, const StringOperation& op, StringLength max);
	static StringLength writeToHelper(char*& buffer, StringLength& bufferLength, const String& str, StringLength max);
};

// support for String methods
template <typename A, typename B>
struct CharOr;

template <class T>
struct Complement {
	T t;
	constexpr Complement(const T& _t) : t(_t) {
	}
	constexpr inline void reset() const {
		t.reset();
	}
	constexpr inline bool operator()() const {
		return !t();
	}
	constexpr bool operator()(char in) const {
		return !t(in);
	}
	template <typename B>
	constexpr CharOr<Complement, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr T operator !() const {
		return t;
	}
};

template <typename A, typename B>
struct CharOr {
	A a;
	B b;
	constexpr CharOr() : a(), b() {
	}
	constexpr CharOr(const A& _a, const B& _b) : a(_a), b(_b) {
	}
	constexpr inline void reset() const {
		a.reset();
		b.reset();
	}
	constexpr inline bool operator()() const {
		return a() || b();
	}
	constexpr bool operator()(char in) const {
		return a(in) || b(in);
	}
	template <typename C>
	constexpr CharOr<CharOr, C> operator+(const C& c) const {
		return {*this, c};
	}
	constexpr Complement<CharOr> operator !() const {
		return {*this};
	}
};

template <class T>
struct CharFunction {
	T t;
	constexpr CharFunction(const T& _t) : t(_t) {
	}
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return t();
	}
	constexpr bool operator()(char in) const {
		return t(in);
	}
	template <typename B>
	constexpr CharOr<CharFunction, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<CharFunction> operator !() const {
		return {*this};
	}
};

template <char m>
struct Char {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return false;
	}
	constexpr inline bool operator()(char in) const {
		return in == m;
	}
	template <typename B>
	constexpr CharOr<Char, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Char> operator !() const {
		return {*this};
	}
};

struct Alpha {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return false;
	}
	constexpr inline bool operator()(char in) const {
		return ('a' <= in && in <= 'z') ||
					 ('A' <= in && in <= 'Z');
	}
	template <typename B>
	constexpr CharOr<Alpha, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Alpha> operator !() const {
		return {*this};
	}
};

struct Num {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return false;
	}
	constexpr inline bool operator()(char in) const {
		return ('0' <= in && in <= '9');
	}
	template <typename B>
	constexpr CharOr<Num, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Num> operator !() const {
		return {*this};
	}
};

struct Whitespace {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return false;
	}
	constexpr inline bool operator()(char in) const {
		return in == ' ' || in == '\t' || in == '\n' || in == '\r';
	}
	template <typename B>
	constexpr CharOr<Whitespace, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Whitespace> operator !() const {
		return {*this};
	}
};

struct Hex {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return false;
	}
	constexpr inline bool operator()(char in) const {
		return ('0' <= in && in <= '9') ||
					 ('a' <= in && in <= 'f') ||
					 ('A' <= in && in <= 'F');
	}
	template <typename B>
	constexpr CharOr<Hex, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Hex> operator !() const {
		return {*this};
	}
};

struct End {
	constexpr inline void reset() const {
	}
	constexpr inline bool operator()() const {
		return true;
	}
	constexpr inline bool operator()(char) const {
		return false;
	}
	template <typename B>
	constexpr CharOr<End, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<End> operator !() const {
		return {*this};
	}
};
template <class T>
struct Escapable {
	mutable bool wasEscape = false;
	T t;
	Escapable(const T& _t) : t(_t) {
	}
	constexpr inline void reset() const {
		wasEscape = false;
	}
	inline bool operator()() const {
		return t();
	}
	bool operator()(char in) const {
		bool retval = !wasEscape && t(in);
		wasEscape = !wasEscape && in == '\\';
		return retval;
	}
	template <typename B>
	CharOr<Escapable, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Escapable> operator !() const {
		return {*this};
	}
};
template <class S, class T>
struct Combination {
	mutable char prev = 0;
	S p;
	T c;
	Combination(const S& _p, const T& _c) : p(_p), c(_c) {
	}
	constexpr inline void reset() const {
		prev = 0;
		p.reset();
		c.reset();
	}
	inline bool operator()() const {
		bool retval = p(prev) && c();
		prev = 0;
		return retval;
	}
	bool operator()(char in) const {
		bool retval = p(prev) && c(in);
		prev = in;
		return retval;
	}
	template <typename B>
	CharOr<Combination, B> operator+(const B& b) const {
		return {*this, b};
	}
	constexpr Complement<Combination> operator !() const {
		return {*this};
	}
};

template <class T>
constexpr CharFunction<T> CharFunc(const T& t) {
	return {t};
}
template <class T>
constexpr Complement<T> CharComplement(const T& t) {
	return {t};
}
template <class T>
Escapable<T> Esc(const T& t) {
	return {t};
}
template <class S, class T>
Combination<S, T> Combi(const S& s, const T& t) {
	return {s, t};
}

#define C(c) Char<c>()

class StringOperationString;

template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
class Hashed;

// the main string
// no auto conversion to StringContainer or Hashed, so all expensive operations are explicit
class String {
	friend class StringOperation;
 public:
#pragma pack(push, 1)
	struct Values {
		StringLength len;
		inline StringLength length() const {
			return len;
		}
		bool operator==(const Values& rhs) const {
			return len == rhs.len;
		}
		bool operator<(const Values& rhs) const {
			return len < rhs.len;
		}
		bool operator>(const Values& rhs) const {
			return len > rhs.len;
		}
	};
#pragma pack(pop)
	struct Writer {
		static void Write(const StringOperation& str, char* buffer, StringLength length) {
			str(buffer, length);
		}
		static void Build(char* buffer UNUSED, StringLength length UNUSED) {
		}
		const static StringLength Extra = 0;
	};
 protected:
	const char* ptr = nullptr; // should be nullptr if length is zero
	StringLength len = 0UL;
 public:
	Values values() const {
		return {len};
	}
	constexpr static StringLength UNKNOWN_LENGTH = std::numeric_limits<StringLength>::max() - 1;
	constexpr static short NOT_FOUND = std::numeric_limits<short>::min();
	constexpr static short END = -1;
	constexpr inline String(const char* pointer, const Values& values) : ptr(pointer), len(values.len) {
		if (len == 0UL)
			ptr = nullptr;
		assert(len > 0UL || ptr == nullptr);
	}
	constexpr inline String(const char* pointer, StringLength length = UNKNOWN_LENGTH) : ptr(pointer), len(!pointer ? 0 : length < UNKNOWN_LENGTH ? length : strlen(pointer)) {
		if (len == 0UL)
			ptr = nullptr;
		assert(len > 0UL || ptr == nullptr);
	}
	String(const std::string& str) : String(str.c_str(), str.length()) {
	}
	constexpr inline String() {
	}
	String(std::nullptr_t) : String() {
	}
	template <StringLength N>
	constexpr inline String(char (&str)[N]) : String(str, N - 1) {
	}
	template <StringLength N>
	constexpr inline String(const char (&str)[N]) : String(str, N - 1) {
	}
	constexpr inline String(const char* begin, const char* end) : String(begin < end ? begin : nullptr, begin < end ? static_cast<StringLength>(end - begin) : 0) {
	}
	constexpr inline String(const String& c) : String(c.ptr, c.len) {
	}
	String& operator=(const String& str) {
		ptr = str.ptr;
		len = str.len;
		assert(len > 0UL || ptr == nullptr);
		return *this;
	}
	std::string stl() const;
	explicit operator std::string() const;
	StringContainer c() const;
	StringContainerT ct() const;
	StringParser parse() const;

	void clear() {
		ptr = nullptr;
		len = {0};
	}
	typedef const char* const_iterator;
	typedef const unsigned char* const_raw_iterator;
	inline const_iterator begin() const {
		return ptr;
	}
	inline const_iterator end() const {
		return ptr + uintptr_t(len);
	}
	inline const_raw_iterator rawbegin() const {
		return reinterpret_cast<const unsigned char*>(ptr);
	}
	inline const_raw_iterator rawend() const {
		return reinterpret_cast<const unsigned char*>(ptr) + uintptr_t(len);
	}
	struct RawIterators {
		const_raw_iterator b;
		const_raw_iterator e;
		const_raw_iterator begin() {
			return b;
		}
		const_raw_iterator end() {
			return e;
		}
	};
	RawIterators raw() const {
		return {rawbegin(), rawend()};
	}
	inline constexpr StringLength length() const {
		return len;
	}
	inline constexpr StringLength size() const {
		return len;
	}
	inline constexpr const char* pointer() const {
		return ptr;
	}
	inline bool empty() const {
		return !ptr || len == 0;
	}
	inline StringLength writeTo(char* buffer, StringLength bufferLength) const {
		StringLength l = std::min(len, bufferLength);
		memcpy(buffer, ptr, l);
		return l;
	}
	constexpr String substring(StringLength pos) const {
		return pos >= length() ? String() : String(pointer() + pos, length() - pos);
	}
	constexpr String substring(StringLength pos, StringLength length) const {
		return pos >= len ? String() : String(pointer() + pos, std::min(len - pos, length));
	}
	template <StringLength N>
	static StringLength printf(const char (&str)[N], const char* format, ...) {
		va_list ap;
		va_start(ap, format);
		int retval = snprintf(str, N, format, ap);
		va_end(ap);
		return StringLength(retval);
	}
	bool startsWith(const String& str) const {
		return str.len == 0 || (str.len <= len && memcmp(ptr, str.ptr, str.len) == 0);
	}
	bool startsWithIgnoreCase(const String& str) const {
		return str.len == 0 || (str.len <= len  && strncasecmp(ptr, str.ptr, str.len) == 0);
	}
	bool endsWith(const String& str) const {
		return str.len == 0 || (str.len <= len && memcmp(ptr + uintptr_t(len) - uintptr_t(str.len), str.ptr, str.len) == 0);
	}
	bool endsWithIgnoreCase(const String& str) const {
		return str.len == 0 || (str.len <= len && strncasecmp(ptr + uintptr_t(len) - uintptr_t(str.len), str.ptr, str.len) == 0);
	}
	// dir() can return "/" and "." depending on input
	String dir() const;
	// always returns first part, possible empty string
	String rawdir() const {
		String path;
		rsplitOn(C('/'), path);
		return path;
	}
	String withoutBase() const;
	String base() const;
	String extension() const;
	String trim(bool extra = false) const; // extra characters as tabs, newlines, etc

	template <typename T, class S = String>
	String span(const T& t, S* next = nullptr) const {
		t.reset();
		const char* p = pointer();
		assert(len == 0 || p);
		for (StringLength l = 0; l < len; ++l) {
			if (!t(p[l])) {
				String retval = substring(0, l);
				if (next)
					*next = substring(l);
				return retval;
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}
	template <typename T, class S = String>
	String span(const T& t, S& next) const {
		return span(t, &next);
	}
	template <typename T>
	String doSpan(const T& t) {
		return span<T>(t, this);
	}
	template <typename T, class S = String>
	String rspan(const T& t, S* next = nullptr) const {
		t.reset();
		const char* p = pointer();
		assert(len == 0 || p);
		for (StringLength l = len; l-- > 0; ) {
			if (!t(p[l])) {
				String retval = substring(l + 1);
				if (next)
					*next = substring(0, l + 1);
				return retval;
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}
	template <typename T, class S = String>
	String rspan(const T& t, S& next) const {
		return rspan(t, &next);
	}
	template <typename T>
	String doRSpan(const T& t) {
		return rspan<T>(t, this);
	}

	template <typename T, class S = String>
	String splitOn(const T& t, S& next, short* delimPtr = nullptr) const {
		return splitOn(t, &next, delimPtr);
	}
	template <typename T, class S = String>
	String splitOn(const T& t, S* next = nullptr, short* delimPtr = nullptr) const {
		t.reset();
		const char* p = pointer();
		assert(len == 0 || p);
		for (StringLength l = 0; l < len; ++l) {
			if (t(p[l])) {
				String retval = substring(0, l);
				if (delimPtr)
					*delimPtr = p[l];
				if (next)
					*next = substring(l + 1);
				return retval;
			}
		}
		String retval = *this;
		if (delimPtr)
			*delimPtr = t() ? END : NOT_FOUND;
		if (next)
			*next = String();
		return retval;
	}
	template <typename T>
	String doSplitOn(const T& t, short* delimPtr = nullptr) {
		return splitOn<T>(t, this, delimPtr);
	}

	template <typename T, class S = String>
	String rsplitOn(const T& t, S& next, short* delimPtr = nullptr) const {
		return rsplitOn(t, &next, delimPtr);
	}
	template <typename T, class S = String>
	String rsplitOn(const T& t, S* next = nullptr, short* delimPtr = nullptr) const {
		t.reset();
		const char* p = pointer();
		assert(len == 0 || p);
		for (StringLength l = len; l-- > 0; ) {
			if (t(p[l])) {
				String retval = substring(l + 1);
				if (delimPtr)
					*delimPtr = p[l];
				if (next)
					*next = substring(0, l);
				return retval;
			}
		}
		String retval = *this;
		if (delimPtr)
			*delimPtr = t() ? END : NOT_FOUND;
		if (next)
			*next = String();
		return retval;
	}
	template <typename T>
	String doRSplitOn(const T& t, short* delimPtr = nullptr) {
		return rsplitOn<T>(t, this, delimPtr);
	}

	String doSplit(const String& s);
	String doSplitIgnoreCase(const String& s);

	template <class S = String>
	String split(const String& s, S& next) const {
		return split(s, &next);
	}
	String split(const String& s) {
		return split<String>(s, nullptr);
	}
	template <class S = String>
	String split(const String& s, S* next = nullptr) const {
		StringLength sLen = s.length();
		if (!s.empty() && sLen <= len && ptr) {
			const char* sPtr = s.pointer();
			char sFirst = sPtr[0];
			StringLength end = len - sLen;
			for (StringLength l = 0; l <= end; ++l) {
				if (ptr[l] == sFirst && memcmp(&ptr[l], sPtr, sLen) == 0) {
					String retval = substring(0, l);
					if (next)
						*next = substring(l + sLen);
					return retval;
				}
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}

	template <class S = String>
	String splitIgnoreCase(const String& s, S& next) const {
		return splitIgnoreCase(s, &next);
	}
	String splitIgnoreCase(const String& s) {
		return splitIgnoreCase<String>(s, nullptr);
	}
	template <class S = String>
	String splitIgnoreCase(const String& s, S* next = nullptr) const {
		StringLength sLen = s.length();
		if (!s.empty() && sLen <= len && ptr) {
			const char* sPtr = s.pointer();
			char sFirst = StringUtil::ToLower(sPtr[0]);
			StringLength end = len - sLen;
			for (StringLength l = 0; l <= end; ++l) {
				if (StringUtil::ToLower(ptr[l]) == sFirst && strncasecmp(&ptr[l], sPtr, sLen) == 0) {
					String retval = substring(0, l);
					if (next)
						*next = substring(l + sLen);
					return retval;
				}
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}

	String doRSplit(const String& s);
	String doRSplitIgnoreCase(const String& s);

	template <class S = String>
	String rsplit(const String& s, S& next) const {
		return rsplit(s, &next);
	}
	template <class S = String>
	String rsplit(const String& s, S* next = nullptr) const {
		StringLength sLen = s.length();
		if (!s.empty() && sLen <= len && ptr) {
			const char* sPtr = s.pointer();
			char sFirst = sPtr[0];
			for (StringLength l = len - sLen + 1; l-- > 0; ) {
				if (ptr[l] == sFirst && memcmp(&ptr[l], sPtr, sLen) == 0) {
					String retval = substring(l + sLen);
					if (next)
						*next = substring(0, l);
					return retval;
				}
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}

	template <class S = String>
	String rsplitIgnoreCase(const String& s, S& next) const {
		return rsplitIgnoreCasE(s, &next);
	}
	template <class S = String>
	String rsplitIgnoreCase(const String& s, S* next = nullptr) const {
		StringLength sLen = s.length();
		if (!s.empty() && sLen <= len && ptr) {
			const char* sPtr = s.pointer();
			for (StringLength l = len - sLen; l-- > 0; ) {
				if (strncasecmp(&ptr[l], sPtr, sLen) == 0) {
					String retval = substring(l + sLen);
					if (next)
						*next = substring(0, l);
					return retval;
				}
			}
		}
		String retval = *this;
		if (next)
			*next = String();
		return retval;
	}

	template <typename P>
	bool containsOn(const P& p) const {
		p.reset();
		for (auto c : *this)
			if (p(c))
				return true;
		return false;
	}
	template <typename P>
	bool containsOnIgnoreCase(const P& p) const {
		p.reset();
		for (auto c : *this)
			if (p(StringUtil::ToLower(c)))
				return true;
		return false;
	}
	bool contains(const String& str) const;
	bool containsIgnoreCase(const String& str) const;
	char operator[](StringLength off) const {
		return ptr[off];
	}

	enum Escape {
		BACKSLASH, // backslash is used to escape certain delimiters as used in StringParser, see Esc()
		LikeC,
		JSON,
		URL,
		SQLITE,
		HTML
	};

	String unescape(Escape escape, MemoryPool& mp) const;
	String escape(Escape escape, MemoryPool& mp) const;
	void escape(Escape escape, RawWriter& writer) const;
	size_t lengthOfEscape(Escape escape) const;
	String base64decode(MemoryPool& mp) const;

	bool equalsIgnoreCase(const String& b) const;
 private:
	static bool lowerless(char a, char b) {
		return uint8_t(StringUtil::ToLower(a)) < uint8_t(StringUtil::ToLower(b));
	}
	static bool lowergreater(char a, char b) {
		return uint8_t(StringUtil::ToLower(a)) > uint8_t(StringUtil::ToLower(b));
	}
 public:
	bool lessIgnoreCase(const String& b) const {
		if (length() < b.length())
			return true;
		if (length() != b.length())
			return false;
		return pointer() != b.pointer() && std::lexicographical_compare(begin(), end(), b.begin(), b.end(), lowerless);
	}
	bool greaterIgnoreCase(const String& b) const {
		if (length() > b.length())
			return true;
		if (length() != b.length())
			return false;
		return pointer() != b.pointer() && std::lexicographical_compare(begin(), end(), b.begin(), b.end(), lowergreater);
	}
	bool lessIgnoreCaseAlpha(const String& b) const {
		if (pointer() == b.pointer() && length() == b.length())
			return false;
		return std::lexicographical_compare(begin(), end(), b.begin(), b.end(), lowerless);
	}
	bool greaterIgnoreCaseAlpha(const String& b) const {
		if (pointer() == b.pointer() && length() == b.length())
			return false;
		return std::lexicographical_compare(begin(), end(), b.begin(), b.end(), lowergreater);
	}
	bool lessAlpha(const String& b) const {
		if (pointer() == b.pointer() && length() == b.length())
			return false;
		return std::lexicographical_compare(begin(), end(), b.begin(), b.end(), std::less<char>());
	}
	bool greaterAlpha(const String& b) const {
		if (pointer() == b.pointer() && length() == b.length())
			return false;
		return std::lexicographical_compare(begin(), end(), b.begin(), b.end(), std::greater<char>());
	}

	int compareUsingVersion(const String& rhs) const;

	template <typename T, int _base = 10, class S = String>
	inline T toNumber(S* next = nullptr) const {
		if (length() == 0) {
			if (next)
				*next = *this;
			return 0;
		}
		T result = 0;
		bool isNegative = ptr[0] == '-';
		StringLength i = isNegative;
		StringLength l = length();
		while (i < l &&
					 ((ptr[i] >= '0' && ptr[i] <= '9')
						|| (_base > 10 && ptr[i] >= 'a' && ptr[i] <= 'z')
						|| (_base > 10 && ptr[i] >= 'A' && ptr[i] <= 'Z')
					 )) {
			result *= _base;
			if ((ptr[i] >= '0') && (ptr[i] <= '9')) {
				result += T(ptr[i] - '0');
			} else if ((ptr[i] >= 'a') && (ptr[i] <= 'z')) {
				result += T(ptr[i] - 'a' + 10);
			} else if ((ptr[i] >= 'A') && (ptr[i] <= 'Z')) {
				result += T(ptr[i] - 'A' + 10);
			}
			++i;
		}
		if constexpr (std::is_floating_point<T>::value) {
			if (i < l && ptr[i] == '.') {
				++i;
				T afterPoint = 1;
				while (i < l &&
							 ((ptr[i] >= '0' && ptr[i] <= '9')
								|| (_base > 10 && ptr[i] >= 'a' && ptr[i] <= 'z')
								|| (_base > 10 && ptr[i] >= 'A' && ptr[i] <= 'Z')
							 )) {
					result *= _base;
					if ((ptr[i] >= '0') && (ptr[i] <= '9')) {
						result += T(ptr[i] - '0');
					} else if ((ptr[i] >= 'a') && (ptr[i] <= 'z')) {
						result += T(ptr[i] - 'a' + 10);
					} else if ((ptr[i] >= 'A') && (ptr[i] <= 'Z')) {
						result += T(ptr[i] - 'A' + 10);
					}
					++i;
					afterPoint *= _base;
				}
				result /= afterPoint;
			}
			if (i > (isNegative ? 1 : 0) && i < l && StringUtil::ToLower(ptr[i]) == 'e') { // minimal one other char read, to avoid matching strings that start with "e..."
				++i;
				int e = 0;
				while (i < l &&
							 ((ptr[i] >= '0' && ptr[i] <= '9')
								|| (_base > 10 && ptr[i] >= 'a' && ptr[i] <= 'z')
								|| (_base > 10 && ptr[i] >= 'A' && ptr[i] <= 'Z')
							 )) {
					e *= _base;
					if ((ptr[i] >= '0') && (ptr[i] <= '9')) {
						e += T(ptr[i] - '0');
					} else if ((ptr[i] >= 'a') && (ptr[i] <= 'z')) {
						e += T(ptr[i] - 'a' + 10);
					} else if ((ptr[i] >= 'A') && (ptr[i] <= 'Z')) {
						e += T(ptr[i] - 'A' + 10);
					}
					++i;
				}
				result *= std::pow(T(10), T(e));
			}
		}
		if (next)
			*next = i > (isNegative ? 1 : 0) ? substring(i) : *this;
		return isNegative ? -result : result;
	}
	template <typename T, int _base = 10, class S = String>
	inline T toNumber(S& next) const {
		return toNumber<T, _base>(&next);
	}

	template <typename T, class S = String>
	inline T hexToNumber(S* next = nullptr) const {
		return toNumber<T, 16>(next);
	}
	template <typename T, class S = String>
	inline T hexToNumber(S& next) const {
		return hexToNumber<T>(&next);
	}

	template <typename T, int _base = 64, class S = String>
	inline T maxToNumber(S* next = nullptr) const {
		static_assert(std::is_integral<T>::value, "no floating points are supported");
		if (length() == 0) {
			if (next)
				*next = *this;
			return 0;
		}
		T result = 0;
		bool isNegative = ptr[0] == '-';
		StringLength i = isNegative;
		StringLength size = length();
		while (i < size &&
					 ((ptr[i] >= '0' && ptr[i] <= '9')
						|| (_base > 10 && ptr[i] >= 'a' && ptr[i] <= 'z')
						|| (_base > 36 && ptr[i] >= 'A' && ptr[i] <= 'Z')
						|| (_base > 62 && ptr[i] == '_')
						|| (_base > 63 && ptr[i] == '.')
					 )) {
			result *= _base;
			if ((ptr[i] >= '0') && (ptr[i] <= '9')) {
				result += T(ptr[i] - '0');
			} else if ((ptr[i] >= 'a') && (ptr[i] <= 'z')) {
				result += T(ptr[i] - 'a' + 10);
			} else if ((ptr[i] >= 'A') && (ptr[i] <= 'Z')) {
				result += T(ptr[i] - 'A' + 36);
			} else if (ptr[i] == '_') {
				result += 62;
			} else if (ptr[i] == '.') {
				result += 63;
			}
			i++;
		}
		if (next)
			*next = i > StringLength(isNegative) ? substring(i) : *this;
		return isNegative ? -result : result;
	}

	template <typename T, class S = String>
	inline T maxToNumber(S& next) const {
		return maxToNumber<T, 64>(&next);
	}

	template <typename T>
	inline T doToNumber() {
		return toNumber<T, 10>(this);
	}
	template <typename T>
	inline T doHexToNumber() {
		return hexToNumber<T>(this);
	}
	template <typename T>
	inline T doMaxToNumber() {
		return maxToNumber<T>(this);
	}

	String append(const StringOperation& a, MemoryPool& mp) const;
	String append(const String& a, MemoryPool& mp) const;

	bool tryAppend(const StringOperation& a, MemoryPool& mp);
	bool tryAppend(const String& a, MemoryPool& mp);

	template <class F>
	String filter(const F& f, MemoryPool& mp) const {
		f.reset();
		StringLength newSize = 0;
		for (char c : *this)
			if (f(c))
				newSize++;
		f.reset();
		char* retval = mp.template allocBytes<char>(newSize);
		newSize = 0;
		for (char c : *this)
			if (f(c))
				retval[newSize++] = c;
		return {retval, newSize};
	}

	explicit operator bool() const {
		return len > 0;
	}

	StringOperationString op() const;
	operator StringOperationString() const;
	template <class StringHashType, StringHashType (*Hash)(const String& s)>
	explicit operator Hashed<String, StringHashType, Hash>() const {
		return {*this};
	}

	String operator()(MemoryPool& mp) const;
	StringT c_str(MemoryPool& mp) const;
	template <StringLength N> String operator()(char (&buffer)[N]) const;
	template <StringLength N> StringT c_str(char (&buffer)[N]) const;
	String operator()(char* buffer, StringLength size) const;
	String to(char*& buffer, StringLength& size) const;
	String string() const {
		return *this;
	}

	// presumes input is ascii/binary
	size_t sizeOfUTF8() const;
	String toUTF8(MemoryPool& mp) const;
	void toUTF8(RawWriter& writer) const;

	// presumes input is UTF8, chars other as U+0000 - U+00FF are dropped
	size_t sizeOfASCII() const;
	String toASCII(MemoryPool& mp) const;
	void toASCII(RawWriter& writer) const;

	StringContainer replace(const String& toBeReplaced, const String& replaceWith) const;
	String replace(const String& toBeReplaced, const String& replaceWith, MemoryPool& mp) const;
};

class StringOperationString : public StringOperation {
	String str;
 public:
	constexpr StringOperationString(const String& string) : StringOperation(string.size()), str(string) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		return str.writeTo(buffer, bufferLength);
	}
};

inline StringOperationString String::op() const {
	return StringOperationString{*this};
}
inline String::operator StringOperationString() const {
	return StringOperationString{*this};
}

// terminating string
class StringT : public String {
 public:
	struct Writer {
		static void Write(const StringOperation& str, char* buffer, StringLength length) {
			str.c_str(buffer, length);
		}
		static void Build(char* buffer, StringLength length) {
			buffer[length] = '\0';
		}
		const static StringLength Extra = 1;
	};
	constexpr inline StringT(const char* ptr, const Values& values) : String(ptr, values) {
	}
	constexpr inline StringT(const char* ptr, StringLength len = std::numeric_limits<StringLength>::max()) : String(ptr, len) {
	}
	inline StringT(const std::string& s) : String(s.c_str(), s.size()) {
	}
	constexpr inline StringT() : String() {
	}
	template <StringLength N>
	constexpr inline StringT(const char (&str)[N]) : String(str) {
	}

	StringT c_str(MemoryPool& mp) const {
		return String::c_str(mp);
	}
	const char* c_str() const {
		return pointer();
	}
	StringT base() const {
		auto base = String::base();
		return {base.pointer(), base.length()};
	}
};

template <StringLength N> String String::operator ()(char (&buffer)[N]) const {
	return op()(buffer);
}
template <StringLength N> StringT String::c_str(char (&buffer)[N]) const {
	return op().c_str(buffer);
}

// operators on strings
inline bool operator==(const String& lhs, const String& rhs) {
	return lhs.length() == rhs.length() && (lhs.pointer() == rhs.pointer() || memcmp(lhs.pointer(), rhs.pointer(), lhs.length()) == 0);
}
inline bool operator<(const String& lhs, const String& rhs) {
	if (lhs.length() < rhs.length())
		return true;
	if (lhs.length() != rhs.length())
		return false;
	if (lhs.pointer() == rhs.pointer())
		return false;
	return memcmp(lhs.pointer(), rhs.pointer(), lhs.length()) < 0;
}
inline bool operator>(const String& lhs, const String& rhs) {
	if (lhs.length() > rhs.length())
		return true;
	if (lhs.length() != rhs.length())
		return false;
	if (lhs.pointer() == rhs.pointer())
		return false;
	return memcmp(lhs.pointer(), rhs.pointer(), lhs.length()) > 0;
}

inline bool operator>=(const String& lhs, const String& rhs) {
	return !(lhs < rhs);
}
inline bool operator<=(const String& lhs, const String& rhs) {
	return !(lhs > rhs);
}
inline bool operator!=(const String& lhs, const String& rhs) {
	return !(lhs == rhs);
}

template <class S = String, class StringHashType = StringHash, StringHashType (*Hash)(const String& s) = StringUtil::Hash>
class Hashed {
	template <class S2, class StringHashType2, StringHashType2 (*Hash2)(const String& s)>
	friend class Hashed;
	S s;
	StringHashType hashCache;
 public:
#pragma pack(push, 1)
	struct Values {
		typename S::Values parent;
		StringHashType hashCache;
		inline auto length() const {
			return parent.length();
		}
		bool operator==(const Values& rhs) const {
			return hashCache == rhs.hashCache && parent == rhs.parent;
		}
		bool operator<(const Values& rhs) const {
			if (hashCache < rhs.hashCache)
				return true;
			if (hashCache != rhs.hashCache)
				return false;
			return parent < rhs.parent;
		}
		bool operator>(const Values& rhs) const {
			if (hashCache > rhs.hashCache)
				return true;
			if (hashCache != rhs.hashCache)
				return false;
			return parent > rhs.parent;
		}
	};
#pragma pack(pop)
	typedef typename S::Writer Writer;
	Values values() const {
		return {s.values(), hashCache};
	}
	Hashed() : hashCache(Hash(String())) {
	}
	Hashed(const char* ptr, const Values& values) : s(ptr, values.parent), hashCache(values.hashCache) {
	}
	Hashed(const S& _s, StringHashType _hashCode) : s(_s), hashCache(_hashCode) {
	}
	explicit Hashed(const StringT& _s) : Hashed(_s, Hash(_s)) {
	}
	template <StringLength N>
	Hashed(const char (&str)[N]) : Hashed(StringT(str)) {
	}
	template <bool enabled = std::is_same<String, S>::value>
	explicit Hashed(const S& _s, typename std::enable_if<enabled, Hashed>::type* = nullptr) : Hashed(_s, Hash(_s)) {
	}
	Hashed(const Hashed<StringT, StringHashType, Hash>& _s) : s(_s.string()), hashCache(_s.hash()) {
	}
	template <bool enabled = std::is_same<String, S>::value>
	Hashed(const Hashed& _s, typename std::enable_if<enabled, Hashed>::type* = nullptr) : s(_s.s), hashCache(_s.hashCache) {
	}

	inline StringHashType hash() const {
		return hashCache;
	}
	Hashed& operator=(const S& _s) {
		s = _s;
		hashCache = Hash(s);
		return *this;
	}
	template <class T>
	Hashed& operator=(const Hashed<T, StringHashType, Hash>& rhs) {
		s = rhs.s;
		hashCache = rhs.hashCache;
		return *this;
	}
	template <StringLength N>
	Hashed& operator=(const char (&str)[N]) {
		return *this = StringT(str);
	}
	operator S() const {
		return s;
	}
	explicit operator bool() const {
		return bool(string());
	}
	S* operator->() {
		return &s;
	}
	const S* operator->() const {
		return &s;
	}
	StringLength length() const {
		return s.length();
	}
	S string() const {
		return s;
	}
	StringOperationString op() const {
		return string().op();
	}
	operator StringOperationString() const {
		return string().op();
	}
	StringC<Hashed> c() const {
		return StringC<Hashed>(*this);
	}
	template <template<typename> class Equal = std::equal_to>
	bool equals(const Hashed<String, StringHashType, Hash>& b, Equal<String> compare = Equal<String>()) const {
		return hashCache == b.hash() && compare(s, b.string());
	}
	template <template<typename> class Less = std::less>
	bool less(const Hashed<String, StringHashType, Hash>& b, Less<String> compare = Less<String>()) const {
		if (b->size() == 0)
			return true;
		if (s.size() == 0)
			return false;
		if (hashCache != b.hash())
			return hashCache < b.hash();
		return compare(s, b.string());
	}
	template <template<typename> class Greater = std::greater>
	bool greater(const Hashed<String, StringHashType, Hash>& b, Greater<String> compare = Greater<String>()) const {
		if (b->size() == 0)
			return false;
		if (s.size() == 0)
			return true;
		if (hashCache != b.hashCache)
			return hashCache > b.hashCache;
		return compare(s, b.string());
	}
	Hashed operator()(MemoryPool& mp) const {
		return {s(mp), hashCache};
	}
	bool empty() const {
		return s.empty();
	}
};

template <class S = String>
using StableHashed = Hashed<S, StringStableHashShort, StringUtil::StableHashShort>;

template <class S = String>
using HashedIgnoreCase = Hashed<S, StringHashIgnoreCase, StringUtil::HashIgnoreCase>;

template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator<(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return lhs.less(rhs);
}

template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator>(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return lhs.greater(rhs);
}

template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator==(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return lhs.hash() == rhs.hash() && lhs.string() == rhs.string();
}

template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator>=(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return !(lhs < rhs);
}
template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator<=(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return !(lhs > rhs);
}
template <class StringHashType, StringHashType (*Hash)(const String& s), class LHS, class RHS>
inline bool operator!=(const Hashed<LHS, StringHashType, Hash>& lhs, const Hashed<RHS, StringHashType, Hash>& rhs) {
	return !(lhs == rhs);
}

// COMPARISONS classes derived from default
template <class S = String>
struct LessIgnoreCase {
	bool operator()(const S& lhs, const S& rhs) const {
		return String(lhs).lessIgnoreCase(String(rhs));
	}
	typedef void is_transparent;
};
template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
struct LessIgnoreCase<Hashed<S, StringHashType, Hash>> {
	bool operator()(const Hashed<S, StringHashType, Hash>& lhs, const Hashed<S, StringHashType, Hash>& rhs) const {
		return lhs.less(rhs, LessIgnoreCase<S>());
	}
};
template <class Content>
struct LessIgnoreCase<StringC<Content>> : public LessIgnoreCase<Content> {
	typedef void is_transparent;
};
struct LessAlpha {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		return String(lhs).lessAlpha(String(rhs));
	}
	typedef void is_transparent;
};
struct LessIgnoreCaseAlpha {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		return String(lhs).lessIgnoreCaseAlpha(String(rhs));
	}
	typedef void is_transparent;
};
struct LessVersion {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		int retval = String(lhs).compareUsingVersion(String(rhs));
		return retval < 0;
	}
	typedef void is_transparent;
};

template <class S = String>
struct GreaterIgnoreCase {
	bool operator()(const S& lhs, const S& rhs) const {
		return String(lhs).greaterIgnoreCase(String(rhs));
	}
	typedef void is_transparent;
};
template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
struct GreaterIgnoreCase<Hashed<S, StringHashType, Hash>> {
	bool operator()(const Hashed<S, StringHashType, Hash>& lhs, const Hashed<String, StringHashType, Hash>& rhs) const {
		return lhs.greater(rhs, GreaterIgnoreCase<S>());
	}
};
template <class Content>
struct GreaterIgnoreCase<StringC<Content>> : public GreaterIgnoreCase<Content> {
	typedef void is_transparent;
};
struct GreaterAlpha {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		return String(lhs).greaterAlpha(String(rhs));
	}
	typedef void is_transparent;
};
struct GreaterVersion {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		int retval = String(lhs).compareUsingVersion(String(rhs));
		return retval > 0;
	}
};
template <class S = String>
struct EqualIgnoreCase {
	template <class LHS, class RHS>
	bool operator()(const LHS& lhs, const RHS& rhs) const {
		return String(lhs).equalsIgnoreCase(String(rhs));
	}
	typedef void is_transparent;
};
template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
struct EqualIgnoreCase<Hashed<S, StringHashType, Hash>> {
	bool operator()(const Hashed<S, StringHashType, Hash>& lhs, const Hashed<S, StringHashType, Hash>& rhs) const {
		return lhs.template equals<EqualIgnoreCase<S>>(rhs);
	}
};

// STRING CONTAINER
#pragma pack(push, 1)
template <class Content>
struct StringContainerSharedContent {
	typename Content::Values contentValues;
	concurrent_count refcount = 0; // 4 bytes

	void check() {
		static_assert(std::is_standard_layout<StringContainerSharedContent>::value, "StringContainerSharedContent should have a standard layout so it will use less memory (as the location of the 'bytes' attribute is known this memory can be used");
	}
	StringContainerSharedContent() {
		check();
	}
	StringContainerSharedContent(const Content& str, StringLength length) : contentValues(str.values()) {
		check();
		Content::Writer::Write(str, bytes, length);
	}
	StringContainerSharedContent(const StringOperation& op, StringLength length) {
		check();
		Content::Writer::Write(op, bytes, length);
		contentValues = Content(StringT(bytes, op.size())).values();
	}
	void build(StringLength length) {
		Content::Writer::Build(bytes, length);
		contentValues = Content(StringT(bytes, length)).values();
	}

	typedef shared_object<StringContainerSharedContent> Ref;

	static Ref Create(const StringOperation& op) {
		StringLength needed = op.size() + Content::Writer::Extra;
		return new(ExtraMemory(needed)) StringContainerSharedContent(op, needed);
	}
	static Ref Create(const Content& str) {
		StringLength needed = str.length() + Content::Writer::Extra;
		return new(ExtraMemory(needed)) StringContainerSharedContent(str, needed);
	}
	static Ref Create(StringLength bytes) {
		StringLength needed = bytes + Content::Writer::Extra;
		return new(ExtraMemory(needed)) StringContainerSharedContent();
	}

	inline Content content() const {
		return {bytes, contentValues};
	}
	inline typename Content::Values values() const {
		return contentValues;
	}
	inline StringLength length() const {
		return contentValues.length();
	}
	inline const char* pointer() const {
		return bytes;
	}

	static StringLength SizeWithMaxAllocation(StringLength max) {
		return max <= sizeof(StringContainerSharedContent) ? 0 : max - sizeof(StringContainerSharedContent) - Content::Writer::Extra;
	}

	bool operator==(const StringContainerSharedContent& rhs) const {
		return contentValues == rhs.contentValues && String(bytes, length()) == String(rhs.bytes, rhs.length());
	}

	char bytes[0];
};
#pragma pack(pop)


// does not support auto conversion to String to avoid wrong comparisons (not based on hash value)
template <class Content>
class StringC {
	static const Content empty;
	typename StringContainerSharedContent<Content>::Ref ptr;
 public:
	typedef Content ContentType;
	StringC(typename StringContainerSharedContent<Content>::Ref&& _ptr) : ptr(std::move(_ptr)) {
	}
	class Builder {
		typename StringContainerSharedContent<Content>::Ref ptr;
		StringLength len;
		Builder(typename StringContainerSharedContent<Content>::Ref _ptr, StringLength _len) : ptr(_ptr), len(_len) {
		}
	 public:
		static Builder CreateWithMax(StringLength max) {
			return CreateWithSize(StringContainerSharedContent<Content>::SizeWithMaxAllocation(max));
		}
		static Builder CreateWithSize(StringLength bytes) {
			return {bytes > 0 ? StringContainerSharedContent<Content>::Create(bytes) : nullptr, bytes};
		}
		char* pointer() {
			return ptr ? const_cast<char*>(ptr->pointer()) : nullptr;
		}
		StringLength length() {
			return len;
		}
		void limit(StringLength limit) {
			if (limit < len)
				len = limit;
		}
		StringC<Content> build() {
			ptr->build(len);
			return std::move(ptr);
		}
		RawWriter writer();
	};
	StringC() {
	}
	StringC(const StringC& str) = default;
	explicit StringC(const StringOperation& string) : ptr(string.size() > 0 ? StringContainerSharedContent<Content>::Create(string) : nullptr) {
	}
	explicit StringC(const Content& string) : ptr(string ? StringContainerSharedContent<Content>::Create(string) : nullptr) {
	}

	StringC& operator=(const StringC& string) = default;
	StringC& operator=(const Content& string) {
		ptr = string.length() > 0 ? StringContainerSharedContent<Content>::Create(string) : nullptr;
		return *this;
	}
	StringC& operator=(const std::nullptr_t&) {
		ptr = nullptr;
		return *this;
	}
	StringC& operator=(const StringOperation& string) {
		ptr = string.size() > 0 ? StringContainerSharedContent<Content>::Create(string) : nullptr;
		return *this;
	}
	inline operator Content() const {
		return ptr ? ptr->content() : empty;
	}
	explicit inline operator bool() const {
		return length() > 0;
	}
	inline bool emptyReference() const {
		return ptr.empty();
	}
	inline auto string() const {
		return content().string();
	}
	inline Content content() const {
		return ptr ? ptr->content() : empty;
	}
	inline typename Content::Values values() const {
		return ptr ? ptr->values() : empty.values();
	}
	inline Content operator*() const {
		return content();
	}
	inline StringOperationString op() const {
		return content().op();
	}
	inline operator StringOperationString() const {
		return content().op();
	}
	inline void clear() {
		ptr = nullptr;
	}
	size_t allocated_size() const {
		return allocation_size(ptr.get());
	}
	inline StringLength length() const {
		return ptr ? ptr->length() : 0UL;
	}
	inline const char* pointer() const {
		return ptr ? ptr->pointer() : nullptr;
	}
	bool operator<(const StringC& rhs) const {
		auto l = values();
		auto r = rhs.values();
		return l < r || (l == r && String(pointer(), length()) < String(rhs.pointer(), rhs.length()));
	}
	bool operator>(const StringC& rhs) const {
		auto l = values();
		auto r = rhs.values();
		return l > r || (l == r && String(pointer(), length()) > String(rhs.pointer(), rhs.length()));
	}
	bool operator==(const StringC& rhs) const {
		return ptr == rhs.ptr || (values() == rhs.values() && String(pointer(), length()) == String(rhs.pointer(), rhs.length()));
	}
};

#ifdef STRING_TAGGED_POINTER
template <>
class StringC<String> {
	typedef String Content;
	StringContainerSharedContent<Content>* ptr = nullptr;
	typedef typename StringContainerSharedContent<Content>::Ref ContentRef;
 public:
	typedef Content ContentType;
	StringC(typename StringContainerSharedContent<Content>::Ref&& _ptr) {
		set(_ptr.get());
	}
	class Builder {
		typename StringContainerSharedContent<Content>::Ref ptr;
		StringLength len;
		Builder(typename StringContainerSharedContent<Content>::Ref _ptr, StringLength _len) : ptr(_ptr), len(_len) {
		}
	 public:
		static Builder CreateWithMax(StringLength max) {
			return CreateWithSize(StringContainerSharedContent<Content>::SizeWithMaxAllocation(max));
		}
		static Builder CreateWithSize(StringLength bytes) {
			return {bytes > 0 ? StringContainerSharedContent<Content>::Create(bytes) : nullptr, bytes};
		}
		char* pointer() {
			return ptr ? const_cast<char*>(ptr->pointer()) : nullptr;
		}
		StringLength length() {
			return len;
		}
		void limit(StringLength limit) {
			if (limit < len)
				len = limit;
		}
		StringC<Content> build() {
			ptr->build(len);
			return std::move(ptr);
		}
		RawWriter writer();
	};
	StringC() {
	}

	inline operator Content() const {
		return content();
	}
	explicit inline operator bool() const {
		return ptr;
	}
	inline bool emptyReference() const {
		return !ptr;
	}
	inline Content operator*() const {
		return content();
	}
	inline StringOperationString op() const {
		return content().op();
	}
	inline operator StringOperationString() const {
		return content().op();
	}
	bool operator<(const StringC& rhs) const {
		auto l = values();
		auto r = rhs.values();
		return l < r || (l == r && String(pointer(), length()) < String(rhs.pointer(), rhs.length()));
	}
	bool operator>(const StringC& rhs) const {
		auto l = values();
		auto r = rhs.values();
		return l > r || (l == r && String(pointer(), length()) > String(rhs.pointer(), rhs.length()));
	}
	bool operator==(const StringC& rhs) const {
		return ptr == rhs.ptr || (values() == rhs.values() && String(pointer(), length()) == String(rhs.pointer(), rhs.length()));
	}
	// special tagged pointer functions
	StringC(const StringC& str) {
		set(str.ptr);
	}
	StringC(StringC&& str) {
		std::swap(ptr, str.ptr);
	}
	explicit StringC(const StringOperation& string) {
		*this = string;
	}
	explicit StringC(const Content& string) {
		*this = string;
	}
	~StringC() {
		set(nullptr);
	}
	void clear() {
		set(nullptr);
	}
	inline void set(decltype(ptr) _ptr) {
		if (ptr && (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1)) == 0)
			ContentRef::DecreaseRefCounter(ptr);
		ptr = _ptr;
		if (ptr && (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1)) == 0)
			ContentRef::IncreaseRefCounter(ptr);
	}
	StringC& operator=(const StringC& string) {
		set(string.ptr);
		return *this;
	}
	StringC& operator=(StringC&& string) {
		set(nullptr);
		std::swap(ptr, string.ptr);
		return *this;
	}
	StringC& operator=(const Content& string) {
		return operator=(string.op());
	}
	StringC& operator=(const std::nullptr_t&) {
		set(nullptr);
		return *this;
	}
	StringC& operator=(const StringOperation& string) {
		ContentRef retainer;
		decltype(ptr) _ptr = nullptr;
		if (string.size() > 0) {
			if (string.size() < __WORDSIZE / 8) {
				_ptr = reinterpret_cast<decltype(ptr)>(uintptr_t(string.size()));
				assert((uintptr_t(_ptr) & ((__WORDSIZE / 8) - 1)) == string.size());
				string(reinterpret_cast<char*>(&_ptr) + (BYTE_ORDER == LITTLE_ENDIAN), string.size());
				assert((uintptr_t(_ptr) & ((__WORDSIZE / 8) - 1)) == string.size());
			} else {
				retainer = StringContainerSharedContent<Content>::Create(string);
				_ptr = retainer.get();
			}
		}
		set(_ptr);
		return *this;
	}
	inline typename Content::Values values() const {
		return {length()};
	}
	inline auto string() const {
		return content().string();
	}
	inline Content content() const {
		return {pointer(), length()};
	}
	size_t allocated_size() const {
		return (!ptr || (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1))) ? 0 : allocation_size(ptr);
	}
	inline StringLength length() const {
		if (!ptr)
			return 0UL;
		return (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1)) ? (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1)) : ptr->length();
	}
	inline const char* pointer() const {
		if (!ptr)
			return nullptr;
		return (uintptr_t(ptr) & ((__WORDSIZE / 8) - 1)) ? reinterpret_cast<const char*>(&ptr) + (BYTE_ORDER == LITTLE_ENDIAN) : ptr->pointer();
	}
};
#endif

template <class Content>
const Content StringC<Content>::empty = {};

template <class Content>
class StringRefWith {
	StringC<Content> c;
	Content str;
 public:
	StringRefWith() {
	}
	StringRefWith(StringC<Content> container) : c(std::move(container)) {
	}
	StringRefWith(const Content& string) : str(string) {
	}
	inline auto string() const {
		return content().string();
	}
	Content content() const {
		return c.emptyReference() ? str : c.content();
	}
	StringC<Content> retain() {
		if (c.emptyReference())
			c = StringC<Content>(str);
		return c;
	}
	Content retain(MemoryPool& mp) {
		if (c.emptyReference()) {
			str = str(mp);
			return str;
		}
		return c.content();
	}
	operator Content() const {
		return content();
	}
	operator StringC<Content>() {
		return retain();
	}
	explicit operator bool() const {
		return bool(content());
	}
	Content operator*() const {
		return content();
	}
	StringOperationString op() const {
		return content().op();
	}
	bool retained() const {
		return !c.emptyREference();
	}
};

typedef StringRefWith<String> StringRef;

// CONVERSIONS
class precision {
	unsigned short p;
 public:
	precision(unsigned short _precision = 1) : p(_precision) {
	}
	unsigned short getPrecision() const {
		return p;
	}
};

class capital {
	bool capitals;
 public:
	capital(bool _capitals) : capitals(_capitals) {
	}
	bool getCapitals() const {
		return capitals;
	}
};

template < class T,
					 unsigned int bufferSize = std::numeric_limits<T>::digits10 + 1 + std::numeric_limits<T>::is_signed,
					 int base = 10 >
class NumberToStringObject : public StringOperation {
	char buffer[bufferSize];
	unsigned char pos = bufferSize;
	template <int v>
	struct Int2Type {
		enum { value = v };
	};

	static_assert(base >= 10, "base should be at least 10, otherwise the allocated storage could be too small (based on digits10)");
	static_assert(base <= 64, "base should be smaller or equal to 64");

	inline char chr(unsigned char r) {
		assert(r < base);
		assert(r < 64);
		return "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_."[r];
	}

	void doConversion(T n, unsigned int precision, Int2Type<false>) {
		while (n != 0) {
			unsigned char r = n % base;
			buffer[--pos] = chr(r);
			n /= base;
		}
		while (pos > 0 && (bufferSize - pos) < precision)
			buffer[--pos] = '0';
	}

	void doConversion(T n, unsigned int precision, Int2Type<true>) {
		if (n >= 0) {
			doConversion(n, precision, Int2Type<false>());
			return;
		}
		while (n != 0) {
			unsigned char r = -(n % base);
			buffer[--pos] = chr(r);
			n /= base;
		}
		while (pos > 0 && (bufferSize - pos) < precision)
			buffer[--pos] = '0';
		buffer[--pos] = '-';
	}

 public:
	NumberToStringObject(T i, precision p = precision(1)) {
		// Potential helpful hints: http://www.jb.man.ac.uk/~slowe/cpp/itoa.html
		doConversion(i, p.getPrecision(), Int2Type<std::numeric_limits<T>::is_signed>());
		_size = bufferSize - pos;
	}
	virtual StringLength writeTo(char* target, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		const char* representation = &buffer[pos];
		memcpy(target, representation, l);
		return l;
	}
};

template < class T,
					 unsigned int bufferSize = std::numeric_limits<T>::digits10 + 1 + std::numeric_limits<T>::is_signed >
class FloatToStringObject : public StringOperation {
	char buffer[bufferSize];

	template <class _CharT, class _Traits>
	class fixed_stringbuf
		: public std::basic_streambuf<_CharT, _Traits> {
		using Base = std::basic_streambuf<_CharT, _Traits>;
	 public:
		typedef _CharT                         char_type;
		typedef _Traits                        traits_type;
		typedef typename traits_type::int_type int_type;
		typedef typename traits_type::pos_type pos_type;
		typedef typename traits_type::off_type off_type;

	 public:
		explicit fixed_stringbuf(char* addr, size_t size) {
			Base::setp(&addr[0], &addr[size]);
		}

		StringLength getStringLength() {
			return StringLength(Base::pptr() - Base::pbase());
		}
	};

 public:
	FloatToStringObject(T i, precision p = precision(99)) {
		fixed_stringbuf<char, std::char_traits<char>> buf {buffer, bufferSize};
		std::ostream s {&buf};
		s.precision(p.getPrecision());
		s.unsetf(std::ios::floatfield);
		s << i;
		_size = buf.getStringLength();
	}
	virtual StringLength writeTo(char* target, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		memcpy(target, buffer, l);
		return l;
	}
};

static inline char chrToHex(unsigned char r) {
	checkAssert(r < 16);
	return "0123456789abcdef"[r];
}

template <int N, int bufferSize = 2 * N>
class ArrayToHexStringObject : public StringOperation {
	char buffer[bufferSize];
 public:
	ArrayToHexStringObject(const unsigned char* in) {
		for (int i = 0; i < N; ++i) {
			buffer[_size++] = chrToHex(in[i] >> 4);
			buffer[_size++] = chrToHex(in[i] & 0xF);
		}
	}
	virtual StringLength writeTo(char* target, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		memcpy(target, buffer, l);
		return l;
	}
};

class StringToHexStringObject : public StringOperation {
	String str;
 public:
	StringToHexStringObject(const String& string) : StringOperation(string.size() * 2), str(string) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		for (StringLength pos = 0; pos < l; ++pos) {
			if (pos % 2 == 0) {
				buffer[pos] = chrToHex(uint8_t(str[pos / 2]) >> 4);
			} else {
				buffer[pos] = chrToHex(uint8_t(str[pos / 2]) & 0xF);
			}
		}
		return l;
	}
};

class StringFromHexStringObject : public StringOperation {
	String str;

 public:
	StringFromHexStringObject(const String& string) : StringOperation(string.size() / 2), str(string) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		for (StringLength pos = 0; pos < l; ++pos) {
			buffer[pos] = char(StringUtil::FromHex(str[pos * 2], str[pos * 2 + 1]));
		}
		return l;
	}
};


template <typename T>
class StringConversion {
 public:
	template <typename... Args>
	static T Convert(const T& s, Args&& ... args) {
		return {s, std::forward<Args>(args)...};
	}
};

#define NUMBER_TO_STRING(type) \
template <> \
class StringConversion< type > { \
public: \
    template <typename... Args> \
    static NumberToStringObject< type > Convert(const type& s, Args&&... args) { \
        return {s, std::forward<Args>(args)...}; \
    } \
}

NUMBER_TO_STRING(char);
NUMBER_TO_STRING(unsigned char);
NUMBER_TO_STRING(short);
NUMBER_TO_STRING(unsigned short);
NUMBER_TO_STRING(int);
NUMBER_TO_STRING(unsigned int);
NUMBER_TO_STRING(long);
NUMBER_TO_STRING(unsigned long);
NUMBER_TO_STRING(long long);
NUMBER_TO_STRING(unsigned long long);

#define FLOAT_TO_STRING(type) \
template <> \
class StringConversion< type > { \
public: \
    template <typename... Args> \
    static FloatToStringObject< type > Convert(const type& s, Args&&... args) { \
        return {s, std::forward<Args>(args)...}; \
    } \
}

FLOAT_TO_STRING(float);
FLOAT_TO_STRING(double);
FLOAT_TO_STRING(long double);

template <>
struct StringConversion<std::string> {
	class STLString : public StringOperation {
		const std::string& str;
	 public:
		STLString(const std::string& string) : StringOperation(string.size()), str(string) {
		}

		virtual StringLength _size() const {
			return str.length();
		}
		virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
			StringLength l = std::min(_size(), bufferLength);
			const char* representation = str.c_str();
			memcpy(buffer, representation, l);
			return l;
		}
	};

	static STLString Convert(const std::string& s) {
		return {s};
	}
};

class CharString : public StringOperation {
	String str;
 public:
	CharString(const char* string) : str(string) {
		_size = this->str.length();
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength l = std::min(_size, bufferLength);
		const char* representation = str.pointer();
		memcpy(buffer, representation, l);
		return l;
	}
};

template <>
struct StringConversion<StringOperation> {
	static const StringOperation& Convert(const StringOperation& s) {
		return s;
	}
};

template <>
struct StringConversion<const char*> {
	static CharString Convert(const char* s) {
		return {s};
	}
};

template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
struct StringConversion<Hashed<S, StringHashType, Hash>> {
	static String Convert(const Hashed<S, StringHashType, Hash>& s) {
		return s;
	}
};

template <>
struct StringConversion<char*> {
	static CharString Convert(char* s) {
		return {s};
	}
};

template <class Content>
struct StringConversion<StringC<Content>> {
	static String Convert(const StringC<Content>& s) {
		return s.content().string();
	}
};

template <StringLength N>
struct StringConversion<char[N]> {
	class FixedCharString : public StringOperation {
		const char (&str)[N];
	 public:
		constexpr FixedCharString(const char (&string)[N]) : StringOperation(N - 1), str(string) {
		}
		virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
			StringLength l = std::min(N - 1, bufferLength);
			memcpy(buffer, str, l);
			return l;
		}
	};
	static FixedCharString Convert(const char (&s)[N]) {
		return {s};
	}
};

template <>
struct StringConversion<bool> {
	class BoolString : public StringOperation {
		bool value;
	 public:
		constexpr BoolString(bool _value) : StringOperation(_value ? 4 : 5), value(_value) {
		}

		virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
			StringLength l = std::min(_size, bufferLength);
			const char* representation = value ? "true" : "false";
			memcpy(buffer, representation, l);
			return l;
		}
	};
	static BoolString Convert(const bool& s) {
		return {s};
	}
};

// conversion functions

template <class T>
auto ToString(const T& t) -> decltype(StringConversion<T>::Convert(t)) {
	return StringConversion<T>::Convert(t);
}

template <class T, typename... Args>
auto ToString(const T& t, Args&& ... p) -> decltype(StringConversion<T>::Convert(t, std::forward<Args...>(p...))) {
	return StringConversion<T>::Convert(t, std::forward<Args...>(p...));
}

template <class T>
NumberToStringObject < T, std::numeric_limits<T>::digits / 4 + 1 + std::numeric_limits<T>::is_signed, 16 > ToHexString(const T& t, precision p = precision()) {
	return NumberToStringObject < T, std::numeric_limits<T>::digits / 4 + 1 + std::numeric_limits<T>::is_signed, 16 > (t, p);
}

template <int N>
ArrayToHexStringObject<N> ArrayToHexString(const char (&in)[N]) {
	return ArrayToHexStringObject<N>(reinterpret_cast<const unsigned char*>(in));
}

template <int N>
ArrayToHexStringObject<N> ArrayToHexString(const unsigned char (&in)[N]) {
	return ArrayToHexStringObject<N>(in);
}

inline StringToHexStringObject ToHexString(const String& str) {
	return {str};
}

inline StringFromHexStringObject FromHexString(const String& str) {
	return {str};
}

template <class T>
NumberToStringObject < T, std::numeric_limits<T>::digits / 6 + 1 + std::numeric_limits<T>::is_signed, 64 > ToMaxString(const T& t, precision p = precision()) {
	return NumberToStringObject < T, std::numeric_limits<T>::digits / 6 + 1 + std::numeric_limits<T>::is_signed, 64 > (t, p);
}

// composite conversion functions

template <class T>
class StringConversion<T*> {
 public:
	class PointerString : public StringOperation {
		const T* pointer;
		decltype(ToHexString(long(std::declval<const T*>()))) hex;
	 public:
		constexpr PointerString(const T* ptr) : pointer(ptr), hex(ToHexString(long(pointer))) {
			_size = 2 + hex._size;
		}

		virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
			StringLength len = writeToHelper(buffer, bufferLength, "0x", 2);
			len += writeToHelper(buffer, bufferLength, hex);
			return len;
		}
	};
	static PointerString Convert(T* s) {
		return {s};
	}
};

template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
auto ToString(const shared_object<Object, Base, RefCountType, ptr, deleter>& t) {
	return ToString(t.get());
}

// OPERATIONS
template <class FirstString, class B>
class StringConcatRef : public StringOperation {
	const FirstString& a;
	const B b;
 public:
	constexpr StringConcatRef(const FirstString& _a, B&& _b) : StringOperation(_a.size() + _b.size()), a(_a), b(std::move(_b)) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength l = a.writeTo(buffer, bufferLength);
		return l + b.writeTo(buffer + l, bufferLength - l);
	}
};

template <class B>
constexpr auto operator+(const StringOperation& a, const B& b) {
	return StringConcatRef<StringOperation, decltype(ToString(b))> {a, ToString(b)};
}

template <class FirstString, class State>
class StateConcat : public StringOperation {
	const FirstString&  t;
	State st;
 public:
	constexpr StateConcat(const FirstString& _t, State&& _st) : StringOperation(_t.size()), t(_t), st(std::move(_st)) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		return t.writeTo(buffer, bufferLength);
	}
	operator State() const {
		return st;
	}
};

inline StateConcat<StringOperation, precision> operator+(const StringOperation& a, const precision& b) {
	return {a, precision(b)};
}

inline StateConcat<StringOperation, capital> operator+(const StringOperation& a, const capital& b) {
	return {a, capital(b)};
}

template <class FirstString, class State, class B>
constexpr auto operator+(const StateConcat<FirstString, State>& a, const B& b) {
	return StringConcatRef<StringOperation, decltype(ToString(b, State(a)))> {a, ToString(b, State(a))};
}

template <class FirstString>
class StringMultiply : public StringOperation {
	const FirstString& a;
	const unsigned long i;
 public:
	StringMultiply(const FirstString& _a, unsigned long _i) : StringOperation(_i * _a._size), a(_a), i(_i) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		unsigned long k = 0; // how many times the string is already written out
		// write once full
		StringLength first = a.writeTo(buffer, bufferLength, 0);
		k++;
		StringLength l = first;
		// repeat if needed
		for (; l < bufferLength && k < i; k++) {
			StringLength len = std::min(first, bufferLength - l);
			memcpy(&buffer[l], buffer, len);
			l += len;
		}
		return l;
	}
};
template <class FirstString>
StringMultiply<FirstString> operator*(const FirstString& a, unsigned long i) {
	return StringMultiply<FirstString>(a, i);
}

template <class FirstString>
class EscapeOperation : public StringOperation {
	const FirstString& a;
 public:
	constexpr EscapeOperation(const FirstString& _a) : StringOperation(_a.size()), a(_a) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength size = a.writeTo(buffer, bufferLength);
		char* end = buffer + size;
		for (char* it = buffer; it < end; ++it)
			if (*it == '\'')
				*it = '"';
		return size;
	}
};

template <class FirstString>
EscapeOperation<FirstString> Escape(const FirstString& str) {
	return EscapeOperation<FirstString>(str);
}

template <class FirstString>
class UppercaseOperation : public StringOperation {
	const FirstString& a;
 public:
	constexpr UppercaseOperation(const FirstString& _a) : StringOperation(_a.size()), a(_a) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength size = a.writeTo(buffer, bufferLength);
		char* end = buffer + size;
		for (char* it = buffer; it < end; ++it)
			*it = StringUtil::ToUpper(*it);
		return size;
	}
};

template <class FirstString>
UppercaseOperation<FirstString> ToUppercase(const FirstString& str) {
	return UppercaseOperation<FirstString>(str);
}

template <class FirstString>
class LowercaseOperation : public StringOperation {
	const FirstString& a;
 public:
	constexpr LowercaseOperation(const FirstString& _a) : StringOperation(_a.size()), a(_a) {
	}
	StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength size = a.writeTo(buffer, bufferLength);
		char* end = buffer + size;
		for (char* it = buffer; it < end; ++it)
			*it = StringUtil::ToLower(*it);
		return size;
	}
};

template <class FirstString>
LowercaseOperation<FirstString> ToLowercase(const FirstString& str) {
	return LowercaseOperation<FirstString>(str);
}

// RAW stuff
class _RawByte : public StringOperation {
 public:
	char n;
	_RawByte(uint8_t _n) : StringOperation(1), n(char(_n)) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, &n, 1);
		return retval;
	}
};

inline _RawByte ToRawU8(uint8_t n) {
	return {n};
}

template <class T>
class _RawNumber : public StringOperation {
 public:
	T n;
	_RawNumber(T _n) : StringOperation(sizeof(T)), n(hton(_n)) {
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		retval += StringOperation::writeToHelper(buffer, bufferLength, static_cast<const void*>(&n), sizeof(T));
		return retval;
	}
};

inline _RawNumber<uint16_t> ToRawU16(uint16_t n) {
	return {n};
}

inline _RawNumber<uint32_t> ToRawU32(uint32_t n) {
	return {n};
}

inline _RawNumber<uint64_t> ToRawU64(uint64_t n) {
	return {n};
}

template <class T>
class _StringConcat : public StringOperation {
	//static_assert(std::is_same<typename T::value_type, String>::value, "should be a container of Strings");
 public:
	const T& strings;
	_StringConcat(const T& _strings) : strings(_strings) {
		for (const auto& s : strings)
			_size += ToString(s).size();
	}
	virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
		StringLength retval = 0;
		for (const auto& s : strings)
			retval += StringOperation::writeToHelper(buffer, bufferLength, ToString(s));
		return retval;
	}
};

template <class T>
_StringConcat<T> StringConcat(const T& t) {
	return {t};
}

template <class T, class Alloc>
struct StringConversion<std::vector<T, Alloc>> {
	static _StringConcat<std::vector<T, Alloc>> Convert(const std::vector<T, Alloc>& s) {
		return {s};
	}
};

// remaining

template <StringLength N>
String StringOperation::operator()(char (&buffer)[N]) const {
	StringLength len = writeTo(buffer, N);
	return String(buffer, len);
}

template <StringLength N> StringT StringOperation::c_str(char (&buffer)[N]) const {
	StringLength len = writeTo(buffer, N - 1);
	buffer[len] = '\0';
	return StringT(buffer, len);
}

template <StringLength N>
const String ConstString(const char (&string)[N]) {
	return String(static_cast<const char*>(string), N - 1);
}

template <>
class StringConversion<Exception> {
 public:
	static auto Convert(const Exception& e) {
		return (String("Exception ") + e.errorCode() + " (" + e.errorString() + ")" +
						" [" + e.function() + " in " + e.sourceFile() + ":" + e.lineNumber() + "]" +
						" due to " + e.description());
	}
};

template <class T>
auto ToHexString(T* ptr) {
	return ToHexString(uintptr_t(ptr));
}

template<>
class StringConversion<std::exception_ptr> {
 public:
	class ExceptionString : public StringOperation {
		String result;
		char b[128];
	 public:
		ExceptionString(std::exception_ptr ep) {
			try {
				std::rethrow_exception(ep);
			} catch (Exception& e) {
				result = ToString(e)(b);
			} catch (std::exception& e) {
				result = String(e.what())(b);
			} catch (std::string& e) {
				result = String(e)(b);
			} catch (...) {
				result = "Unknown error";
			}
			_size = result.size();
		}
		virtual StringLength writeTo(char* buffer, StringLength bufferLength) const override {
			return writeToHelper(buffer, bufferLength, result);
		}
	};
	static ExceptionString Convert(std::exception_ptr e) {
		return {e};
	}
};

}
}

constexpr bitpowder::lib::StringT operator "" _S(const char* value, size_t size) {
	return {value, bitpowder::lib::StringLength(size)};
}

inline bitpowder::lib::Hashed<bitpowder::lib::StringT> operator "" _HS(const char* value, size_t size) {
	return bitpowder::lib::Hashed<bitpowder::lib::StringT> {{value, bitpowder::lib::StringLength(size)}};
}

inline bitpowder::lib::Hashed<bitpowder::lib::StringT, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase> operator "" _HSIC(const char* value, size_t size) {
	return bitpowder::lib::Hashed<bitpowder::lib::StringT, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase> {{value, bitpowder::lib::StringLength(size)}};
}

inline bitpowder::lib::Hashed<bitpowder::lib::StringT, bitpowder::lib::StringStableHashShort, bitpowder::lib::StringUtil::StableHashShort> operator "" _SHS(const char* value, size_t size) {
	return bitpowder::lib::Hashed<bitpowder::lib::StringT, bitpowder::lib::StringStableHashShort, bitpowder::lib::StringUtil::StableHashShort> {{value, bitpowder::lib::StringLength(size)}};
}

namespace std {
inline ostream& operator<< (ostream& out, const bitpowder::lib::String& str) {
	out.write(str.pointer(), streamsize(str.size()));
	return out;
}
inline ostream& operator<< (ostream& out, const bitpowder::lib::StringT& str) {
	out.write(str.pointer(), streamsize(str.size()));
	return out;
}
inline ostream& operator<< (ostream& out, const bitpowder::lib::StringOperation& op) {
	bitpowder::lib::StaticMemoryPool<16384> mp;
	return out << op(mp);
}
// specific versions of above function, so the copy is avoided
template <class Content>
inline ostream& operator<< (ostream& out, const bitpowder::lib::StringC<Content>& str) {
	return out << str.content();
}
template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
inline ostream& operator<< (ostream& out, const bitpowder::lib::Hashed<S, StringHashType, Hash>& str) {
	out.write(str->pointer(), streamsize(str->size()));
	return out;
}


template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
struct less<bitpowder::lib::Hashed<S, StringHashType, Hash>> {
	bool operator()(const bitpowder::lib::Hashed<S, StringHashType, Hash>& lhs, const bitpowder::lib::Hashed<S, StringHashType, Hash>& rhs) const {
		return lhs.less(rhs);
	}
};

template <class S>
struct less<bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>> {
	bool operator()(const bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>& lhs, const bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>& rhs) const {
		return lhs.less(rhs, bitpowder::lib::LessIgnoreCase<S>());
	}
};

template <class Content>
struct less<bitpowder::lib::StringC<Content>> : public less<Content> {
	typedef void is_transparent;
};

template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
struct greater<bitpowder::lib::Hashed<S, StringHashType, Hash>> {
	bool operator()(const bitpowder::lib::Hashed<S, StringHashType, Hash>& lhs, const bitpowder::lib::Hashed<S, StringHashType, Hash>& rhs) const {
		return lhs.greater(rhs);
	}
};

template <class S>
struct greater<bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>> {
	bool operator()(const bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>& lhs, const bitpowder::lib::Hashed<S, bitpowder::lib::StringHashIgnoreCase, bitpowder::lib::StringUtil::HashIgnoreCase>& rhs) const {
		return lhs.greater(rhs, bitpowder::lib::GreaterIgnoreCase<S>());
	}
};

template <class Content>
struct greater<bitpowder::lib::StringC<Content>> : public greater<Content> {
	typedef void is_transparent;
};


template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
struct equal_to<bitpowder::lib::Hashed<S, StringHashType, Hash>> {
	bool operator()(const bitpowder::lib::Hashed<S, StringHashType, Hash>& lhs, const bitpowder::lib::Hashed<S, StringHashType, Hash>& rhs) const {
		return lhs.equals(rhs);
	}
};
template <class Content>
struct equal_to<bitpowder::lib::StringC<Content>> : public equal_to<Content> {
	typedef void is_transparent;
};


template <>
struct hash<bitpowder::lib::String> {
	bitpowder::lib::StringHash operator()(const bitpowder::lib::String& str) const {
		return bitpowder::lib::StringUtil::Hash(str);
	}
};

template <class S, class StringHashType, StringHashType (*Hash)(const bitpowder::lib::String& s)>
struct hash<bitpowder::lib::Hashed<S, StringHashType, Hash>> {
	StringHashType operator()(const bitpowder::lib::Hashed<S, StringHashType, Hash>& str) const {
		return str.hash();
	}
};

template <class Content>
struct hash<bitpowder::lib::StringC<Content>> : public hash<Content> {
	auto operator()(const bitpowder::lib::StringC<Content>& str) const {
		return hash<Content>::operator()(str.content());
	}
};

}


/**
Copyright 2013-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "simplestring.h"
#include <type_traits>

namespace bitpowder {
namespace lib {

struct AtEnd {
	String text;
	AtEnd(String _text = {}) : text(std::move(_text)) {
	}
};

struct Empty {
};

class StringParser {
	const static int DEFAULT_ERROR = std::numeric_limits<int>::max();
	const static int NO_ERROR = 0;

	StringLength processedBytes = 0;
	String left;
	int err = NO_ERROR; // 0 = no error
	StringLength errorOccuredAtPosition = std::numeric_limits<StringLength>::max();
	String matchedPart;

	void process(StringLength bytes) {
		processedBytes += bytes;
		left = left.substring(bytes);
	}

	void setError(int _err = DEFAULT_ERROR) {
		assert(err == NO_ERROR);
		assert(errorOccuredAtPosition == std::numeric_limits<StringLength>::max());
		err = _err;
		errorOccuredAtPosition = processedBytes;
	}
public:
	StringParser(const String& input, int error = NO_ERROR) : left(input), err(error) {
	}
	StringParser(const StringParser& copy) {
		*this = copy;
	}
	StringParser& operator=(const StringParser& copy) {
		processedBytes = copy.processedBytes;
		left = copy.left;
		err = copy.err;
		errorOccuredAtPosition = copy.errorOccuredAtPosition;
		matchedPart = copy.matchedPart;
		return *this;
	}

	StringParser& error(int errorCodeIfDefaultErrorCodeIsCurrentlyActive) {
		if (err == DEFAULT_ERROR)
			err = errorCodeIfDefaultErrorCodeIsCurrentlyActive;
		return *this;
	}
	StringParser& position(StringLength& position, int& error) {
		position = processedBytes;
		error = err;
		return *this;
	}
	StringParser& matched(String& matched) {
		matched = matchedPart;
		return *this;
	}
	StringParser& matched(StringLength& matched) {
		matched = matchedPart.size();
		return *this;
	}
	StringParser& unskip() {
		if (!err && matchedPart) {
			assert(matchedPart.pointer() + matchedPart.size() == left.pointer() || left.empty());
			left = {matchedPart.pointer(), matchedPart.size() + left.size()};
			processedBytes -= matchedPart.size();
			matchedPart = {};
		}
		return *this;
	}

	class Optional {
		friend class StringParser;
		// storing the original values of StringParser, in the StringParser the optional stuff is tried
		StringLength processedBytes = 0;
		String left;
		bool valid;
#ifndef NDEBUG
		bool inUse = false;
#endif
	public:
		operator bool() const {
			return valid;
		}
	};

	StringParser& begin(Optional& optional) {
#ifndef NDEBUG
		assert(!optional.inUse);
		optional.inUse = true;
#endif
		optional.processedBytes = processedBytes;
		optional.left = left;
		optional.valid = err == 0;
		return *this;
	}
	StringParser& end(Optional& optional) {
#ifndef NDEBUG
		assert(optional.inUse);
		optional.inUse = false;
#endif
		if (optional.valid && err) {
			processedBytes = optional.processedBytes;
			left = optional.left;
			err = 0;
			errorOccuredAtPosition = std::numeric_limits<StringLength>::max();
			optional.valid = false;
		} else
			optional.valid = optional.valid && err == 0;
		return *this;
	}

	StringParser& nonEmpty() {
		if (!err && left.empty())
			setError();
		return *this;
	}

	StringParser& empty() {
		if (!err && !left.empty())
			setError();
		return *this;
	}

	template <class N, int Base = 10>
	StringParser & number(N& n, StringLength min = 1, StringLength max = std::numeric_limits<StringLength>::max()) {
		if (err)
			return *this;
		String remainder;
		String candidate = left.substring(0, max);
		N retval = candidate.toNumber<N, Base>(remainder);
		StringLength chars = candidate.size() - remainder.size();
		if (chars < min || chars > max) {
			n = {};
			setError();
		} else {
			n = retval;
			process(chars);
		}
		return *this;
	}

	template <class N>
	StringParser& hexNumber(N& n, StringLength min = 1, StringLength max = std::numeric_limits<StringLength>::max()) {
		return number<N, 16>(n, min, max);
	}

	template <class N>
	StringParser& maxNumber(N& n, StringLength min = 1, StringLength max = std::numeric_limits<StringLength>::max()) {
		if (err)
			return *this;
		String remainder;
		String candidate = left.substring(0, max);
		N retval = candidate.maxToNumber<N>(remainder);
		StringLength chars = candidate.size() - remainder.size();
		if (chars < min || chars > max) {
			n = {};
			setError();
		} else {
			n = retval;
			process(chars);
		}
		return *this;
	}

	StringParser& accept() {
		setError();
		return *this;
	}

	template <typename T, typename... Args>
	StringParser& accept(const T& first, const Args& ... args) {
		if (err)
			return *this;
		if (_accept(first))
			return *this;
		return accept(args...);
	}

	template <typename T, typename... Args>
	StringParser& repeatAccept(unsigned int N, const T& first, const Args& ... args) {
		if (N == 0)
			return *this;
		accept(first, args...);
		return repeatAccept(N - 1, first, args...);
	}

	bool _accept(const Empty&) {
		matchedPart = {};
		return true;
	}

	bool _accept(const End&) {
		if (left.size() > 0)
			return false;
		matchedPart = {};
		return true;
	}

	bool _accept(const String& str) {
		if (!left.startsWith(str))
			return false;
		matchedPart = left.substring(0, str.length());
		process(str.length());
		return true;
	}

	bool _accept(const StringT& str) {
		return _accept(str.string());
	}

	template <class Content>
	bool _accept(const StringC<Content>& str) {
		return _accept(str.string());
	}

	template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
	bool _accept(const Hashed<S, StringHashType, Hash>& str) {
		return _accept(str.string());
	}

	template <StringLength N>
	bool _accept(const char (&str)[N]) {
		return _accept(String(str));
	}

	bool _accept(const char& c) {
		if (left.size() == 0 || left.pointer()[0] != c)
			return false;
		matchedPart = left.substring(0, 1);
		process(1);
		return true;
	}

	template <class CharMatcher>
	bool _accept(const CharMatcher& m) {
		//static_assert(!std::is_invocable<C, char>::value, "should be a Matcher function, not primitives"); // C++17
		static_assert(std::is_same<bool, typename std::result_of<CharMatcher(char)>::type>::value, "should be a Matcher function, not primitives (which get cast by the syntax below"); // C++11
		if (left.size() == 0)
			return m() && (static_cast<void>(matchedPart = {}), true);
		if (!m(left.pointer()[0]))
			return false;
		matchedPart = left.substring(0, 1);
		process(1);
		return true;
	}

	// accept ignore case
	StringParser& acceptIgnoreCase() {
		setError();
		return *this;
	}

	template <typename T, typename... Args>
	StringParser& acceptIgnoreCase(const T& first, const Args& ... args) {
		if (err)
			return *this;
		if (_acceptIgnoreCase(first))
			return *this;
		return acceptIgnoreCase(args...);
	}

	bool _acceptIgnoreCase(const End&) {
		if (left.size() > 0)
			return false;
		matchedPart = {};
		return true;
	}
	bool _acceptIgnoreCase(const String& str) {
		if (!left.startsWithIgnoreCase(str))
			return false;
		matchedPart = left.substring(0, str.length());
		process(str.length());
		return true;
	}

	bool _acceptIgnoreCase(const StringT& str) {
		return _acceptIgnoreCase(str.string());
	}

	template <class Content>
	bool _acceptIgnoreCase(const StringC<Content>& str) {
		return _acceptIgnoreCase(str.string());
	}

	template <class S, class StringHashType, StringHashType (*Hash)(const String& s)>
	bool _acceptIgnoreCase(const Hashed<S, StringHashType, Hash>& str) {
		return _acceptIgnoreCase(str.string());
	}

	template <StringLength N>
	bool _acceptIgnoreCase(const char (&str)[N]) {
		return _acceptIgnoreCase(String(str));
	}

	bool _acceptIgnoreCase(const char& c) {
		if (left.size() == 0 || StringUtil::ToLower(left.pointer()[0]) != StringUtil::ToLower(c))
			return false;
		matchedPart = left.substring(0, 1);
		process(1);
		return true;
	}

	template <class CharMatcher>
	bool _acceptIgnoreCase(const CharMatcher& m) {
		static_assert(std::is_same<bool, typename std::result_of<CharMatcher(char)>::type>::value, "should be a Matcher function, not primitives (which get cast by the syntax below"); // C++11
		if (left.size() == 0)
			return m();
		if (!m(StringUtil::ToLower(left.pointer()[0])) && !m(StringUtil::ToUpper(left.pointer()[0])))
			return false;
		matchedPart = left.substring(0, 1);
		process(1);
		return true;
	}

	template <class Span>
	StringParser& span(const Span& s, StringLength min = 0, StringLength max = std::numeric_limits<StringLength>::max()) {
		if (err)
			return *this;
		matchedPart = left.substring(0, max).span(s);
		if (matchedPart.length() < min)
			err = DEFAULT_ERROR;
		else
			process(matchedPart.length());
		return *this;
	}

	template <class Split>
	StringParser& searchFor(const Split& on) {
		if (err)
			return *this;
		short delim;
		String prefix = left.splitOn<Split, String>(on, nullptr, &delim);
		if (delim == String::NOT_FOUND)
			setError();
		else {
			matchedPart = delim == String::END ? String() : left.substring(prefix.length(), 1);
			process(prefix.length() + (delim == String::END ? 0 : 1));
		}
		return *this;
	}
	template <class S, class Split>
	StringParser& splitOn(S& _prefix, const Split& on) {
		if (err)
			return *this;
		short delim;
		String prefix = left.splitOn<Split, String>(on, nullptr, &delim);
		if (delim == String::NOT_FOUND)
			setError();
		else {
			_prefix = prefix;
			matchedPart = delim == String::END ? String() : left.substring(prefix.length(), 1);
			process(prefix.length() + (delim == String::END ? 0 : 1));
		}
		return *this;
	}

	// bounded search
	template <String (String::*SplitMethod)(const String&)>
	StringParser& _search(bool& foundOne, StringLength& max, const String& text) {
		String in = left.substring(0, max + text.length() - 1);
		String prefix = (in.*SplitMethod)(text);
		if (prefix.length() < max) {
			foundOne = true;
			max = prefix.length();
			matchedPart = left.substring(max, text.length());
			assert(matchedPart == text);
		}
		return *this;
	}
	template <String (String::*SplitMethod)(const String&)>
	StringParser& _search(bool& foundOne, StringLength& max, const AtEnd& text) {
		if (max < left.length())
			// not at end, so ignore
			return *this;
		if (left.endsWith(text.text)) {
			foundOne = true;
			max = left.length() - text.text.length();
			matchedPart = left.substring(max, text.text.length());
			assert(matchedPart == text.text);
		}
		return *this;
	}
	template <String (String::*SplitMethod)(const String&), typename Text>
	StringParser& _searchHelper(bool foundOne, StringLength max, const Text& text) {
		_search<SplitMethod>(foundOne, max, text);
		if (foundOne) {
			process(max + matchedPart.length());
		} else {
			setError();
		}
		return *this;
	}
	template <String (String::*SplitMethod)(const String&), typename Text, typename... Args>
	StringParser& _searchHelper(bool foundOne, StringLength max, const Text& text, Args... tokens) {
		_search<SplitMethod>(foundOne, max, text);
		return _searchHelper<SplitMethod>(foundOne, max, tokens...);
	}
	template <typename... Args>
	StringParser& search(Args... tokens) {
		if (err)
			return *this;
		return _searchHelper<&String::split>(false, left.length(), tokens...);
	}
	template <typename... Args>
	StringParser& searchIgnoreCase(Args... tokens) {
		if (err)
			return *this;
		return _searchHelper<&String::splitIgnoreCase>(false, left.length(), tokens...);
	}

	template <class S = String, typename... Args>
	StringParser & split(S& prefix, Args... tokens) {
		if (err)
			return *this;
		String l = left;
		search(tokens...);
		prefix = l.substring(0, l.length() - left.length() - matchedPart.length());
		return *this;
	}

	template <class S = String, typename... Args>
	StringParser & splitIgnoreCase(S& prefix, Args... tokens) {
		if (err)
			return *this;
		String l = left;
		searchIgnoreCase(tokens...);
		prefix = l.substring(0, l.length() - left.length() - matchedPart.length());
		return *this;
	}


	template <class Span>
	StringParser& rspan(const Span& s) {
		if (err)
			return *this;
		String m = left.rspan(s);
		left = left.substring(0, left.length() - m.length());
		return *this;
	}

	template <class Span, class S = String>
	StringParser & rspan(S& m, const Span& s) {
		if (err)
			return *this;
		m = left.rspan(s);
		left = left.substring(0, left.length() - m.length());
		return *this;
	}

	StringParser& rsearch(const String& text) {
		if (!err) {
			String remainder;
			String prefix = left.rsplit(text, remainder);
			if (prefix == left) {
				// not found
				setError();
			} else {
				left = remainder;
			}
		}
		return *this;
	}
	/*
	  template <class Split, class S = String>
	  StringParser & rsplitOn(S& prefix, const Split& on, short* delim = nullptr) {
	    if (!err) {
	      short delimCopy;
	      if (!delim)
	        delim = &delimCopy;
	      prefix = left.doRSplitOn<Split>(on, delim);
	      err = *delim == String::NOT_FOUND ? DEFAULT_ERROR : 0;
	    }
	    return *this;
	  }

	  template <class S = String>
	  StringParser & rsplit(S& prefix, const String& splitOn) {
	    if (!err)
	      prefix = left.doRSplit(splitOn);
	    return *this;
	  }

	  template <class S = String>
	  StringParser & rsplitIgnoreCase(S& prefix, const String& splitOn) {
	    if (!err)
	      prefix = left.doRSplitIgnoreCase(splitOn);
	    return *this;
	  }
	  */

	StringParser& remainder(StringParser& remainder) {
		if (!err)
			remainder = *this;
		return *this;
	}

	template <class S = String>
	StringParser & remainder(S& remainder) {
		if (!err)
			remainder = left;
		return *this;
	}

	StringParser& remainder(StringLength& remainder) {
		if (!err)
			remainder = left.length();
		return *this;
	}

	const String& remaining() const {
		return left;
	}

	operator bool() const {
		return !err;
	}

	template <typename Func>
	StringParser& check(const Func& f) {
		if (err)
			return *this;
		if (!f())
			err = DEFAULT_ERROR;
		return *this;
	}
};

}
}


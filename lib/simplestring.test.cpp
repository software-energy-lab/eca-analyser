/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "simplestring.h"
#include "rawreader.h"
#include "hash.h"
#include "randombytes.h"

IGNORE_WARNINGS_START
#include <gtest/gtest.h>
IGNORE_WARNINGS_END

namespace {
using namespace bitpowder::lib;

TEST(String, BoolConversion) {
	StaticMemoryPool<128> mp;
	String t = ("test="_S + true)(mp);
	EXPECT_EQ("test=true"_S, t);
	String f = ("test="_S + false)(mp);
	EXPECT_EQ("test=false"_S, f);
}

template <class T>
void testStringConversionFor(T i, const char* expected, unsigned short p = 1) {
	StaticMemoryPool<128> mp;
	if (p > 1) {
		StringT test = ("test-"_S + precision(p) + i).c_str(mp);
		if (test != String(expected))
			fprintf(stderr, "%s == '%s'\n", expected, test.c_str());
		EXPECT_EQ(test, String(expected));
	} else {
		StringT test = ("test-"_S + i).c_str(mp);
		if (test != String(expected))
			fprintf(stderr, "%s == '%s'\n", expected, test.c_str());
		EXPECT_EQ(test, String(expected));
	}
}

TEST(String, NumberConvertionDecimal) {
	testStringConversionFor<int>(0, "test-0");
	testStringConversionFor<int>(-1, "test--1");
	testStringConversionFor<int>(int(0xFFFFFFFF), "test--1");
	testStringConversionFor<int>(-10, "test--10");
	testStringConversionFor<int>(10, "test-10");
	testStringConversionFor<int>(0x7FFFFFFF, "test-2147483647");
	testStringConversionFor<int>(-int64_t(0x80000000), "test--2147483648");
	testStringConversionFor<unsigned int>(0, "test-0");
	testStringConversionFor<unsigned int>(0, "test-00000", 5);
	testStringConversionFor<unsigned int>(10, "test-10");
	testStringConversionFor<unsigned int>(0x7FFFFFFF, "test-2147483647");
	testStringConversionFor<unsigned int>(-0x80000000, "test-2147483648");
	testStringConversionFor<unsigned int>(0xFFFFFFFF, "test-4294967295");
	testStringConversionFor<unsigned int>(uint32_t(-1), "test-4294967295");
}

template <class T>
void testHexStringConversionFor(T i, const char* expected, unsigned short p = 1) {
	StaticMemoryPool<128> mp;
	if (p > 1) {
		StringT test = ("test-"_S + ToHexString(i, p)).c_str(mp);
		if (test != String(expected))
			fprintf(stderr, "%s == '%s'\n", expected, test.c_str());
		EXPECT_EQ(test, String(expected));
	} else {
		StringT test = ("test-"_S + ToHexString(i)).c_str(mp);
		if (test != String(expected))
			fprintf(stderr, "%s == '%s'\n", expected, test.c_str());
		EXPECT_TRUE(test == String(expected));
	}
}

TEST(String, NumberConvertionHex) {
	testHexStringConversionFor<int>(0, "test-0");
	testHexStringConversionFor<int>(-1, "test--1");
	testHexStringConversionFor<int>(int(0xFFFFFFFF), "test--1");
	testHexStringConversionFor<int>(-10, "test--a");
	testHexStringConversionFor<int>(10, "test-a");
	testHexStringConversionFor<int>(0x7FFFFFFF, "test-7fffffff");
	testHexStringConversionFor<int>(-int64_t(0x80000000), "test--80000000");
	testHexStringConversionFor<unsigned int>(0, "test-0");
	testHexStringConversionFor<unsigned int>(0, "test-00000", 5);
	testHexStringConversionFor<unsigned int>(10, "test-a");
	testHexStringConversionFor<unsigned int>(0x7FFFFFFF, "test-7fffffff");
	testHexStringConversionFor<unsigned int>(-0x80000000, "test-80000000");
	testHexStringConversionFor<unsigned int>(0xFFFFFFFF, "test-ffffffff");
	testHexStringConversionFor<unsigned int>(uint32_t(-1), "test-ffffffff");
}

TEST(String, PathStuff) {
	String test = "/test/bla/test.ext/bla";
	EXPECT_EQ("bla"_S, test.base());
	EXPECT_EQ(""_S, test.extension());
	EXPECT_EQ("/test/bla/test.ext"_S, test.dir());
	EXPECT_EQ("/test/bla/test.ext/"_S, test.withoutBase());
	EXPECT_EQ("ext"_S, test.dir().extension());
	String test2 = "somefile";
	EXPECT_EQ(".", test2.dir());
	EXPECT_EQ(test2, test2.base());
	EXPECT_EQ("somefile", test2.base());
	EXPECT_EQ("", test2.extension());

	EXPECT_EQ("/"_S, "/bla"_S.dir());
	EXPECT_EQ("/"_S, "/bla"_S.withoutBase());
	EXPECT_EQ(""_S, "bla"_S.withoutBase());
}

TEST(String, ToNumber) {
	EXPECT_EQ(10, "10"_S.toNumber<int>());
	EXPECT_EQ(100, "100"_S.toNumber<int>());
	EXPECT_EQ(101, "101"_S.toNumber<int>());
	EXPECT_EQ(1012, "1012"_S.toNumber<int>());
	EXPECT_EQ(-1012, "-1012"_S.toNumber<int>());

	EXPECT_FLOAT_EQ(.1234f, ".1234"_S.toNumber<float>());
	EXPECT_FLOAT_EQ(-.1234f, "-.1234"_S.toNumber<float>());
	EXPECT_FLOAT_EQ(10.0f, "10"_S.toNumber<float>());
	EXPECT_FLOAT_EQ(10.11f, "10.11"_S.toNumber<float>());
	EXPECT_FLOAT_EQ(100.99999f, "100.99999"_S.toNumber<float>());
	EXPECT_FLOAT_EQ(101.1234567890f, "101.1234567890"_S.toNumber<float>()); // to large number for float
	EXPECT_FLOAT_EQ(1012.1111111f, "1012.1111111"_S.toNumber<float>()); // // to large number for float
	EXPECT_DOUBLE_EQ(101.1234567890, "101.1234567890"_S.toNumber<double>());
	EXPECT_DOUBLE_EQ(1012.1111111, "1012.1111111"_S.toNumber<double>());
	EXPECT_FLOAT_EQ(-1012.111f, "-1012.111"_S.toNumber<float>());

	String remainder;
	EXPECT_FLOAT_EQ(.1234e0f, ".1234E0test"_S.toNumber<float>(remainder));
	EXPECT_EQ("test"_S, remainder);
	EXPECT_FLOAT_EQ(-.1234e1f, "-.1234e1double"_S.toNumber<float>(remainder));
	EXPECT_EQ("double"_S, remainder);
	EXPECT_FLOAT_EQ(10.0e2f, "10E2some"_S.toNumber<float>(remainder));
	EXPECT_EQ("some"_S, remainder);
	EXPECT_FLOAT_EQ(10.11e3f, "10.11E3"_S.toNumber<float>(remainder));
	EXPECT_EQ(""_S, remainder);
	EXPECT_FLOAT_EQ(100.99999e4f, "100.99999E4"_S.toNumber<float>(remainder));
	EXPECT_EQ(""_S, remainder);
	EXPECT_FLOAT_EQ(101.1234567890e5f, "101.1234567890E5"_S.toNumber<float>(remainder)); // to large number for float
	EXPECT_EQ(""_S, remainder);
	EXPECT_FLOAT_EQ(1012.1111111e6f, "1012.1111111E6"_S.toNumber<float>(remainder)); // // to large number for float
	EXPECT_EQ(""_S, remainder);
	EXPECT_DOUBLE_EQ(101.1234567890e7, "101.1234567890E7"_S.toNumber<double>(remainder));
	EXPECT_EQ(""_S, remainder);
	EXPECT_DOUBLE_EQ(1012.1111111e8, "1012.1111111E8"_S.toNumber<double>(remainder));
	EXPECT_EQ(""_S, remainder);
	EXPECT_FLOAT_EQ(-1012.111e11f, "-1012.111e11"_S.toNumber<float>(remainder));
	EXPECT_EQ(""_S, remainder);

	EXPECT_EQ(int(-0x80000000), "-80000000"_S.hexToNumber<int>());
	EXPECT_EQ(0x7FFFFFFF, "7fffffff"_S.hexToNumber<int>());
	EXPECT_EQ(0x7FFFFFFF, "7FFFFFFF"_S.hexToNumber<int>());
	EXPECT_EQ(0x10, "10"_S.hexToNumber<int>());
	EXPECT_EQ(0x1012, "1012"_S.hexToNumber<int>());
	EXPECT_EQ(-0x1012, "-1012"_S.hexToNumber<int>());
}

TEST(String, FloatToString) {
	MemoryPool mp;
	EXPECT_EQ("0.123399999999999"_S, ToString(.1234)(mp));
	EXPECT_EQ("-0.12339999999999"_S, ToString(-.1234)(mp));
	EXPECT_EQ(2U, ToString(10.0)._size);
	EXPECT_EQ("10"_S, ToString(10.0)(mp));
	EXPECT_EQ("10.10999999999999"_S, ToString(10.11)(mp));
	EXPECT_EQ("100.9999899999999"_S, ToString(100.99999)(mp));
	EXPECT_EQ("101.1234"_S, ToString(101.1234567890f)(mp)); // to large number for float
	EXPECT_EQ("1012.111"_S, ToString(1012.1111111111111f)(mp)); // to large number for float
	EXPECT_EQ("101.1234567890000"_S, ToString(101.1234567890)(mp));
	EXPECT_EQ("1012.111111111111"_S, ToString(1012.1111111111111)(mp));
	EXPECT_EQ("-1012.11"_S, ToString(-1012.111f)(mp));
}

TEST(String, Number) {
	StaticMemoryPool<128> smp;
	for (short i = std::numeric_limits<short>::min(); i < std::numeric_limits<short>::max(); ++i) {
		smp.clear();
		String str = ToString(i)(smp);
		EXPECT_EQ(i, str.toNumber<short>());
	}
}

TEST(String, HexNumber) {
	StaticMemoryPool<128> smp;
	for (short i = std::numeric_limits<short>::min(); i < std::numeric_limits<short>::max(); ++i) {
		smp.clear();
		String str = ToHexString(i)(smp);
		EXPECT_EQ(i, str.hexToNumber<short>());
	}
}

TEST(String, MaxNumber) {
	StaticMemoryPool<128> smp;
	for (short i = std::numeric_limits<short>::min(); i < std::numeric_limits<short>::max(); ++i) {
		smp.clear();
		String str = ToMaxString(i)(smp);
		EXPECT_EQ(i, str.maxToNumber<short>());
	}
	EXPECT_EQ(9223372036854775807ULL, "7.........."_S.maxToNumber<unsigned long long>());
	EXPECT_EQ("7.........."_S, ToMaxString(9223372036854775807ULL)(smp));
	EXPECT_EQ(9223372036854775808ULL, "80000000000"_S.maxToNumber<unsigned long long>());
	EXPECT_EQ("80000000000"_S, ToMaxString(9223372036854775808ULL)(smp));

	EXPECT_EQ(std::numeric_limits<unsigned long long>::max(), "..........."_S.maxToNumber<unsigned long long>());
	EXPECT_EQ("f.........."_S, ToMaxString(std::numeric_limits<unsigned long long>::max())(smp));
	EXPECT_EQ("f........._"_S, ToMaxString(std::numeric_limits<unsigned long long>::max() - 1)(smp));
}

TEST(String, EscapeURL) {
	StaticMemoryPool<128> mp;
	String escaped = "Bla0+/:?%5D%5B%C3%AB";
	String escapedAlt = "Bla0%20/:?%5d%5b%c3%ab";
	String unescaped = "Bla0 /:?][ë"; // because this is unicode
	EXPECT_EQ(unescaped, escaped.unescape(String::Escape::URL, mp));
	EXPECT_EQ(unescaped, escapedAlt.unescape(String::Escape::URL, mp));
	EXPECT_EQ(escaped, unescaped.escape(String::Escape::URL, mp));
}

TEST(String, EscapeLikeC) {
	StaticMemoryPool<128> mp;
	String escaped = "\\a\\b\\f\\n\\r\\t\\v\\\\\\\"abc\\xC3\\xAB";
	String unescaped = "\a\b\f\n\r\t\v\\\"abcë";
	EXPECT_EQ(unescaped, escaped.unescape(String::Escape::LikeC, mp));
	EXPECT_EQ(escaped, unescaped.escape(String::Escape::LikeC, mp));
}

TEST(String, DecodeBase64) {
	StaticMemoryPool<128> mp;
	EXPECT_EQ("foobar", "Zm9vYmFy"_S.base64decode(mp));
}

TEST(String, EscapeHTML) {
	StaticMemoryPool<128> mp;
	String unescaped = "<>&html";
	String escaped = "&lt;&gt;&amp;html";
	//EXPECT_EQ(unescaped, escaped.unescape(String::Escape::HTML, mp));
	EXPECT_EQ(escaped, unescaped.escape(String::Escape::HTML, mp));
}

void TestJSONConversion(unsigned char* bytes, size_t length, unsigned int index, MemoryPool& mp) {
	if (index >= length) {
		auto rewinder = mp.rewinder();
		String unescaped = {reinterpret_cast<char*>(bytes), length};
		String escaped = unescaped.escape(String::Escape::JSON, mp);
		String decoded = escaped.unescape(String::Escape::JSON, mp);
		EXPECT_EQ(unescaped, decoded);
		return;
	}
	for (size_t i = 0; i < 256; ++i) {
		bytes[index] = uint8_t(i);
		TestJSONConversion(bytes, length, index + 1, mp);
	}
}

TEST(String, EscapeJSON) {
	StaticMemoryPool<256> mp;
	for (size_t length = 0; length < 3; ++length) {
		mp.clear();
		unsigned char* bytes = mp.allocBytes<unsigned char>(length);
		//std::cout << "testing for length " << length << std::endl;
		TestJSONConversion(bytes, length, 0, mp);
	}
}
TEST(String, EscapeJSONSamples) {
	StaticMemoryPool<256> mp;
	String escaped = "\\ue0b2\\ue0b0\\ue4fa\\uf0e7\\uf004\\uf49b"_S;
	String unescaped = escaped.unescape(String::Escape::JSON, mp);
	String expected = "";
	EXPECT_EQ(expected, unescaped) << ToHexString(unescaped);
	escaped = "\\uD801\\uDC37";
	unescaped = escaped.unescape(String::Escape::JSON, mp);
	expected = "𐐷"; //U+10437 (𐐷)
	EXPECT_EQ(expected, unescaped) << ToHexString(unescaped);
	escaped = "\\uD852\\uDF62";
	unescaped = escaped.unescape(String::Escape::JSON, mp);
	expected = u8"\U00024b62";
	EXPECT_EQ(expected, unescaped) << ToHexString(unescaped);
}

TEST(String, rspan) {
	static Char<' '> space;
	String test = "  abc  ";
	String remainder;
	String spanned = test.rspan(space, remainder);
	EXPECT_EQ(test.substring(0, 5), remainder);
	EXPECT_EQ("  abc", remainder);
	EXPECT_EQ(test.substring(5), spanned);
	EXPECT_EQ("  ", spanned);
}

TEST(String, span) {
	static Char<' '> space;
	String test = "  abc  ";
	String remainder;
	String spanned = test.span(space, remainder);
	EXPECT_EQ(test.substring(2), remainder);
	EXPECT_EQ("abc  ", remainder);
	EXPECT_EQ(test.substring(0, 2), spanned);
	EXPECT_EQ("  ", spanned);
}

TEST(String, split) {
	static Char<' '> space;
	String test = "abc def";
	String tail;
	short delim = '\0';
	String head = test.splitOn(space, tail, &delim);
	EXPECT_EQ(test.substring(0, 3), head);
	EXPECT_EQ("abc", head);
	EXPECT_EQ(test.substring(4, 3), tail);
	EXPECT_EQ("def", tail);
	EXPECT_EQ(' ', delim);
}

TEST(String, rsplit) {
	static Char<' '> space;
	String test = "abc def";
	String tail;
	short delim = '\0';
	String head = test.rsplitOn(space, tail, &delim);
	EXPECT_EQ(test.substring(4, 3), head);
	EXPECT_EQ("def", head);
	EXPECT_EQ(test.substring(0, 3), tail);
	EXPECT_EQ("abc", tail);
	EXPECT_EQ(' ', delim);
}

TEST(String, trim) {
	String test = "  a b c  ";
	String trimmed = test.trim();
	EXPECT_EQ("a b c", trimmed);
}

TEST(String, rsplit_string) {
	String test = "01234567890";
	String tail;
	String head = test.rsplit("456"_S, tail);
	EXPECT_EQ("0123", tail);
	EXPECT_EQ("7890", head);
	head = test.rsplit("890"_S, tail);
	EXPECT_EQ("01234567", tail);
	EXPECT_EQ("", head);

	tail = "foobar";
	head = test.rsplit(String(), tail);
	EXPECT_EQ("01234567890", head);
	EXPECT_EQ("", tail);
}

TEST(String, split_string) {
	String test = "01234567890";
	String tail;
	String head = test.split("456"_S, tail);
	EXPECT_EQ("0123", head);
	EXPECT_EQ("7890", tail);
	head = test.split("890"_S, tail);
	EXPECT_EQ("01234567", head);
	EXPECT_EQ("", tail);

	tail = "foobar";
	head = test.split(String(), tail);
	EXPECT_EQ("01234567890", head);
	EXPECT_EQ("", tail);
}

/*
TEST(HashedIgnoreCaseString, hashTest) {
  HashedIgnoreCase<String> test = ""_HS;
  EXPECT_EQ(test.hashIgnoreCase(), test.string().hashIgnoreCase());
  test = "abc";
  EXPECT_EQ(test.hashIgnoreCase(), test.string().hashIgnoreCase());
  test = "cde";
  EXPECT_EQ(test.hashIgnoreCase(), test.string().hashIgnoreCase());
  test = "lala";
  EXPECT_EQ(test.hashIgnoreCase(), test.string().hashIgnoreCase());
  test = "SpeedHTTPd";
  EXPECT_EQ(test.hashIgnoreCase(), test.string().hashIgnoreCase());
  EXPECT_TRUE("lala"_S == String("lala"_HS));
  EXPECT_TRUE("lala"_HSIC == StableHashedIgnoreCase<String>("lala"_S));
  EXPECT_TRUE("lala"_HSIC == HashedIgnoreCase<String>("lala"_S));
  EXPECT_FALSE("laLa"_S == String("lala"_HS));
  EXPECT_FALSE("laLa"_HSIC == StableHashedIgnoreCase<String>("lala"_S));
  EXPECT_FALSE("laLa"_HSIC == HashedIgnoreCase<String>("lala"_S));
  EXPECT_TRUE("laLa"_S.equalsIgnoreCase("lala"_HSIC));
  EXPECT_TRUE("laLa"_HSIC.equalsIgnoreCase("lala"_S));
}
*/

TEST(String, HashTest) {
	Hashed<String> test = ""_HS;
	EXPECT_EQ(test.hash(), StringUtil::Hash(test));
	test = "abc";
	EXPECT_EQ(test.hash(), StringUtil::Hash(test));
	test = "cde";
	EXPECT_EQ(test.hash(), StringUtil::Hash(test));
	test = "lala";
	EXPECT_EQ(test.hash(), StringUtil::Hash(test));
	test = "SpeedHTTPd";
	EXPECT_EQ(test.hash(), StringUtil::Hash(test));
	EXPECT_TRUE("lala"_S == String("lala"_HS));
	EXPECT_TRUE("lala"_SHS == StableHashed<String>("lala"_S));
	EXPECT_TRUE("lala"_HS == Hashed<String>("lala"_S));
	EXPECT_FALSE("laLa"_S == String("lala"_HS));
	EXPECT_FALSE("laLa"_SHS == StableHashed<String>("lala"_S));
	EXPECT_FALSE("laLa"_HS == Hashed<String>("lala"_S));
	EXPECT_FALSE("laLa"_S == "lala"_HS);
	EXPECT_FALSE("laLa"_HS == "lala"_S);

	EXPECT_EQ(0xeda34aaf, StringUtil::StableHashShort("foobar"_S));
	EXPECT_EQ(0xa2aa05ed9085aaf9LL, StringUtil::StableHashLong("foobar"_S));
}

TEST(String, contains) {
	String test = "01234567890";
	EXPECT_TRUE(test.contains("0123"));
	EXPECT_TRUE(test.contains("456"));
	EXPECT_TRUE(test.contains("9"));
	EXPECT_TRUE(test.contains("890"));
	EXPECT_FALSE(test.contains("9012"));
	EXPECT_FALSE(test.contains("465"));
	EXPECT_FALSE(test.contains("78901"));
}

TEST(String, equalsIgnoreCase) {
	EXPECT_TRUE("foobar"_S.equalsIgnoreCase("FooBar"_S));
	EXPECT_FALSE("foobar"_S.equalsIgnoreCase("FooBa"_S));
	EXPECT_FALSE("fooba"_S.equalsIgnoreCase("FooBar"_S));
}

TEST(String, StringHexConversion) {
	const std::pair<String, String> tests[] = {
		{"ABCDEF"_S, "414243444546"_S},
		{"a"_S, "61"_S},
		{"\0"_S, "00"_S},
		{"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14"_S, "000102030405060708090a0b0c0d0e0f1011121314"_S},
		{"\xff\xfe"_S, "fffe"_S},
		{"\xaa"_S, "aa"_S},
		{""_S, ""_S}
	};
	for (auto& t : tests) {
		auto& testIn = t.first;
		auto& testExpectedHex = t.second;
		StaticMemoryPool<128> smp;
		String testHex = ToHexString(testIn)(smp);
		EXPECT_EQ(testExpectedHex, testHex);
		String testOut = FromHexString(testHex)(smp);
		EXPECT_EQ(testOut, testIn);
	}
}

TEST(String, RawString) {
	StaticMemoryPool<128> mp;
	String t = ToRawString("12345"_S)(mp);
	EXPECT_EQ("\0\0\0\00512345"_S, t);
}

TEST(String, RawNumber) {
	StaticMemoryPool<128> mp;
	String t = ToRawU32(0x11223344)(mp);
	EXPECT_EQ("\x11\x22\x33\x44", t);
}

TEST(String, RawSame) {
	String key = "key";
	String attributes = "attributes";
	String valueType = "valueType";
	String value = "value";
	unsigned char CMD_PUT = 1;
	size_t packetContentLength = 1 + 4 + (4 + key.size()) + (4 + attributes.size()) + (4 + valueType.size()) + (4 + value.size());
	auto messageBuilder = StringContainer::Builder::CreateWithSize(4 + packetContentLength);
	RawWriter writer = messageBuilder.writer();
	writer.writeUnsignedInt32(uint32_t(packetContentLength));
	writer.writeUnsignedByte(CMD_PUT);
	writer.writeUnsignedInt32(0); // store request
	writer.writeString(key);
	writer.writeString(attributes);
	writer.writeString(valueType);
	writer.writeString(value);
	ASSERT_TRUE(writer.valid());
	StringContainer message = messageBuilder.build();

	StringContainer message2 = ToRawString(
															 ToRawU8(CMD_PUT) +
															 ToRawU32(0) +
															 ToRawString(key) +
															 ToRawString(attributes) +
															 ToRawString(valueType) +
															 ToRawString(value)
														 ).c();
	EXPECT_EQ(message, message2);
}


void toUTF8Conversions(String sourceString, String expectedString, int line) {
	StaticMemoryPool<128> mp;
	String generated = sourceString.toUTF8(mp);
	EXPECT_EQ(expectedString.size(), generated.size()) << line;
	EXPECT_EQ(expectedString, generated) << line;
}

void fromUTF8Conversions(String sourceString, String expectedString, int line) {
	StaticMemoryPool<128> mp;
	String generated = sourceString.toASCII(mp);
	EXPECT_EQ(expectedString.size(), generated.size()) << line << " " << generated;
	EXPECT_EQ(expectedString, generated) << sourceString << " line:" << line << " generated:" << ToHexString(generated) << " expecting:" << ToHexString(expectedString);
}


TEST(String, UTF8) {
	// test cases from http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt
	toUTF8Conversions("abcdefghijklmnopqrstuvwxyz", u8"abcdefghijklmnopqrstuvwxyz", __LINE__); // é
	toUTF8Conversions("\x82", u8"\u0082", __LINE__); // é
	toUTF8Conversions("\x84", u8"\u0084", __LINE__); // ä

	fromUTF8Conversions(u8"\u0082", "\x82", __LINE__); // é
	fromUTF8Conversions(u8"\u0084", "\x84", __LINE__); // ä
	fromUTF8Conversions("κόσμε", "", __LINE__);

	fromUTF8Conversions(u8"\u0000\u1234", "\u0000", __LINE__);
	fromUTF8Conversions(u8"\u0080", "\x80", __LINE__);
	fromUTF8Conversions(u8"\u00FF", "\xFF", __LINE__);

	fromUTF8Conversions("(agapó) ἀγαπῶ (ἀγαπᾶν) 𠜎", "(agap\xf3)  () ", __LINE__);

	fromUTF8Conversions("¢ĳ€𤭢𩸽", "\xa2", __LINE__);
}

#ifndef MEMORY_DEBUG
#ifndef TSAN_ENABLED // avoid false positives, as stack memory is reused
TEST(String, Append) {
	{
		const unsigned int SIZE = 260U;
		StaticMemoryPool<SIZE> mp;
		String current = " "_S(mp);
		EXPECT_EQ(SIZE - 1U, mp.available());

		EXPECT_TRUE(current.tryAppend("a", mp));
		EXPECT_EQ(SIZE - 2U, mp.available());

		EXPECT_TRUE(current.tryAppend("b", mp));
		EXPECT_EQ(SIZE - 3U, mp.available());

		EXPECT_TRUE(current.tryAppend("c", mp));
		EXPECT_EQ(SIZE - 4U, mp.available());

		EXPECT_EQ(" abc"_S, current);
	}
	const unsigned int SIZE = 256U;
	StaticMemoryPool<SIZE> mp;
	String current = " "_S(mp);
	EXPECT_EQ(SIZE - 1U, mp.available());

	EXPECT_TRUE(current.tryAppend("a", mp));
	EXPECT_EQ(SIZE - 2U, mp.available());

	EXPECT_TRUE(current.tryAppend("b", mp));
	EXPECT_EQ(SIZE - 3U, mp.available());

	EXPECT_TRUE(current.tryAppend("c", mp));
	EXPECT_EQ(SIZE - 4U, mp.available());

	EXPECT_EQ(" abc"_S, current);
}
#endif
#endif

TEST(String, ContainerSize) {
#if __WORDSIZE == 64
	EXPECT_EQ(12U, sizeof(StringContainerSharedContent<String>));
	EXPECT_EQ(16U, sizeof(StringContainerSharedContent<Hashed<>>));
	EXPECT_EQ(20U, sizeof(StringContainerSharedContent<Hashed<String, StringHashLong, StringUtil::HashLong>>));
	EXPECT_EQ(4U, sizeof(decltype(StringContainerSharedContent<String>::refcount)));
#else
	EXPECT_EQ(8U, sizeof(StringContainerSharedContent<String>));
	EXPECT_EQ(12U, sizeof(StringContainerSharedContent<Hashed<>>));
	EXPECT_EQ(16U, sizeof(StringContainerSharedContent<Hashed<String, StringHashLong, StringUtil::HashLong>>));
	EXPECT_EQ(4U, sizeof(decltype(StringContainerSharedContent<String>::refcount)));
#endif
}

TEST(StringContainer, NullByte) {
	{
		auto builder = StringC<StringT>::Builder::CreateWithSize(1);
		builder.pointer()[0] = 37;
		builder.pointer()[1] = 42;
		auto str = builder.build();
		const char* ptr = str.content().pointer();
		EXPECT_EQ(37, ptr[0]);
		EXPECT_EQ(0, ptr[1]);
	}

#ifdef __FreeBSD__
#if __WORDSIZE == 64
	{
		// highly dependent on the memory allocator used
		auto builderR = StringC<String>::Builder::CreateWithSize(4);
		auto str = builderR.build();
		EXPECT_EQ(12 + 4UL, str.allocated_size());

		builderR = StringC<String>::Builder::CreateWithSize(5);
		str = builderR.build();
		EXPECT_LE(12 + 5UL, str.allocated_size());
	}
#endif
#endif

#ifdef STRING_TAGGED_POINTER
	{
		// test for tagged pointers
		StringContainer str {"1"};
		EXPECT_EQ("1"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "12";
		EXPECT_EQ("12"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "123";
		EXPECT_EQ("123"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

#if __WORDSIZE == 64
		str = "1234";
		EXPECT_EQ("1234"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "12345";
		EXPECT_EQ("12345"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "123456";
		EXPECT_EQ("123456"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "1234567";
		EXPECT_EQ("1234567"_S, str.content());
		EXPECT_EQ(0UL, str.allocated_size());

		str = "12345678";
		EXPECT_EQ("12345678"_S, str.content());
		EXPECT_LE(12 + 8UL, str.allocated_size());
#else
		str = "1234";
		EXPECT_EQ("1234"_S, str.content());
		EXPECT_LT(0UL, str.allocated_size());
#endif
	}
#endif

#ifdef __FreeBSD__
#if __WORDSIZE == 64
	auto builder = StringC<StringT>::Builder::CreateWithSize(3);
	auto str = builder.build();
	EXPECT_EQ(12 + 3 + 1UL, str.allocated_size());

	builder = StringC<StringT>::Builder::CreateWithSize(4);
	str = builder.build();
	EXPECT_LE(12 + 4 + 1UL, str.allocated_size());
#endif
#endif
}

#ifdef STRING_TAGGED_POINTER
TEST(StringContainer, Tagged) {
	std::vector<StringContainer> array;
	for (size_t i = 0; i < 100000; ++i) {
		array.push_back(StringContainer(ToString(i)));
	}
	StaticMemoryPool<128> mp;
	for (size_t i = 0; i < array.size(); ++i) {
		EXPECT_EQ(ToString(i)(mp), array[i]) << i;
		mp.rewind();
	}
}
#endif

TEST(String, StringContainerSorting) {
	StringC<StableHashed<>> a {"caX"_S}; // smallest xxhash
	StringC<StableHashed<>> b {"abc"_S}; // middle xxhash
	StringC<StableHashed<>> c {"bca"_S}; // largest xxhash
	std::less<StringC<StableHashed<>>> hashCompare;
	std::less<String> stringCompare;

	EXPECT_TRUE(stringCompare(b.content(), a.content()));
	EXPECT_TRUE(stringCompare(b.content(), c.content()));
	EXPECT_TRUE(stringCompare(c.content(), a.content()));
	EXPECT_FALSE(stringCompare(a.content(), b.content()));
	EXPECT_FALSE(stringCompare(c.content(), b.content()));
	EXPECT_FALSE(stringCompare(a.content(), c.content()));

	EXPECT_LT(b.content().string(), a.content().string());
	EXPECT_LT(b.content().string(), c.content().string());
	EXPECT_LT(c.content().string(), a.content().string());

	EXPECT_GE(a.content().string(), b.content().string());
	EXPECT_GE(c.content().string(), b.content().string());
	EXPECT_GE(a.content().string(), c.content().string());

	EXPECT_TRUE(hashCompare(a, b));
	EXPECT_TRUE(hashCompare(b, c));
	EXPECT_TRUE(hashCompare(a, c));
	EXPECT_FALSE(hashCompare(b, a));
	EXPECT_FALSE(hashCompare(c, b));
	EXPECT_FALSE(hashCompare(c, a));

	EXPECT_LT(a, b);
	EXPECT_LT(b, c);
	EXPECT_LT(a, c);

	EXPECT_GT(b, a);
	EXPECT_GT(c, b);
	EXPECT_GT(c, a);
}

TEST(String, Replace) {
	EXPECT_EQ("defdef"_S, "abcdef"_S.replace("abc", "def").content());
	EXPECT_EQ("defdef"_S, "abcabc"_S.replace("abc", "def").content());
	EXPECT_EQ("defdef"_S, "defdef"_S.replace("abc", "def").content());

	StaticMemoryPool<512> mp;
	EXPECT_EQ("defdef"_S, "abcdef"_S.replace("abc", "def", mp));
	EXPECT_EQ("defdef"_S, "abcabc"_S.replace("abc", "def", mp));
	EXPECT_EQ("defdef"_S, "defdef"_S.replace("abc", "def", mp));
}


struct TestToStringObject {
	concurrent_count refcount;
};

TEST(String, ConversionOfPointers) {
	const String* foo = nullptr;
	StaticMemoryPool<128> mp;
	EXPECT_EQ("string: 0x0", ("string: "_S + foo)(mp));

	String* bar = nullptr;
	EXPECT_EQ("string: 0x0", ("string: "_S + bar)(mp));

	shared_object<TestToStringObject> testObject = nullptr;
	EXPECT_EQ("string: 0x0", ("string: "_S + testObject)(mp));
}

TEST(String, LessVersion) {
	LessVersion lv;
	EXPECT_FALSE(lv("file1.ext"_S, "file1.ext"_S));
	EXPECT_FALSE(lv("file10.ext"_S, "file10.ext"_S));

	EXPECT_TRUE(lv("file1.ext"_S, "file10.ext"_S));
	EXPECT_TRUE(lv("file2.ext"_S, "file11.ext"_S));
	EXPECT_TRUE(lv("file10.ext"_S, "file11.ext"_S));
	EXPECT_TRUE(lv("file123.ext"_S, "file1234.ext"_S));

	EXPECT_FALSE(lv("file10.ext"_S, "file1.ext"_S));
	EXPECT_FALSE(lv("file11.ext"_S, "file2.ext"_S));
	EXPECT_FALSE(lv("file11.ext"_S, "file10.ext"_S));
	EXPECT_FALSE(lv("file1234.ext"_S, "file123.ext"_S));
}

TEST(DISABLED_String, HashShort) {
	size_t size = 1 * 1024 * 1024;
	char* buffer = static_cast<char*>(malloc(size));
	//bitpowder::crypto::RandomBytes(buffer, size);
	String s = {buffer, size};
	for (size_t i = 256; i-- > 0; ) {
		StringUtil::StableHashShort(s);
	}
	free(buffer);
}

TEST(DISABLED_String, HashLong) {
	size_t size = 1 * 1024 * 1024;
	char* buffer = static_cast<char*>(malloc(size));
	//bitpowder::crypto::RandomBytes(buffer, size);
	String s = {buffer, size};
	for (size_t i = 256; i-- > 0; ) {
		StringUtil::StableHashLong(s);
	}
	free(buffer);
}

TEST(String, Various) {
	StaticMemoryPool<128> mp;
	String a = "     "_S(mp); // 5 bytes
	String b = a;
	("foobar"_S).op().to(b);
	EXPECT_EQ("fooba"_S, a);
	EXPECT_EQ(""_S, b);

	// STL
	std::string x = "foobar"_S.op().stl();
	EXPECT_EQ(std::string("foobar"), x);
}

// crashes on windows
#ifndef _WIN32
TEST(StringContainer, Size) {
	// Smaller sizes for the increments gives unreliable tests, as sometimes larger pieces of memory are used for smaller allocations
	for (size_t i = 16 * 1024 * 1024; i <= 128 * 1024 * 1024; i += 16 * 1024 * 1024) {
		size_t expectedSize = i;
		StringContainer t = StringContainer::Builder::CreateWithMax(expectedSize).build();
		void* reference = malloc_with_minimal_size(expectedSize);
		EXPECT_EQ(allocation_size(reference), t.allocated_size()) << "allocation of size " << i;
		free(reference);
	}
}
#endif

}

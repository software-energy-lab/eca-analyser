/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __FreeBSD__
#include <malloc_np.h> // FreeBSD
#elif defined(__APPLE__)
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include "memory.h"
#include "simplestring.h"

namespace bitpowder {
namespace lib {

#ifdef MEMORY_CACHE
MemoryPage* MemoryPage::BUSY = reinterpret_cast<MemoryPage*>(-1UL);
#endif


std::set<CacheOfInstancesBase*>* CacheOfInstancesBase::GetAllInstanceCaches() {
	static std::set<CacheOfInstancesBase*>* retval = new std::set<CacheOfInstancesBase*>();
	return retval;
}

size_t CacheOfInstancesBase::_PurgeAll() {
	size_t retval = 0;
	for (auto it : *GetAllInstanceCaches())
		retval += it->purge();
	return retval;
}

void CacheOfInstancesBase::_StatsAll() {
	StaticMemoryPool<1024> mp;
	std::cout << _StatsAll(mp);
}

String CacheOfInstancesBase::_StatsAll(MemoryPool& mp) {
	String retval;
	for (auto it : *GetAllInstanceCaches()) {
		auto [activeCount, destructedCount, cacheCount, className] = it->stats();
		retval = retval.append("- "_S + String(className, String::UNKNOWN_LENGTH) + ": heap=" + activeCount + " freed=" + destructedCount + " cached=" + cacheCount + "\n", mp);
		free(className);
	}
	return retval;
}

size_t _allocation_size(void* data) {
	if (!data)
		return 0;
#ifdef __APPLE__
	return malloc_size(data);
#elif __FreeBSD__
	return malloc_usable_size(data);
#elif __linux__
	return malloc_usable_size(data);
#elif _WIN32
	return _msize(data);
#else
	abort();
#endif
}

void* _malloc_with_minimal_size(size_t& bytes) {
	void* data = malloc(bytes);
	bytes = allocation_size(data);
	return data;
}

void* _realloc_with_minimal_size(void* data, size_t& bytes) {
	data = realloc(data, bytes);
	bytes = allocation_size(data);
	return data;
}

size_t Memory::Purge() {
	return MemoryPage::Purge() + CacheOfInstancesBase::_PurgeAll();
}

void Memory::Stats() {
	CacheOfInstancesBase::_StatsAll();
}

String Memory::Stats(MemoryPool& mp) {
	return CacheOfInstancesBase::_StatsAll(mp);
}

#ifdef MEMORY_CACHE
std::atomic<MemoryPage*> MemoryPage::Pages = {nullptr};
#endif

struct MemoryInit {
	MemoryInit() {
		std::set_new_handler(handle);
	}
	static void handle() {
		// the memory allocation is automatically retried when this method ends, so if no memory clean up, it could not satisfy the allocation
		if (Memory::Purge() == 0)
			throw std::bad_alloc();
	}
};

MemoryInit init;

}
}

/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <stdio.h>
#include <ctype.h>
#include <typeinfo>
#include <cstdlib>

/**
 * Fast manner of testing types of certain classes, type should be stored in an integer.
 * Typical use case: virtual base class with templated derived classes, and to check if a derived class is of a certain type.
 *
 * Background information: http://www.nerdblog.com/2006/12/how-slow-is-dynamiccast.html
 */

namespace bitpowder {
namespace lib {

typedef const size_t* FastTypeT;
#define FastTypeEmpty nullptr

template <class T>
struct FastType {
	static const size_t ValueAddr;
	static constexpr const FastTypeT Value = &ValueAddr;

	template <class A, FastTypeT A::*type = &A::type>
	static bool PointerOfType(A* a) {
		return a != nullptr && a->*type == Value;
	}

	template <class A, FastTypeT A::*type = &A::type>
	static T * Cast(A* a) {
		return PointerOfType(a) ? static_cast<T*>(a) : nullptr;
	}

	template <class A, FastTypeT A::*type = &A::type>
	static const T * Cast(const A* a) {
		return PointerOfType(a) ? static_cast<const T*>(a) : nullptr;
	}

};

template <class T> const size_t FastType<T>::ValueAddr = typeid(T).hash_code();
template <class T> constexpr const FastTypeT FastType<T>::Value;

}
}


/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "type_hash.h"
#include "exception.h"
#include "simplestring.h"
#include <iostream>
#include <type_traits>
#include <tuple>

/**
 * TODO:
 * - make the tokenizer read in extra data if needed (as in streaming parser)
 * - make the parser return if the tokenizer does not have enough data available to return the next token (remember all the states and choices made by the parser, so if new data is available, it is possible to continue)
 * - make the JSON parser more dynamic with user callbacks, allowing the user to influence which fields are remembered, and possible adjust values on the fly
 */

namespace bitpowder {
namespace lib {

class TokenBase {
public:
	FastTypeT type;
	StringLength start = 0;
	StringLength length = 0;
	constexpr TokenBase(FastTypeT _type) : type(_type) {
	}
	TokenBase(const TokenBase& rhs) = default;
	TokenBase& operator=(const TokenBase& rhs) = default;
	virtual ~TokenBase() { }
	virtual void print(std::ostream& out) const = 0;
};

template <class T>
class Token : public TokenBase {
public:
	T value;
	constexpr Token() : TokenBase(FastType<Token<T>>::Value), value() {
		//std::cout << "Token default " << typeid(T).name() << "=" << FastType<Token<T>>::Value << std::endl;
	}
	constexpr Token(const T& _value) : TokenBase(FastType<Token<T>>::Value), value(_value) {
		//std::cout << "TokenConst with value " << value << " hash=" << type << std::endl;
	}
	constexpr Token(T&& _value) : TokenBase(FastType<Token<T>>::Value), value(std::move(_value)) {
		//std::cout << "TokenMove with value " << value << " hash=" << type << std::endl;
	}
	virtual void print(std::ostream& out) const {
#ifndef NDEBUG
		out << typeid(T).name() << "(" << value << ")";
#else
		out << value;
#endif
	}
	operator T() const {
		return value;
	}
};

//#define PARSER_DEBUG
//#define PARSER_USE_DYNAMIC_CAST

template <class Lexer, int N>
class ParserState {
public:
	Lexer* lexer;
	TokenBase* current[N];

	void step() {
		//std::cout << "step: " << current[0] << " gone" << std::endl;
		for (int i = 0; i < N - 1; ++i)
			current[i] = current[i + 1];
		current[N - 1] = lexer->next(); //lexer->end() ? nullptr : lexer->next();
		//std::cout << "new token: " << current[N-1] << std::endl;
	}
	ParserState& operator=(const ParserState& c) = delete;
public:
	ParserState(Lexer& _lexer) : lexer(&_lexer), current() {
		for (int i = 0; i < N; ++i)
			current[i] = lexer->next();
#ifdef PARSER_DEBUG
		std::cout << "start state: ";
		print(std::cout) << std::endl;
#endif
	}
	ParserState(const ParserState& c) : lexer(c.lexer), current() {
		for (int i = 0; i < N; ++i)
			current[i] = c.current[i];
	}

	template <int I = 0>
	typename std::enable_if<I >= 0 && I <N, TokenBase*>::type getToken() {
		return current[I];
	}
	std::ostream& print(std::ostream& out) const {
		out << "[";
		for (int i = 0; i < N; ++i) {
			if (i > 0)
				out << ", ";
			out << current[i];
		}
		out << "]";
		return out;
	}
};

struct ParserError {
	int code = 0;
	String msg;
	ParserError(int _code = 0, const String& _msg = {}) : code(_code), msg(_msg) {
	}
	ParserError(String _msg) : code(std::numeric_limits<int>::max()), msg(_msg) {
	}
	int error() const {
		return code;
	}
	String message() const {
		return msg;
	}
	// indicates error
	operator bool() const {
		return code;
	}
};

template <class Lexer, int N, class T, class Data>
class Parser {
public:
	typedef ParserState<Lexer, N> ParserStateType;
	const static int DEFAULT_ERROR = std::numeric_limits<int>::max();

public:
	ParserStateType* p;
	T result;

	ParserError err; // 0 = no error
	int test; // > 0 means a conditional operator (like choose()) is in effect, and uses a lookahead
	int acceptIndex; //
	Data userData; // data that is not returned, but used (like a dictionary lookup etc)
public:
	typedef Parser& (*Cont)(Parser&);

	Parser(ParserStateType* parserState, Data _userData, int _test = 0, int _acceptIndex = 0, const T& _result = T()) : p(parserState), result(_result), test(_test), acceptIndex(_acceptIndex), userData(_userData) {
	}
	~Parser() {
	}
	Parser(Parser&& c) : p(c.p), result(c.result), err(c.err), test(c.test), acceptIndex(c.acceptIndex), userData(std::move(c.userData)) {
	}
	Parser(const Parser& c) : p(c.p), result(c.result), err(c.err), test(c.test), acceptIndex(c.acceptIndex), userData(c.userData) {
	}
	Parser& operator=(const Parser& c) {
		p = c.p;
		result = c.result;
		err = c.err;
		test = c.test;
		acceptIndex = c.acceptIndex;
		userData = c.userData;
		return *this;
	}
	Data& data() {
		return userData;
	}
	bool testing() {
		return test > 0;
	}

	// expect end
	Parser& end() {
		if (!err && p->current[0])
			err = {DEFAULT_ERROR};
		return *this;
	}

	// if there is an undefined error, set the error code to argument
	Parser& error(ParserError onError) {
		if (err.code == DEFAULT_ERROR)
			err = onError;
		return *this;
	}

	// indicate there is an error
	Parser& setError(int setErrorTo, String setErrorMessage = String()) {
		err = {setErrorTo, setErrorMessage};
		return *this;
	}
	Parser& setError(ParserError setErrorTo) {
		err = setErrorTo;
		return *this;
	}

	Parser& getError(ParserError& to) {
		to = err;
		return *this;
	}

	// fetch the current token and store it as the current value of the parser
	Parser& fetch() {
		if (!err && (!test || acceptIndex < N)) {
			TokenBase* token = p->current[acceptIndex];
			Token<T>* c = nullptr;
			if (token && FastType<Token<T>>::PointerOfType(token))
				c = static_cast<Token<T>*>(token);
#ifdef PARSER_USE_DYNAMIC_CAST
			else if (token)
				c = dynamic_cast<Token<T>*>(token);
#endif
			if (c) {
				if (test) {
					acceptIndex++;
				} else {
					result = std::move(c->value);
					p->step();
				}
			} else
				err = {DEFAULT_ERROR, "error"};
		}
		return *this;
	}

	// accept a token with value expectedToken
	template <class S>
	Parser& _accept(const S& expectedToken) {
		if (!err && (!test || acceptIndex < N)) {
#ifdef PARSER_DEBUG
			std::cout << "accept(" << typeid(S).name() << "(" << expectedToken << ")) ";
			p->print(std::cout);
			std::cout << " resulted in" << std::endl;
#endif
			TokenBase* token = p->current[acceptIndex];
			Token<S>* c = nullptr;
			if (token && FastType<Token<S>>::PointerOfType(token))
				c = static_cast<Token<S>*>(token);
#ifdef PARSER_USE_DYNAMIC_CAST
			else if (token)
				c = dynamic_cast<Token<S>*>(token);
#endif
#ifdef PARSER_DEBUG
			if (!c)
				std::cout << "could not cast current value " << (token ? token->type : nullptr) << " to correct type: " << typeid(S).name() << "=" << FastType<Token<S>>::Value << std::endl;
#endif
			if (c && c->value == expectedToken) {
				if (test) {
					acceptIndex++;
				} else
					p->step();
			} else
				err = {DEFAULT_ERROR, "error"};
#ifdef PARSER_DEBUG
			p->print(std::cout);
			if (acceptIndex >= N)
				std::cout << "lookahead reached (" << acceptIndex << ")";
			std::cout << " with error " << err << std::endl;
#endif
		}
		return *this;
	}

	// accept a token with value expectedToken
	template <class S>
	Parser& token(S& storeTo) {
		if (!err && (!test || acceptIndex < N)) {
#ifdef PARSER_DEBUG
			std::cout << "accept(" << typeid(S).name() << ") ";
			p->print(std::cout);
			std::cout << " resulted in" << std::endl;
#endif
			TokenBase* token = p->current[acceptIndex];
			Token<S>* c = nullptr;
			if (token && FastType<Token<S>>::PointerOfType(token))
				c = static_cast<Token<S>*>(token);
#ifdef PARSER_USE_DYNAMIC_CAST
			else if (token)
				c = dynamic_cast<Token<S>*>(token);
#endif
#ifdef PARSER_DEBUG
			if (!c)
				std::cout << "could not cast current value " << (token ? token->type : nullptr) << " to correct type: " << typeid(S).name() << "=" << FastType<Token<S>>::Value << std::endl;
#endif
			if (c) {
				if (test) {
					acceptIndex++;
				} else {
					storeTo = c->value;
					p->step();
				}
			} else
				err = {DEFAULT_ERROR, "error"};
#ifdef PARSER_DEBUG
			p->print(std::cout);
			if (acceptIndex >= N)
				std::cout << "lookahead reached (" << acceptIndex << ")";
			std::cout << " with error " << err << std::endl;
#endif
		}
		return *this;
	}

	// helper class
	template <class S>
	Parser& _accept(const Token<S>& expectedToken) {
		return _accept<S>(expectedToken.value);
	}

	Parser& accept() {
		err = {DEFAULT_ERROR, "error"};
		return *this;
	}

	template <typename S>
	Parser& accept(S&& first) {
		return _accept(first);
	}

	template <typename S, typename... Args>
	Parser& accept(S&& first, Args&& ... args) {
		if (err)
			return *this;
		test++;
		int ai = acceptIndex;
		_accept(first);
		test--;
		acceptIndex = ai;
		if (!err)
			return _accept(first);
		err = 0;
		return accept(std::forward<Args>(args)...);
	}

	// execute another rule
	Parser& perform1(Cont path) {
		return err || (test && acceptIndex >= N) ? *this : path(*this);
	}

	template <typename ContAlt>
	Parser& perform1(ContAlt&& path) {
		return err || (test && acceptIndex >= N) ? *this : path(*this);
	}

	// handy shortcut to accept a token of a certain type
	template <class TokenType>
	Parser& perform1(const Token<TokenType>& expectedValue) {
		return _accept(expectedValue);
	}

	// handy shortcut to accept a token of a certain type
	template <class TokenType>
	Parser& perform1(TokenType& value) {
		return fetch<TokenType>(value);
	}

	// execute a rule to incorporate a certain token in the current type
	template <class Type>
	Parser& perform1(int (*construct)(T&, const Token<Type>&, Data userData)) {
		if (!err && (!test || acceptIndex < N)) {
			TokenBase* token = p->current[acceptIndex];
			Token<Type>* c = nullptr;
			if (token && FastType<Token<Type>>::PointerOfType(token))
				c = static_cast<Token<Type>*>(token);
#ifdef PARSER_USE_DYNAMIC_CAST
			else if (token)
				c = dynamic_cast<Token<Type>*>(token);
#endif
#ifdef PARSER_DEBUG
			std::cout << "perform1() resulted in ";
			if (token)
				token->print(std::cout);
			std::cout << "/" << c << " ParserState(" << p->current[acceptIndex] << ", ";
#endif
			if (c) {
				if (test) {
					acceptIndex++;
				} else {
					err = std::move(construct(result, *c, userData));
					p->step();
				}
			} else
				err = {DEFAULT_ERROR, "error"};
#ifdef PARSER_DEBUG
			std::cout << err << ", result=" << result << ")" << std::endl;
#endif
		}
		return *this;
	}

	// perform can execute all kinds of actions
	template<typename Type>
	Parser& perform(Type&& path) {
		return err ? *this : perform1(std::forward<Type>(path));
	}

	// perform can execute all kinds of actions
	template<typename Type, typename... Args>
	Parser& perform(Type&& path, Args&& ... path2) {
		return err ? *this : perform1(std::forward<Type>(path)).perform(std::forward<Args>(path2)...);
	}

	// execute the rule indicated in path and store the result of the rule in 'result'
	Parser& fetch(Cont&& path, T& returnResult) {
		if (err)
			return *this;
		T a = std::move(result);
		perform1(std::forward<Cont>(path));
		if (!err)
			returnResult = std::move(result);
		result = std::move(a);
		return *this;
	}

	// store the token of type T in the variable 'result'
	Parser& fetch(T& returnResult) {
		if (err)
			return *this;
		T a = std::move(result);
		fetch();
		if (!err)
			returnResult = std::move(result);
		result = std::move(a);
		return *this;
	}

	// store the token of type NewT in the variable 'result'
	template <class NewT>
	Parser& fetch(NewT& returnResult) {
		if (err)
			return *this;
		Parser<Lexer, N, NewT, Data> tParser(p, userData, test, acceptIndex);
		tParser.fetch();
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			returnResult = std::move(tParser.result);
		return *this;
	}

	// store the result of the rule indicated in 'result'
	template <class NewT, class Type>
	Parser& fetch(int (*construct)(NewT& retval, const Token<Type>&, Data userData), NewT& returnResult, const NewT& startResult = NewT()) {
		if (err)
			return *this;
		Parser<Lexer, N, NewT, Data> tParser(p, userData, test, acceptIndex, startResult);
		tParser.perform(construct);
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			returnResult = std::move(tParser.result);
		return *this;
	}

	// store the result of the rule indicated in 'result'
	template <class NewT>
	Parser& fetch(Parser<Lexer, N, NewT, Data>& (*path)(Parser<Lexer, N, NewT, Data>& cont), NewT& returnResult, const NewT& startResult = NewT()) {
		if (err)
			return *this;
		Parser<Lexer, N, NewT, Data> tParser(p, userData, test, acceptIndex, startResult);
		tParser.perform(path);
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			returnResult = std::move(tParser.result);
		return *this;
	}

	// execute some rule, and process the result with a function
	template <typename... Args>
	Parser& process(Cont path, Args&& ... args) {
		T value = {};
		return fetch(path, value).modify(std::forward<Args>(args)..., std::move(value));
	}

	// execute some rule, and process the result with a function
	template <class NewT, class Type, typename... Args>
	Parser& process(int (*construct)(NewT& retval, const Token<Type>&, Data userData), Args&& ... args) {
		if (err)
			return *this;
		Parser<Lexer, N, NewT, Data> tParser(p, userData, test, acceptIndex);
		tParser.perform(construct);
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			modify(std::forward<Args>(args)..., std::move(tParser.result));
		return *this;
	}

	// execute some rule, and process the result with a function
	template <class NewT, typename... Args>
	Parser& process(Parser<Lexer, N, NewT, Data>& (*path)(Parser<Lexer, N, NewT, Data>& cont), Args&& ... args) {
		if (err)
			return *this;
		Parser<Lexer, N, NewT, Data> tParser(p, userData, test, acceptIndex);
		tParser.perform(path);
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			modify(std::forward<Args>(args)..., std::move(tParser.result));
		return *this;
	}

	// execute some rule, and process the result with a function
	template <typename... Args>
	Parser& scope(Data scopedUserData, Cont path, Args&& ... args) {
		if (err)
			return *this;
		Parser<Lexer, N, T, Data> tParser(p, scopedUserData, test, acceptIndex);
		tParser.perform(path);
		err = tParser.err;
		test = tParser.test;
		acceptIndex = tParser.acceptIndex;
		if (!err)
			modify(std::forward<Args>(args)..., std::move(tParser.result));
		return *this;
	}

	// optionally repeat this rule
	template<typename... Args>
	Parser& repeat(Args&& ... path) {
		while (!err && (!test || acceptIndex < N)) {
			test++;
			int ai = acceptIndex;
			perform(std::forward<Args>(path)...);
			test--;
			bool ok = !err && acceptIndex > ai; // check if progress is made, no progress is stop rule
			acceptIndex = ai;
			err = 0;
			if (!ok)
				return *this;
			perform(std::forward<Args>(path)...);
		}
		return *this;
	}

	// optionally repeat this rule
	template<typename... Args>
	Parser& repeatChoose(Args&& ... path) {
		while (!err && (!test || acceptIndex < N)) {
			test++;
			int ai = acceptIndex;
			choose(std::forward<Args>(path)...);
			test--;
			bool ok = !err && acceptIndex > ai; // check if progress is made, no progress is stop rule
			acceptIndex = ai;
			err = 0;
			if (!ok)
				return *this;
			choose(std::forward<Args>(path)...);
		}
		return *this;
	}

	// optionally execute this rule
	template<typename... Args>
	Parser& opt(Args&& ... path) {
		if (err)
			return *this;
		if (test && acceptIndex == N)
			return *this;
		test++;
		int ai = acceptIndex;
		perform(std::forward<Args>(path)...);
		test--;
		bool ok = !err && acceptIndex > ai;
		acceptIndex = ai;
		err = 0;
		if (ok)
			return perform(std::forward<Args>(path)...);
		return *this;
	}

	// optionally execute one of the mentioned rules
	template<typename... Args>
	Parser& optChoose(Args&& ... path) {
		if (err)
			return *this;
		if (test && acceptIndex == N)
			return *this;
		test++;
		int ai = acceptIndex;
		choose(std::forward<Args>(path)...);
		test--;
		bool ok = !err && acceptIndex > ai;
		acceptIndex = ai;
		err = 0;
		if (ok)
			return choose(std::forward<Args>(path)...);
		return *this;
	}

	// choose one of the given options
	template <typename Type>
	Parser& choose(Type path) {
		if (test && acceptIndex == N)
			return *this;
		return perform1(std::forward<Type>(path));
	}

	// choose one of the given options
	template<typename Type, typename... Args>
	Parser& choose(Type path, Args&& ... path2) {
		if (err)
			return *this;
		if (test && acceptIndex == N)
			return *this;
		test++;
		int ai = acceptIndex;
		perform1(path);
		test--;
		bool ok = !err && acceptIndex > ai;
		acceptIndex = ai;
		err = 0;
		if (ok)
			return perform1(path);
		return choose(std::forward<Args>(path2)...);
	}

	// retreive the current value
	Parser& retreive(T& returnResult) {
		if (!err && !test) {
			returnResult = std::move(result);
		}
		return *this;
	}

	// store a value in the parser
	Parser& store(T (*func)(Data userData)) {
		if (!err && !test)
			this->result = std::move(func(userData));
		return *this;
	}

	// store this value in the parser
	Parser& store(T&& newResult) {
		if (!err && !test)
			result = std::move(newResult);
		return *this;
	}

	// store this value in the parser
	Parser& store(const T& newResult) {
		if (!err && !test)
			result = newResult;
		return *this;
	}

	template <typename Func, typename... Extra>
	Parser& apply(const Func& construct, Extra&& ... extra) {
		if (!err && !test)
			result = std::move(construct(std::move(result), std::forward<Extra>(extra)...), userData);
		return *this;
	}

	template <typename Func, typename... Extra>
	//C++14 typename std::enable_if<std::is_int<typename std::result_of<>::type>::value,Parser&>::type
	Parser& modify(const Func& construct, Extra&& ... extra) {
		if (!err && !test)
			err = construct(result, std::forward<Extra>(extra)..., userData);
		return *this;
	}

	Parser& debug(const String& str) {
		USING(str);
		// if (!err && (!test || acceptIndex < N)) {
		//  if (test)
		//    std::cout << "testing: ";
		//  std::cout << str << std::endl;
		// }
		return *this;
	}

	Parser& operator()() {
		return *this;
	}

	operator ParserError() const {
		return err;
	}
};

template <class ResultType>
class ParseResult {
	bool success = false;
	unsigned long pos = 0;
	lib::StringContainer errorMessage;
	lib::String currentToken;
	ResultType retval;
public:
	ParseResult(unsigned long position, const lib::String& currentInput, const lib::String& _errorMessage) : pos(position), errorMessage(_errorMessage), currentToken(currentInput) {
	}
	ParseResult(const ResultType& _retval) : success(true), retval(_retval) {
	}
	ParseResult(ResultType&& _retval) : success(true), retval(std::move(_retval)) {
	}
	operator bool() const {
		return success;
	}
	ResultType& result() {
		return retval;
	}
	ResultType& operator*() {
		return retval;
	}
	ResultType* operator->() {
		return &retval;
	}
	lib::String error() const {
		return errorMessage.content();
	}
	lib::String currentInput() const {
		return currentToken;
	}
	unsigned long position() const {
		return pos;
	}
};

}
}

namespace std {
std::ostream& operator<< (std::ostream& out, const bitpowder::lib::TokenBase* t);

template <class ResultType>
std::ostream& operator<< (std::ostream& out, bitpowder::lib::ParseResult<ResultType>& parseResult) {
	out << "[result pos=" << parseResult.position() << " error=" << parseResult.error() << " result=" << parseResult.result() << "]";
	return out;
}
}


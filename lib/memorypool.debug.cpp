#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include <vector>

#include "memorypool.h"

#define DEFAULT_SIZE 1024
//#define NO_MEMORYPOOL_ALIGNMENT

namespace bitpowder {
namespace lib {

// possibly needed for address sanitizer etc
void memorypool_free(void* ptr) {
	delete[] static_cast<char*>(ptr);
}

void* memorypool_malloc(size_t size) {
	return static_cast<void*>(new char[size]);
}

void MemoryPool::clear(MemoryPoolStatus status) {
	std::vector<MemoryPoolItem*> toBeFreed;
	// pass 1, execute delayed items, but not free()'s
	while (!delayed.empty() && delayed.back() != status.delayed) {
		MemoryPoolItem* c = delayed.pop();
		// due too dependencies if using MemoryPool Allocator, the deallocation of objects should be delayed
		if (c->func && c->func != &memorypool_free) {
			(c->func)(c->data);
			c->func = nullptr;
		}
		toBeFreed.push_back(c);
	}
	checkAssert(!delayed.empty() || status.delayed == nullptr);
	checkAssert(delayed.back() == status.delayed);
	// pass 2, free allocated memory
	for (MemoryPoolItem* c : toBeFreed) {
		if (c->func != nullptr)
			(c->func)(c->data);
		delete c;
	}
}

MemoryPoolStatus MemoryPool::status() {
	return MemoryPoolStatus(delayed.back());
}

void MemoryPool::addMemory(void* data [[maybe_unused]], size_t _size [[maybe_unused]]) {
}

void MemoryPool::addMemory(MemoryPool* mp [[maybe_unused]]) {
}

MemoryPool::MemoryPool() {
}

MemoryPool::MemoryPool(size_t size, void* data) {
	addMemory(data, size);
}

MemoryPool::~MemoryPool() {
	clear();
}

void MemoryPool::clear() {
	if (!delayed.empty()) {
		MemoryPoolStatus status = {nullptr};
		clear(status);
		checkAssert(delayed.empty());
	}
}

void MemoryPool::rewind() {
	MemoryPoolStatus status = {nullptr};
	rewind(status);
}

void* MemoryPool::_alloc(size_t size) {
	MemoryPoolItem* i = new MemoryPoolItem();
	checkAssert(i);
	i->func = memorypool_free;
	i->data = memorypool_malloc(size);
	delayed.push_back(i);
	return i->data;
}

void* MemoryPool::_allocAtLeast(size_t& size) {
	MemoryPoolItem* i = new MemoryPoolItem();
	checkAssert(i);
	i->func = memorypool_free;
	i->data = memorypool_malloc(size);
	delayed.push_back(i);
	return i->data;
}

void* MemoryPool::allocAll(size_t& size) {
	size_t requestedSize = DEFAULT_SIZE;
	void* retval = _alloc(requestedSize);
	if (retval)
		size = requestedSize;
	return retval;
}

char* MemoryPool::strndup(const char* src, size_t len) {
	if (src == nullptr)
		return nullptr;
	char* dst = static_cast<char*>(_alloc(len + 1));
	if (dst) {
		memcpy(dst, src, len);
		dst[len] = '\0';
	}
	return dst;
}

char* MemoryPool::strdup(const char* src) {
	if (src == nullptr)
		return nullptr;
	//return mpmemdup(src, strlen(src)+1);
	return strndup(src, strlen(src));
}

int MemoryPool::vprintf(char** strPtr, size_t max, const char* format, va_list ap) {
	char* str = static_cast<char*>(_alloc(max));
	checkAssert(str);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
	int size = vsnprintf(str, max, format, ap);
#pragma clang diagnostic pop
	if (size_t(size) >= max) {
		str[max - 1] = '\0';
	}
	if (strPtr != nullptr)
		*strPtr = str;
	return size;
}

int MemoryPool::printf(char** strPtr, size_t max, const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	int retval = vprintf(strPtr, max, format, ap);
	va_end(ap);
	return retval;
}

// not terminating the string, useful for generating byte strings
int MemoryPool::vprintf_nt(char** strPtr, size_t max, const char* format, va_list ap) {
	return vprintf(strPtr, max + 1, format, ap);
}

int MemoryPool::printf_nt(char** strPtr, size_t max, const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	int retval = vprintf_nt(strPtr, max, format, ap);
	va_end(ap);
	return retval;
}

void destroyMemoryPool(void* data) {
	MemoryPool* mp = static_cast<MemoryPool*>(data);
	destroy<MemoryPool>(mp);
}

MemoryPool::MemoryPool(MemoryPool* from, size_t size [[maybe_unused]]) {
	if (from) {
		from->delay(destroyMemoryPool, this);
	}
}

MemoryPool::MemoryPool(MemoryPool& from, size_t size [[maybe_unused]]) {
	from.delay(destroyMemoryPool, this);
}

}
}


/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef checkAssert
#define checkAssert(expression) { if (!(expression)) throw bitpowder::lib::Exception(0,  __func__, __FILE__, __LINE__); }
#define checkRetval(expression) { if (expression) throw bitpowder::lib::Exception(errno, __func__, __FILE__, __LINE__); }
#define checkCondition(expression) { if (!(expression)) throw bitpowder::lib::Exception(0,  __func__, __FILE__, __LINE__); }
#endif

#include "lib-common.h"

#include <string>
#include <iostream>

namespace bitpowder {
namespace lib {

class String;
class StringOperation;
class MemoryPool;

// ToString(Exception) is supported

class Exception {
	std::string desc;
	const char* where = nullptr;
	const char* file = nullptr;
	int lineNo = 0;
	int err = 0;
 public:
	Exception() { }
	Exception(int _err);
	Exception(int _err, const char* _where, const char* _sourceFile, const int _lineNo);
	Exception(const char* _desc, int _err = 0, const char* _where = nullptr, const char* _sourceFile = nullptr, const int _lineNo = 0);
	Exception(std::string&& _desc, int _err = 0, const char* _where = nullptr, const char* _sourceFile = nullptr, const int _lineNo = 0);
	Exception(StringOperation&& _desc, int _err = 0, const char* _where = nullptr, const char* _sourceFile = nullptr, const int _lineNo = 0);
	Exception(const Exception& e);

	Exception& operator=(const Exception& e) {
		desc = e.desc;
		err = e.err;
		where = e.where;
		file = e.file;
		lineNo = e.lineNo;
		return *this;
	}
	const char* errorString() const {
		return strerror(err);
	}
	int errorCode() const {
		return err;
	}
	const char* description() const {
		return desc.c_str();
	}
	const char* function() const {
		return where;
	}
	const char* sourceFile() const {
		return file;
	}
	int lineNumber() const {
		return lineNo;
	}
	friend std::ostream& operator <<(std::ostream& out, const Exception& e);
};

std::ostream& operator <<(std::ostream& out, const Exception& e);

}
}

/**
Copyright 2012-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef NCONCURRENCY
#include "spinlock.h"
#endif
#include "stack.h"
#include "queue.h"
#include "exception.h"
#ifndef NDEBUG
#endif
#include <set>

#ifndef WIN32
#ifndef NO_CPP_DEMANGLE
#include <cxxabi.h>
#ifdef __cplusplus
using __cxxabiv1::__cxa_demangle;
#endif
#endif
#endif

#include <cstddef>
#include <type_traits>
#include <limits.h>

#ifdef MEMORY_DEBUG
#define MEMORY_DEBUG_MEMORY_H
#ifdef MEMORY_CACHE
#undef MEMORY_CACHE
#endif
#endif

namespace bitpowder {
namespace lib {

size_t _allocation_size(void*);
template <typename T>
size_t allocation_size(T* t) {
	return _allocation_size(static_cast<void*>(t));
}
void* _malloc_with_minimal_size(size_t& size);
template <typename T = void>
T * malloc_with_minimal_size(size_t& size) {
	return static_cast<T*>(_malloc_with_minimal_size(size));
}
void* _realloc_with_minimal_size(void*, size_t& size);
template <typename T = void>
T * realloc_with_minimal_size(T* ptr, size_t& size) {
	return static_cast<T*>(_realloc_with_minimal_size(ptr, size));
}

template <class RetType, class T>
RetType* GetExtra(T* object) {
	return reinterpret_cast<RetType*>(object + 1);
}

class Memory {
public:
	static size_t Purge();
	static void Stats();
	static String Stats(MemoryPool& mp);
};

// CACHE INSTANCES

struct CachedInstance {
	CachedInstance* next = nullptr;
};

class CacheOfInstancesBase {
public:
	static std::set<CacheOfInstancesBase*>* GetAllInstanceCaches();
	static size_t _PurgeAll();
	static void _StatsAll();
	static String _StatsAll(MemoryPool& mp);
	virtual size_t purge() const = 0;
	virtual std::tuple<uintptr_t, uintptr_t, uintptr_t, char*> stats() const = 0;
	virtual ~CacheOfInstancesBase() { // just to prevent a compiler warning, as all CacheOfInstances are allocated statically, this destructor is never executed (for now).
	}
};

template <class T>
class CacheOfInstances : CacheOfInstancesBase {
public:
#ifndef NCONCURRENCY
	mutable SpinLock instanceCacheLock;
#endif
	mutable Stack<CachedInstance*> instanceCache;
	mutable uintptr_t instanceCount = 0;
	mutable uintptr_t activeCount = 0;
	mutable uintptr_t destructedCount = 0;

	size_t purge() const override {
#ifndef NCONCURRENCY
		auto l = instanceCacheLock.locker();
#endif
		auto local = instanceCache.pop_all();
		instanceCount = 0;
#ifndef NCONCURRENCY
		l.unlock();
#endif
		size_t retval = 0;
		while (!local.empty()) {
			CachedInstance* instance = local.pop_back();
			retval += allocation_size(instance);
			free(instance);
		}
		return retval;
	}
	virtual std::tuple<uintptr_t, uintptr_t, uintptr_t, char*> stats() const override {
#ifndef NO_CPP_DEMANGLE
		int status = 0;
		// __cxa_demangle expect heap data (2nd argument), if too small chunk is passed realloc() will be called
		char* displayName = __cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
		if (!displayName)
			displayName = strdup(typeid(T).name());
#else
		displayName = strdup(typeid(T).name());
#endif
#ifndef NCONCURRENCY
		auto l = instanceCacheLock.locker();
#endif
		return {activeCount, destructedCount, instanceCount, displayName};
	}
	CacheOfInstances() {
		GetAllInstanceCaches()->insert(this);
	}
};

template < class T, size_t SIZE = std::numeric_limits<size_t>::max()>
class CacheInstances {
public:
	static constexpr size_t GetAllocationSize() {
		return (SIZE == std::numeric_limits<size_t>::max() || SIZE < sizeof(T)) ? sizeof(T) : SIZE;
	}
	static constexpr size_t GetSizeOfExtra() {
		return GetAllocationSize() - sizeof(T);
	}
private:
#ifndef MEMORY_DEBUG_MEMORY_H
	static CacheOfInstances<T> InstanceCache;
#ifndef NDEBUG
	static std::set<void*> Instances;
#endif
public:
	~CacheInstances() {
	}
	static size_t Purge() {
		return InstanceCache.purge();
	}

	template <class C>
	static void Reuse(C& objects) {
		unsigned long objectCount = 0;
		Queue<CachedInstance*> convertedObjects;
		while (!objects.empty()) {
			auto pointer = objects.pop();
			destroy(pointer);
			convertedObjects.push(new (pointer) CachedInstance());
			++objectCount;
		}
		Reuse(convertedObjects, objectCount);
	}

	static void Reuse(Queue<CachedInstance*>& convertedObjects, unsigned long objectCount) {
		if (convertedObjects.empty())
			return;

#ifndef NCONCURRENCY
		auto l = InstanceCache.instanceCacheLock.locker();
#endif

#ifndef NDEBUG
		for (auto pointer : convertedObjects)
			Instances.erase(pointer);
#endif

		InstanceCache.instanceCache.push_back(std::move(convertedObjects));
		InstanceCache.instanceCount += objectCount;
		InstanceCache.destructedCount += objectCount;
		InstanceCache.activeCount -= objectCount;
	}

	class Reserved {
		T* pointer = nullptr;
		void clear() {
			if (pointer) {
				T* c = pointer;
#ifndef NCONCURRENCY
				auto l = InstanceCache.instanceCacheLock.locker();
#endif
				InstanceCache.instanceCache.push_back(new (c) CachedInstance());
				++InstanceCache.instanceCount;
				--InstanceCache.activeCount;
#ifndef NDEBUG
				Instances.erase(pointer);
#endif
				pointer = nullptr;
			}
		}

	public:
		Reserved(T* ptr = nullptr) : pointer(ptr) {
		}
		Reserved(Reserved&& r) : pointer(r.pointer) {
			r.pointer = nullptr;
		}
		Reserved& operator=(Reserved&& r) {
			if (this == &r)
				return *this;
			clear();
			pointer = r.pointer;
			r.pointer = nullptr;
			return *this;
		}
		~Reserved() {
			clear();
		}

		void* get() {
			T* retval = pointer;
			pointer = nullptr;
			return retval;
		}

		void reserve() {
			if (pointer)
				return;
#ifndef NCONCURRENCY
			auto l = InstanceCache.instanceCacheLock.locker();
#endif
			pointer = reinterpret_cast<T*>(InstanceCache.instanceCache.pop_back());
			if (pointer)
				--InstanceCache.instanceCount;
			++InstanceCache.activeCount;
#ifndef NCONCURRENCY
			l.unlock();
#endif
			if (!pointer)
				pointer = reinterpret_cast<T*>(malloc(GetAllocationSize()));
#ifndef NDEBUG
#ifndef NCONCURRENCY
			l.lock(InstanceCache.instanceCacheLock);
#endif
			Instances.insert(static_cast<void*>(pointer));
#endif
		}
	};

	static Reserved reserve() {
		Reserved retval;
		retval->reserve();
		return retval;
	}

	static void* operator new(size_t size, Reserved& r) {
		assert(sizeof(T) == size);
		void* retval = r.get();
		return retval ? retval : operator new(size);
	}

	static void* operator new(size_t size) {
		assert(sizeof(T) == size);
		USING(size);
#ifndef NCONCURRENCY
		auto l = InstanceCache.instanceCacheLock.locker();
#endif
		void* retval = InstanceCache.instanceCache.pop_back();
		if (retval)
			--InstanceCache.instanceCount;
		++InstanceCache.activeCount;
#ifndef NCONCURRENCY
		l.unlock();
#endif
		if (!retval)
			retval = malloc(GetAllocationSize());

#ifndef NDEBUG
#ifndef NCONCURRENCY
		l.lock(InstanceCache.instanceCacheLock);
#endif
		Instances.insert(static_cast<void*>(retval));
#endif
		return retval;
	}

	static void operator delete(void* pointer, size_t size) {
		assert(sizeof(T) == size);
		USING(size);

#ifndef NCONCURRENCY
		auto l = InstanceCache.instanceCacheLock.locker();
#endif

		InstanceCache.instanceCache.push_back(new (pointer) CachedInstance());
		++InstanceCache.instanceCount;
		--InstanceCache.activeCount;
		++InstanceCache.destructedCount;

#ifndef NDEBUG
		// to avoid the assert in Stack ensuring the next pointer is always nullptr
		Instances.erase(pointer);
#endif
	}

#ifndef NDEBUG
	static void ShowInstances() {
		fprintf(stderr, "%u instances\n", uint32_t(Instances.size()));
		for (auto i : Instances) {
			fprintf(stderr, "* %p\n", static_cast<void*>(i));
		}
	}
	static void ShowCachedInstancesHelper(CachedInstance* pointer) {
		fprintf(stderr, "* %p\n", static_cast<void*>(pointer));
	}
	static void ShowCachedInstances() {
		std::for_each(InstanceCache.instanceCache.begin(), InstanceCache.instanceCache.end(), ShowCachedInstancesHelper);
	}
#endif
#else
public:

	template <class C>
	static void Reuse(C& objects) {
		while (!objects.empty()) {
			delete objects.pop();
		}
	}
	static void Reuse(Queue<CachedInstance*>& convertedObjects, unsigned long objectCount [[maybe_unused]]) {
		while (!convertedObjects.empty())
			delete convertedObjects.pop();
	}

	static size_t Purge() {
		return 0;
	}
	class Reserved {
	public:
		void reserve() {
		}
	};
	static void* operator new(size_t size [[maybe_unused]]) {
		void* pointer = malloc(GetAllocationSize());
		return pointer;
	}
	static void* operator new(size_t size [[maybe_unused]], Reserved& r [[maybe_unused]]) {
		void* pointer = malloc(GetAllocationSize());
		return pointer;
	}
	static void operator delete(void* pointer, size_t size [[maybe_unused]]) {
		free(pointer);
	}
	static void ShowInstances() {
	}
	static void ShowCachedInstances() {
	}
#endif

	static void* operator new(size_t, void* p) {
		return p;
	}

	static void operator delete(void*, size_t, void*) {
	}

	// only objects of one size are supported (as the operator delete does not know the size information)
	template <typename... Args>
	static void* operator new(size_t nbytes, size_t extra, Args&& ... args) = delete;
};

#ifndef MEMORY_DEBUG_MEMORY_H
template <typename T, size_t SIZE> CacheOfInstances<T> CacheInstances<T, SIZE>::InstanceCache;

#ifndef NDEBUG
template <typename T, size_t SIZE> std::set<void*> CacheInstances<T, SIZE>::Instances;
#endif
#endif

struct AllocCounter {
	size_t bytes = 0;
	size_t count = 0;

	size_t getBytes() const {
		return bytes;
	}
	size_t getCount() const {
		return count;
	}
};

#ifndef MEMORY_CACHE
class MemoryPage {
public:
	const static int SIZE = 16384;

	static void* Get() {
		return malloc(SIZE);
	}
	static void Reuse(void* ptr) {
		free(ptr);
	}
	template <class C>
	static void Reuse(C& objects) {
		while (auto object = objects.pop()) {
			destroy(object);
			free(object);
		}
	}
	static size_t Purge() {
		return 0;
	}
};
#else
class MemoryPage {
public:
	const static int SIZE = 16384;
	MemoryPage* next = nullptr;
	static MemoryPage* BUSY;
	static std::atomic<MemoryPage*> Pages;

	static void* Get() {
		MemoryPage* page;
		while ((page = Pages.exchange(BUSY)) == BUSY);
		MemoryPage* next = page ? page->next : nullptr;
		Pages.exchange(next);
		return page ? page : malloc(SIZE);
	}
	static void Reuse(void* ptr) {
		MemoryPage* page = new (ptr) MemoryPage();
		while ((page->next = Pages.exchange(BUSY)) == BUSY);
		Pages.exchange(page);
	}
	template <class C>
	static void Reuse(C& objects) {
		if (objects.empty())
			return;

		Queue<MemoryPage*> convertedObjects;
		while (!objects.empty()) {
			auto pointer = objects.pop();
			destroy(pointer);
			convertedObjects.push(new (pointer) MemoryPage());
		}

		MemoryPage* page;
		while ((page = Pages.exchange(BUSY)) == BUSY);
		convertedObjects.back()->next = page;
		Pages.exchange(convertedObjects.front());
	}
	static size_t Purge() {
		MemoryPage* page;
		while ((page = Pages.exchange(nullptr)) == BUSY);

		size_t retval = 0;
		while (page) {
			MemoryPage* next = page->next;
			retval += allocation_size(page);
			free(page);
			page = next;
		}
		return retval;
	}
};
#endif

template <class T>
struct AllocateFromMemoryPage {
	const size_t EXTRA_SIZE = lib::MemoryPage::SIZE - sizeof(T);

	static void operator delete(void* pointer, size_t size) {
		assert(size == sizeof(T));
		USING(size);
		MemoryPage::Reuse(pointer);
	}

	static void* operator new(size_t size) {
		assert(size == sizeof(T));
		USING(size);
		return MemoryPage::Get();
	}

	static size_t GetSizeOfExtra() {
		return lib::MemoryPage::SIZE - sizeof(T);
	}
};

template <class T, size_t N>
struct Allocate {
	static void operator delete(void* pointer, size_t size) {
		assert(size == sizeof(T));
		USING(size);
		free(pointer);
	}

	static void* operator new(size_t size) {
		assert(size == sizeof(T));
		USING(size);
		return malloc(N);
	}

	static size_t GetSizeOfExtra() {
		static_assert(N > sizeof(T), "Allocate should be larger then object being allocated");
		return N - sizeof(T);
	}
};

struct Any {};

template <typename A = std::allocator<Any>>
class CountingAllocator {
public:
	AllocCounter* counter = nullptr;
	typedef typename A::size_type size_type;
	typedef typename A::difference_type difference_type;
	typedef typename A::pointer pointer;
	typedef typename A::const_pointer const_pointer;
	typedef typename A::reference reference;
	typedef typename A::const_reference const_reference;
	typedef typename A::value_type value_type;

	template <class U>
	struct rebind {
		typedef CountingAllocator<U> other;
	};

	A a;
	CountingAllocator() {
	}
	CountingAllocator(AllocCounter* allocCounter) : counter(allocCounter) {
	}
	CountingAllocator(AllocCounter* allocCounter, const A& _a) : counter(allocCounter), a(_a) {
	}

	CountingAllocator(const CountingAllocator& copy) : counter(copy.counter), a(copy.a) {
	}
	template <class U>
	inline CountingAllocator(const CountingAllocator<U>& copy) : counter(copy.counter), a(copy.a) {
	}

	inline pointer address(reference x) const {
		return a.address(x);
	}
	inline const_pointer address(const_reference x) const {
		return a.address(x);
	}
	inline pointer allocate(size_type n) {
		size_t bytes = sizeof(value_type) * n;
		void* retval = a.allocate(n);
		if (counter) {
			counter->bytes += bytes;
			++counter->count;
		}
		return static_cast<pointer>(retval);
	}

	inline void deallocate(pointer p, size_type n) {
		a.deallocate(p, n);
		size_t bytes = sizeof(value_type) * n;
		if (counter) {
			counter->bytes -= bytes;
			--counter->count;
		}
	}
	inline void deallocate(void* p, size_t bytes) {
		a.deallocate(p, bytes);
		if (counter) {
			counter->bytes -= bytes;
			--counter->count;
		}
	}
	size_type max_size() const {
		return a.max_size();
	}
	void construct(pointer p,  const_reference val) {
		a.construct(p, val);
	}
	template<class U, typename... Args>
	void construct(U* p,  Args&& ... args) {
		a.construct(p, std::forward<Args>(args)...);
	}
	void construct(pointer p) {
		a.construct(p);
	}
	void destroy(pointer p) {
		a.destroy(p);
	}
	bool operator==(const CountingAllocator& rhs) const {
		return this == &rhs;
	}
	bool operator!=(const CountingAllocator& rhs) const {
		return this == &rhs;
	}
};

struct CachingAny {
	// should be minimal size of one pointer (for internal workings of CachingAllocator)
	char _padding[__WORDSIZE / 8];
};

#ifndef MEMORY_DEBUG_MEMORY_H
template <typename T = CachingAny>
class CachingAllocator {
public:
	const static size_t N = (lib::MemoryPage::SIZE - sizeof(void*)) / sizeof(T);
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T value_type;

	template <class U>
	struct rebind {
		typedef CachingAllocator<U> other;
	};
protected:
	struct value_proxy {
		char placeholder[sizeof(value_type)];
	};
	struct Chunk {
		value_proxy objects[std::max(size_t(1), N)];
		Chunk* next = nullptr;
		static_assert(sizeof(CachedInstance) <= sizeof(value_type), "internal requirement");
	};

public:
	Stack<CachedInstance*> cache;
	Stack<Chunk*> freeList;

	CachingAllocator() {
	}
	~CachingAllocator() {
		cache.clear(); // first clear linked list, then delete memory
		while (Chunk* instance = freeList.pop_back())
			delete instance;
	}
	CachingAllocator(const CachingAllocator&) {
	}
	CachingAllocator(CachingAllocator&& copy) : cache(std::move(copy.cache)), freeList(std::move(copy.freeList)) {
	}
	template <class U>
	inline CachingAllocator(const CachingAllocator<U>&) {
	}
	inline pointer address(reference x) const {
		return &x;
	}
	inline const_pointer address(const_reference x) const {
		return &x;
	}
	inline pointer allocate(size_type n) {
		if (n != 1) {
			size_t bytes = sizeof(value_type) * n;
			return static_cast<pointer>(malloc(bytes));
		}
		void* retval = cache.pop_back();
		if (!retval) {
			Chunk* chunk = new Chunk();
			freeList.push_back(chunk);
			// save unused objects
			for (unsigned int i = N - 1; i-- > 0; )
				cache.push_back(new (&chunk->objects[i]) CachedInstance());
			// return last object
			retval = static_cast<void*>(&chunk->objects[N - 1]);
		}
		return static_cast<pointer>(retval);
	}

	inline void deallocate(pointer p, size_type n) {
		deallocate(static_cast<void*>(p), sizeof(value_type)*n);
	}
	inline void deallocate(void* p, size_t bytes) {
		if (bytes == sizeof(value_type))
			cache.push_back(new (p) CachedInstance());
		else
			free(p);
	}
	size_type max_size() const {
		return std::numeric_limits<size_type>::max();
	}
	template <typename... Args>
	void construct(pointer p,  Args... args) {
		new(static_cast<void*>(p)) T(std::forward<Args>(args)...);
	}
	template <class U, typename... Args>
	void construct(U* p,  Args... args) {
		new(static_cast<void*>(p)) U(std::forward<Args>(args)...);
	}
	void construct(pointer p) {
		new(static_cast<void*>(p)) T();
	}
	void destroy(pointer p) {
		p->T::~T();
	}
	bool operator==(const CachingAllocator& rhs) const {
		// FIXME: should support this, as it will probably allow faster transfers
		return this == &rhs;
	}
	bool operator!=(const CachingAllocator& rhs) const {
		// FIXME: should support this, as it will probably allow faster transfers
		return this != &rhs;
	}
};
#else
template <typename T = CachingAny>
class CachingAllocator {
public:
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T value_type;

	template <class U>
	struct rebind {
		typedef CachingAllocator<U> other;
	};
	CachingAllocator() {
	}
	~CachingAllocator() {
	}
	CachingAllocator(const CachingAllocator&) {
	}
	CachingAllocator(CachingAllocator&& copy [[maybe_unused]]) {
	}
	template <class U>
	inline CachingAllocator(const CachingAllocator<U>&) {
	}
	inline pointer address(reference x) const {
		return &x;
	}
	inline const_pointer address(const_reference x) const {
		return &x;
	}
	inline pointer allocate(size_type n) {
		size_t bytes = sizeof(value_type) * n;
		return static_cast<pointer>(malloc(bytes));
	}
	inline void deallocate(pointer p, size_type n) {
		deallocate(static_cast<void*>(p), sizeof(value_type)*n);
	}
	inline void deallocate(void* p, size_t bytes [[maybe_unused]]) {
		free(p);
	}
	size_type max_size() const {
		return std::numeric_limits<size_type>::max();
	}
	void construct(pointer p,  const_reference val) {
		new(static_cast<void*>(p)) T(val);
	}
	template<class U, typename... Args>
	void construct(U* p,  Args&& ... args) {
		new(static_cast<void*>(p)) U(std::forward<Args>(args)...);
	}
	void construct(pointer p) {
		new(static_cast<void*>(p)) T();
	}
	void destroy(pointer p) {
		p->T::~T();
	}
	bool operator==(const CachingAllocator& rhs) const {
		// FIXME: should support this, as it will probably allow faster transfers
		return this == &rhs;
	}
	bool operator!=(const CachingAllocator& rhs) const {
		// FIXME: should support this, as it will probably allow faster transfers
		return this != &rhs;
	}
};
#endif

}
}


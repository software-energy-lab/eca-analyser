/**
Copyright 2014-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "exception.h"
#include <assert.h>
#include <atomic>
#include <iostream>

/**
  * shared_object is a smart pointer using storage of the object pointed to, so it has minimal overhead.
  *
  * std::shared_ptr and shared_object are functional more or less equivalent. std::shared_ptr uses
  * two pointers: one to the control block and one to the data that gets pointed to. This makes all
  * the pointers double in size compared to shared_object.
  *
  * The aliasing std::shared_ptr functionality can be achieved with shared_object by using the
  * count_proxy facility. This adds an extra pointer field to the object that is referenced (but
  * shared_objects are half the size of std::shared_ptr).
  *
  * Because shared_objects use embedded reference counters, objects can be of dynamic size without
  * requiring additional allocations (std::shared_ptr can do the same, although the control block
  * needs to be allocated separatly).
  *
  * Depending on the needs, shared_object lets you choose the type of the reference counter. This
  * can be a concurrent refernece counter or just a plain 'int'.
  *
  * The default manner of std::shared_ptr to use std::make_shared to allocate at once can not be used
  * with CacheInstances, as only instances are cached, not control blocks. It also does not work with
  * the 'reserve' behaviour of CacheInstances.
  *
  * A major drawback of shared_object is that its use has the be properly envisioned during design
  * of the classes. It can not be added afterwards like std::shared_ptr.
  */

//#define ZOMBIE 0x4242

namespace bitpowder {
namespace lib {

template <class Int>
class concurrent_counter {
	std::atomic<Int> refcount = {0};
public:
	typedef Int ValueType;
	concurrent_counter(Int _refcount = 0) : refcount(_refcount) {
	}
	concurrent_counter(const concurrent_counter&) {
	}
	~concurrent_counter() {
		assert(refcount == 0);
	}

	// prefix
	Int operator++() {
		auto retval = refcount.fetch_add(1, std::memory_order_relaxed);
		assert(retval < std::numeric_limits<Int>::max());
		return retval + 1;
	}

	// postfix
	Int operator++(int) {
		auto retval = refcount.fetch_add(1, std::memory_order_relaxed);
		assert(retval < std::numeric_limits<Int>::max());
		return retval;
	}

	Int operator+=(Int value) {
		auto retval = refcount.fetch_add(value, std::memory_order_relaxed);
		assert(retval <= std::numeric_limits<Int>::max() - value);
		return retval + value;
	}

	// prefix
	Int operator--() {
		auto retval = refcount.fetch_sub(1, std::memory_order_relaxed);
		assert(retval > 0);
		return retval - 1;
	}

	// postfix
	Int operator--(int) {
		auto retval = refcount.fetch_sub(1, std::memory_order_relaxed);
		assert(retval > 0);
		return retval;
	}

	Int operator-=(Int value) {
		auto retval = refcount.fetch_sub(value, std::memory_order_relaxed);
		assert(retval >= value);
		return retval;
	}

	operator Int() const {
		return refcount;
	}

	Int get() const {
		return refcount;
	}

	// helper for thread sanitizer
	auto memory() {
		return &refcount;
	}

#ifdef ZOMBIE
	concurrent_counter& operator=(Int value) {
		refcount = value;
		return *this;
	}
#endif
};

using concurrent_count = concurrent_counter<uint32_t>;

template <class Action>
class ref_count_with_action {
	std::atomic<int> refcount = {0};
	Action action;
public:
	ref_count_with_action(Action&& act) : action(std::forward<Action>(act)) {
	}

	void operator++(int) {
		refcount++;
	}

	int operator--(int) {
		int count = refcount--;
		if (count == 1)
			action();
		return count;
	}

	operator int() {
		return refcount;
	}
};

template <class Type>
class count_proxy {
	Type* refcount = nullptr;
public:
	count_proxy(Type& _refcount) : refcount(&_refcount) {
	}
	count_proxy() {
	}
	count_proxy(const count_proxy& proxy) : refcount(proxy.refcount) {
	}
	typename Type::ValueType operator++(int) {
		return refcount ? typename Type::ValueType((*refcount)++) : 0;
	}

	typename Type::ValueType operator--(int) {
		return refcount ? typename Type::ValueType((*refcount)--) : 0;
	}

	typename Type::ValueType operator++() {
		return refcount ? typename Type::ValueType(++(*refcount)) : 0;
	}

	typename Type::ValueType operator--() {
		return refcount ? typename Type::ValueType(--(*refcount)) : 0;
	}

	operator typename Type::ValueType() {
		return refcount ? typename Type::ValueType(*refcount) : 0;
	}

	// helper for thread sanitizer
	auto memory() {
		return refcount->memory();
	}

#ifdef ZOMBIE
	count_proxy& operator=(typename Type::ValueType value) {
		*refcount = value;
		return *this;
	}
#endif
};

template <class Object, class Base = Object, class RefCountType = decltype(Base::refcount), RefCountType Base::*ptr = &Base::refcount, void (*deleter)(Object*) = &performDelete<Object>>
class shared_object {
	Object* object = nullptr;
	template <class OtherObject, class OtherBase, class OtherRefCountType, OtherRefCountType OtherBase::*otherPtr, void (*otherDeleter)(OtherObject*)>
	friend class shared_object;
	static RefCountType& GetRefCounter(Object* object) {
		RefCountType* retval = const_cast<RefCountType*>(&(static_cast<Base*>(object)->*ptr));
		assert(retval);
		return *retval;
	}
public:
	typedef Object object_type;
	static void IncreaseRefCounter(Object* object) {
		if (object) {
#ifdef ZOMBIE
			auto result = ++GetRefCounter(object);
			assert(result < ZOMBIE);
			USING(result);
#else
			++GetRefCounter(object);
#endif
		}
	}
	static void DecreaseRefCounter(Object* object) {
		if (!object)
			return;
		// explanation of the annotiations: http://valgrind.org/docs/manual/hg-manual.html
		if (GetRefCounter(object)-- == 1) {
			TSAN_ANNOTATE_HAPPENS_AFTER(GetRefCounter(object).memory());
#ifdef ZOMBIE
			// do not deallocate, put a placemaker in the counter field
			destroy(object);
			GetRefCounter(object) = ZOMBIE;
#else
			deleter(object);
#endif
		} else {
			TSAN_ANNOTATE_HAPPENS_BEFORE(GetRefCounter(object).memory());
		}
	}
	shared_object() {
	}
	shared_object(std::nullptr_t) {
	}
	shared_object(Object* _object) : object(_object) {
		IncreaseRefCounter(object);
	}
	~shared_object() {
		DecreaseRefCounter(object);
	}
	template <class OtherObject, class OtherBase, class OtherRefCountType, OtherRefCountType OtherBase::*otherPtr = &OtherBase::refcount, void (*otherDeleter)(OtherObject*) = &performDelete<OtherObject>>
	shared_object(const shared_object<OtherObject, OtherBase, OtherRefCountType, otherPtr, otherDeleter>& o) : object(static_cast<Object*>(o.get())) {
		IncreaseRefCounter(object);
	}
	shared_object(const shared_object& o) : object(o.object) {
		IncreaseRefCounter(object);
	}
	template <class OtherObject, class OtherBase, class OtherRefCountType, OtherRefCountType OtherBase::*otherPtr = &OtherBase::refcount, void (*otherDeleter)(OtherObject*) = &performDelete<OtherObject>>
	shared_object(shared_object<OtherObject, OtherBase, OtherRefCountType, otherPtr, otherDeleter>&& o) : object(static_cast<Object*>(o.get())) {
		o.object = nullptr;
	}
	shared_object(shared_object&& o) : object(o.object) {
		o.object = nullptr;
	}

	shared_object& operator=(shared_object&& o) {
		if (this == &o)
			return *this;
		DecreaseRefCounter(object);
		object = o.object;
		o.object = nullptr;
		return *this;
	}
	shared_object& operator=(const shared_object& o) {
		if (this == &o)
			return *this;
		DecreaseRefCounter(object);
		object = o.object;
		IncreaseRefCounter(object);
		return *this;
	}
	shared_object& operator=(const std::nullptr_t&) {
		DecreaseRefCounter(object);
		object = nullptr;
		return *this;
	}
	inline operator Object* () const {
#ifdef ZOMBIE
		assert(!object || GetRefCounter(object) < ZOMBIE);
#endif
		return object;
	}
	inline Object& operator*() const {
#ifdef ZOMBIE
		assert(GetRefCounter(object) < ZOMBIE);
#endif
		return *object;
	}
	inline Object* operator->() const {
#ifdef ZOMBIE
		assert(GetRefCounter(object) < ZOMBIE);
#endif
		return object;
	}
	explicit inline operator bool() const {
		return object != nullptr;
	}
	inline bool operator==(const shared_object& o) const {
		return object == o.object;
	}
	inline bool operator==(const std::nullptr_t&) const {
		return object == nullptr;
	}
	inline bool operator!=(const shared_object& o) const {
		return !(*this == o);
	}
	inline bool operator!=(const std::nullptr_t&) const {
		return object != nullptr;
	}
	inline bool empty() const {
		return object == nullptr;
	}
	inline Object* get() const {
		return object;
	}
	inline void swap(shared_object& rhs) {
		std::swap(object, rhs.object);
	}
};

#define REFERENCE_COUNTED_INTERFACE(T) \
    bitpowder::lib::count_proxy<bitpowder::lib::concurrent_count> refcount; \
    using Ref = bitpowder::lib::shared_object<T>; \
    T(bitpowder::lib::concurrent_count& _refcount) : refcount(_refcount) { \
    } \
    T(const bitpowder::lib::count_proxy<bitpowder::lib::concurrent_count>& _refcount) : refcount(_refcount) { \
      assert(&refcount != &_refcount); \
    } \
    virtual ~T() { \
    }

class shared_object_owner_less {
public:
	template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
	bool operator()(const shared_object<Object, Base, RefCountType, ptr, deleter>& lhs, const shared_object<Object, Base, RefCountType, ptr, deleter>& rhs) const {
		if (!bool(lhs) || !bool(rhs))
			return bool(rhs);
		return *lhs < *rhs;
	}

	template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*), class T>
	bool operator()(const shared_object<Object, Base, RefCountType, ptr, deleter>& lhs, const T& rhs) const {
		return !bool(lhs) || T(*lhs) < rhs;
	}

	template <class T, class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
	bool operator()(const T& lhs, const shared_object<Object, Base, RefCountType, ptr, deleter>& rhs) const {
		return !bool(rhs) || lhs < T(*rhs);
	}
	typedef void is_transparent;
};

}
}

namespace std {

template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
void swap(bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>& lhs, bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>& rhs) {
	lhs.swap(rhs);
}

template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
class remove_pointer<bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>> {
public:
	typedef Object type;
};

template <class Object, class Base, class RefCountType, RefCountType Base::*ptr, void (*deleter)(Object*)>
struct owner_less<bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>> {
	bool operator()(const bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>& lhs, const bitpowder::lib::shared_object<Object, Base, RefCountType, ptr, deleter>& rhs) const {
		if (!bool(lhs) || !bool(rhs))
			return bool(rhs);
		return *lhs < *rhs;
	}
};

}


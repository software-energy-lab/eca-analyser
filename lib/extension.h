/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

//#define EXTENSION_USING_CPP_TYPE_INDEX

#include "stack.h"

#ifdef EXTENSION_USING_CPP_TYPE_INDEX
#include <typeindex>
#else
#include "type_hash.h"
#endif

namespace bitpowder {
namespace lib {

template <class E, typename... CopyArgs>
class Extension {
public:
#ifdef EXTENSION_USING_CPP_TYPE_INDEX
	std::type_index type;
#else
	FastTypeT type;
#endif
	E next = nullptr;
#ifdef EXTENSION_USING_CPP_TYPE_INDEX
	Extension() : type(typeid(E)) {}
#else
	Extension() : type(FastTypeEmpty) {}
#endif
	// at least one virtual method to make dynamic_cast work
	virtual ~Extension() {}

	// default is no copy, override in derived class if deep or shallow copy is needed
	// probably with return new DerivedClass(*this);
	// if an extension should not be copied, return nullptr
	virtual E copy(CopyArgs&& ...) {
		return nullptr;
	}
};

template <class E, typename... CopyArgs>
class ExtensionContainer {
public:
	typedef E Element;
	typedef Stack<Element, Extension<Element, CopyArgs...>> ExtensionStack;
private:
	struct NotNull {
		template <class T>
		bool operator()(T* t) {
			return t;
		}
	};
	template <class T>
	struct Caster {
#ifdef EXTENSION_USING_CPP_TYPE_INDEX
		std::type_index targetType = typeid(std::remove_pointer<T>::type);
#endif
		T operator()(Extension<E, CopyArgs...>* e) {
#ifdef EXTENSION_USING_CPP_TYPE_INDEX
			if (e->type == targetType)
#else
			if (FastType<typename std::remove_pointer<T>::type>::PointerOfType(e))
#endif
				return static_cast<T>(e);
			return nullptr;
		}
	};
	template <class T>
	struct CasterDynamic {
		T operator()(Extension<E, CopyArgs...>* e) {
			return dynamic_cast<T>(e);
		}
	};
protected:
	ExtensionStack extensions;
public:
	ExtensionContainer() {
	}
	// default behaviour is no copy of extensions (safest choice, so no memory leaks occur)
	// deep copy needs support from all the derived extensions, because the type information is not know by the container)
	// shallow copy can be supported by overriding copy in BaseExtension (which derives from e.g. Extension<BaseExtension*>) with "return this";
	// this copy constructor will only work if no CopyArgs are defined
	ExtensionContainer(const ExtensionContainer& c) {
		// otherwise wrong order will be used, can matter
		ExtensionStack toBeAdded;
		for (Element ext : c.extensions) {
			Element copy = ext->copy();
			if (copy)
				toBeAdded.push(copy);
		}
		while (!toBeAdded.empty())
			extensions.push(toBeAdded.pop());
	}

	ExtensionContainer(ExtensionContainer&& c) = default;
	ExtensionContainer& operator=(ExtensionContainer&& c) = default;

	// default behaviour is no copy of extensions (safest choice, so no memory leaks occur)
	// deep copy needs support from all the derived extensions, because the type information is not know by the container)
	// shallow copy can be supported by overriding copy in BaseExtension (which derives from e.g. Extension<BaseExtension*>) with "return this";
	ExtensionContainer copy(CopyArgs&& ... args) {
		// otherwise wrong order will be used, can matter
		ExtensionContainer retval;
		ExtensionStack toBeAdded;
		for (Element ext : extensions) {
			Element copy = ext->copy(std::forward<CopyArgs...>(args...));
			if (copy)
				toBeAdded.push(copy);
		}
		while (!toBeAdded.empty())
			retval.extensions.push(toBeAdded.pop());
		return retval;
	}

	ExtensionStack& getAllExtensions() {
		return extensions;
	}

	template <class T>
	T getExtension() const {
		auto tExtensions = selectExtensions<T>();
		return tExtensions.begin() != tExtensions.end() ? *tExtensions.begin() : nullptr;
	}

	template <class T>
	T getExtensionOfBaseType() const {
		auto tExtensions = selectExtensionsOfBaseType<T>();
		return tExtensions.begin() != tExtensions.end() ? *tExtensions.begin() : nullptr;
	}

	// needed to support erase
	template <class T>
	auto selectExtensions() {
		return apply_filter_on(apply_map_on(extensions, Caster<T>()), NotNull());
	}

	template <class T>
	auto selectExtensions() const {
		return apply_filter_on(apply_map_on(extensions, Caster<T>()), NotNull());
	}

	// needed to support erase
	template <class T>
	auto selectExtensionsOfBaseType() {
		return apply_filter_on(apply_map_on(extensions, CasterDynamic<T>()), NotNull());
	}

	template <class T>
	auto selectExtensionsOfBaseType() const {
		return apply_filter_on(apply_map_on(extensions, CasterDynamic<T>()), NotNull());
	}

	template <class T>
	void addExtension(const T& extension) {
#ifdef EXTENSION_USING_CPP_TYPE_INDEX
		extension->Extension<E>::type = std::type_index(typeid(typename std::remove_pointer<T>::type));
#else
		extension->Extension<E>::type = FastType<typename std::remove_pointer<T>::type>::Value;
#endif
		extensions.push_back(extension);
	}

	template <class T>
	T removeExtension() {
		for (auto it : selectExtensions<T>())
			return it.erase();
		return nullptr;
	}

	template <class T>
	T removeExtensionOfBaseType() {
		for (auto it : selectExtensionsOfBaseType<T>())
			return it.erase();
		return nullptr;
	}

	ExtensionStack clearExtensions() {
		return extensions.pop_all();
	}
};

}
}


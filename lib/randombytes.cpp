/**
Copyright 2014-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "exception.h"
#include <random>
#include "randombytes.h"
#include "assert.h"

// this interface needed for contrib/tweenacl.cpp
void randombytes(unsigned char* ptr, unsigned long length) {
	static_assert(std::is_same<std::random_device::result_type, unsigned int>::value, "assumption of code below");
	static std::random_device rd("/dev/urandom");
	if ((uintptr_t(ptr) & 0x03) && length > 3) {
		unsigned int value = rd();
		switch (uintptr_t(ptr) & 0x03) {
		case 0x01:
			*(ptr++) = value & 0xFF;
			--length;
			value >>= 8;
			FALLTHROUGH;
		case 0x02:
			*(ptr++) = value & 0xFF;
			--length;
			value >>= 8;
			FALLTHROUGH;
		case 0x03:
			*(ptr++) = value & 0xFF;
			--length;
		}
	}
	assert((uintptr_t(ptr) & 0x03) == 0);
	unsigned int* intPtr = reinterpret_cast<unsigned int*>(ptr);
	for (unsigned long i = length / 4; i-- > 0 ;) {
		unsigned int value = rd();
		intPtr[i] = value;
	}
	if (length & 3) {
		unsigned int value = rd();
		for (unsigned long i = (length & (~0U << 2)); i < length; ++i) {
			ptr[i] = value & 0xFF;
			value >>= 8;
		}
	}
}

void bitpowder::crypto::RandomBytes(void* ptr, std::size_t length) {
	::randombytes(static_cast<unsigned char*>(ptr), length);
}

#ifdef MINI_DEPENDENCIES
extern "C" {
	void randombytes(unsigned char* ptr, unsigned long long length) {
		bitpowder::crypto::RandomBytes(ptr, std::size_t(length));
	}
}
#endif

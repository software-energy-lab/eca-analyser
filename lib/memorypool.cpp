/**
Copyright 2010-2019 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef MEMORY_DEBUG
#include "memorypool.debug.cpp"
#else

#include "memorypool.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#ifndef NCONCURRENCY
#include "spinlock.h"
#endif

#if defined(_WIN32)
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
#endif

#ifndef NDEBUG
#define EXTRA_SAFE
#endif

/*
 * TODO:
 * - reserve(int chunks, int maxSizeOfItem): make users of reserve specify more clearly what they need
 * - make MemoryPage::Get() work with thread local storage
 * - one drawback is large allocations can leave a lot of smaller MemoryPoolPages on the stack unused permantently.
 *   A solution is to have two heads of the 'all' queue:
 *   - one for smaller allocations (from MemoryPage::Get())
 *   - one for large allocations
 *   During rewinding both queues can be reconstructed by traversing 'all' and checking if it is a large or small allocation.
 *   MemoryPoolStatus should remember one additional pointer (for the large allocation queue).
 *   A big advantage of this scheme is that reserve() can be made before a loop and this reservation reused over multiple restore() operations.
 *   A condition for efficiency: if there is no small page available, use a large page for smaller allocations (to avoid memory allocations)
 * - split restore() into two operations: rewind() and clear().
 *   - rewind(status) frees the memory to a state as before, but keeps the pages so subsequent are faster because no memory allocations need to be done (useful in a loop, or during a time critical code path, e.g. request handling in a webserver)
 *   - clear(status) as rewind(status) but newly allocated pages are really released from this pool (useful on the end of a loop or time critical path)
 */

/**
 * Two important situations that need to be taken into account when modifying the implementation of MemoryPool:
 * - when restoring/clearing a MemoryPool containing a STL container with custom allocator: the memory freed/deallocted
 * by the STL container must not be re-added to the MemoryPool, as the MemoryPage containing this memory is going to be
 * deallocted
 * - if new memory is allocated (especially memory that is allocated out of band of the memory pool system), needs to
 * be deallocted after all the delayed objects are executed, as a STL container can access this memory during its
 * destructor phase.
 */

namespace bitpowder {
namespace lib {

#define OverheadOfMemoryPoolPage 0

MemoryPoolPage::MemoryPoolPage(size_t totalSize, bool _fixedSize, bool _deallocate) : size(totalSize - (sizeof(MemoryPoolPage) - OverheadOfMemoryPoolPage)), available(size), fromPool(_fixedSize), deallocate(_deallocate) {
}

inline void MemoryPoolPage::reset() {
	assert(!fromPool || size == MemoryPoolPage::GetDefaultSize());
	assert(available <= size);
#ifdef EXTRA_SAFE
	bzero(GetExtra<void>(this), size);
#endif
	available = size;
}

size_t MemoryPoolPage::GetDefaultSize() {
	return MemoryPage::SIZE - (sizeof(MemoryPoolPage) - OverheadOfMemoryPoolPage);
}

void MemoryPool::_clear(const MemoryPoolStatus& status) {
	rewinding = true;

	while (delayObjects.back() != status.delayObjects) {
		auto object = delayObjects.pop_back();
		object->execute();
	}

	while (all.back() && all.back() != status.current) {
		MemoryPoolPage* current = all.pop_back();
		assert(!current->next);
		assert(!current->prev);
		current->reset();
		if (current->size > MemoryPage::SIZE - sizeof(MemoryPoolPage)) {
			largePages.push_front(current);
		} else {
			smallPages.push_front(current);
		}
	}

	assert(all.back() == status.current);

	// reset available to previous value
	if (all.back()) {
		assert(all.back()->available <= status.available);
#ifdef EXTRA_SAFE
		bzero(GetExtra<char>(all.back()) + all.back()->size - status.available, status.available - all.back()->available);
#endif
		all.back()->available = status.available;
	}

	// dispose of pages (not earlier, because a delay function of an 'earlier' memorypool can depend on memory of a 'later' memorypool...)
	Queue<MemoryPoolPage*, MemoryPoolPage, &MemoryPoolPage::prev> reuse;
	Queue<MemoryPoolPage*, MemoryPoolPage, &MemoryPoolPage::prev> deallocate;

	// rewind until status.nextPage (to avoid removing pages that were added with addMemory)
	// do not remove pages further as status.nextPage, they could be referenced by another MemoryPoolStatus
	while (smallPages.back() && smallPages.back() != status.nextSmallPage) {
		assert(smallPages.back() != status.current);
		MemoryPoolPage* p = smallPages.pop_back();
		if (p->deallocate) {
			deallocate.push(p);
		} else if (p->fromPool) {
			reuse.push(p);
		}
	}
	assert(smallPages.back() == status.nextSmallPage);
	while (largePages.back() && largePages.back() != status.nextLargePage) {
		assert(largePages.back() != status.current);
		MemoryPoolPage* p = largePages.pop_back();
		if (p->deallocate) {
			deallocate.push(p);
		} else if (p->fromPool) {
			reuse.push(p);
		}
	}
	assert(largePages.back() == status.nextLargePage);

	// this should be done fairly late, as here memory gets deallocated, and in that memory MemoryPoolPages can exists (that are in the linked lists that are traversed above)
	while (auto object = deallocate.pop())
		free(object);

	// add all the memorypools to be recycled in one operation to the pool
	MemoryPage::Reuse(reuse);

	rewinding = false;
}

void MemoryPool::_rewind(const MemoryPoolStatus& status) {
	rewinding = true;

	while (delayObjects.back() != status.delayObjects) {
		auto object = delayObjects.pop_back();
		object->execute();
	}

	while (all.back() && all.back() != status.current) {
		MemoryPoolPage* current = all.pop_back();
		assert(!current->next);
		assert(!current->prev);
		current->reset();
		if (current->size > MemoryPage::SIZE - sizeof(MemoryPoolPage)) {
			largePages.push_front(current);
		} else {
			smallPages.push_front(current);
		}
	}
	assert(all.back() == status.current);

	// reset available to previous value
	if (all.back()) {
		assert(all.back()->available <= status.available);
#ifdef EXTRA_SAFE
		bzero(GetExtra<char>(all.back()) + all.back()->size - status.available, status.available - all.back()->available);
#endif
		all.back()->available = status.available;
	}

	// until the status.nextPage we have to remove all 'addMemory' (because otherwise it gets added twice)
	MemoryPoolPage* c = smallPages.back();
	while (c && c != status.nextSmallPage) {
		if (c->fromPool || c->deallocate) {
			c = smallPages.prev(c);
		} else {
			MemoryPoolPage* p = smallPages.prev(c);
			smallPages.erase(c);
			c = p;
		}
	}
	assert(c == status.nextSmallPage);

	c = largePages.back();
	while (c && c != status.nextLargePage) {
		if (c->fromPool || c->deallocate) {
			c = largePages.prev(c);
		} else {
			MemoryPoolPage* p = largePages.prev(c);
			largePages.erase(c);
			c = p;
		}
	}
	assert(c == status.nextLargePage);

#ifdef EXTRA_SAFE
	if (all.back()) {
		assert(all.back()->available <= all.back()->size);
		bzero(GetExtra<char>(all.back()) + all.back()->size - all.back()->available, all.back()->available);
	}
#endif

	rewinding = false;
}

MemoryPool::~MemoryPool() {
	if (empty()) {
		assert(delayObjects.empty());
		return;
	}
	rewinding = true;

	// this should be done fairly late, as here memory gets deallocated, and in that memory MemoryPoolPages can exists (that are in the linked lists that are traversed above)
	while (auto object = delayObjects.pop_back())
		object->execute();

	// dispose of pages (not earlier, because a delay function of an 'earlier' memorypool can depend on memory of a 'later' memorypool...)
	Queue<MemoryPoolPage*, MemoryPoolPage, &MemoryPoolPage::prev> reuse;
	Queue<MemoryPoolPage*, MemoryPoolPage, &MemoryPoolPage::prev> deallocate;

	// rewind nextPages, as these are never allocated from the global pool, we don't have to readded them
	while (MemoryPoolPage* p = largePages.pop_back()) {
		if (p->deallocate) {
			deallocate.push(p);
		} else if (p->fromPool) {
			reuse.push(p);
		}
	}

	while (MemoryPoolPage* p = smallPages.pop_back()) {
		if (p->deallocate) {
			deallocate.push(p);
		} else if (p->fromPool) {
			reuse.push(p);
		}
	}

	while (MemoryPoolPage* p = all.pop_back()) {
		if (p->deallocate) {
			deallocate.push(p);
		} else if (p->fromPool) {
			reuse.push(p);
		}
	}

	// this should be done fairly late, as here memory gets deallocated, and in that memory MemoryPoolPages can exists (that are in the linked lists that are traversed above)
	while (auto object = deallocate.pop())
		free(object);

	// add all the memorypools to be recycled in one operation to the pool
	MemoryPage::Reuse(reuse);
}

MemoryPoolPage* MemoryPool::_addMemory(void* data, size_t size) {
#ifdef EXTRA_SAFE
	bzero(data, size);
#endif

	// make multiple of alignment, so MemoryPoolPage start at a correct alignment
	size_t extra = uintptr_t(data) & (alignof(MemoryPoolPage) - 1);
	if (extra) {
		size_t neededPadding = alignof(MemoryPoolPage) - extra;
		if (neededPadding > size) {
			data = nullptr;
			size = 0;
		} else {
			data = static_cast<char*>(data) + neededPadding;
			size -= neededPadding;
		}
	}
	if (!data || size < sizeof(MemoryPoolPage) + MINIMAL_USEFUL_SIZE) // || size > USHRT_MAX /* length of short */)
		return nullptr;

	MemoryPoolPage* p = new (data) MemoryPoolPage(size, false, false);
	return p;
}

bool MemoryPool::addMemory(void* data, size_t _size) {
	if (!data || rewinding)
		return false;

	MemoryPoolPage* p = _addMemory(data, _size);
	if (!p)
		return false;
	(_size > MemoryPage::SIZE - sizeof(MemoryPoolPage) ? largePages : smallPages).push_back(p);
	return true;
}

void MemoryPool::addMemory(MemoryPool* mp) {
	if (mp) {
		size_t size = 0;
		void* data = mp->allocAll(size);
		if (data && size > 0)
			addMemory(data, size);
	}
}

// allocs new pool
MemoryPoolPage* MemoryPool::allocExtra(size_t size) {
	//fprintf(stderr, "allocExtra: size=%li; large=%i/%u; small=%i/%u\n", size, !largePages.empty(), largePages.empty() ? 0 : largePages.front()->available, !smallPages.empty(), smallPages.empty() ? 0 : smallPages.front()->available);
	if (size > MemoryPage::SIZE - sizeof(MemoryPoolPage)) {
		if (!largePages.empty()) {
			MemoryPoolPage* next = largePages.pop_front();
			assert(!next->next);
			assert(!next->prev);
			all.push_back(next);
			assert(all.back() == next);
			return next;
		}
		size_t bytes = size + sizeof(MemoryPoolPage);
		char* data = malloc_with_minimal_size<char>(bytes);
		assert(data);
		MemoryPoolPage* page = new (data) MemoryPoolPage(bytes, false, true);
		all.push_back(page);
		return page;
	}
	if (!smallPages.empty()) {
		MemoryPoolPage* next = smallPages.pop_front();
		assert(!next->next);
		assert(!next->prev);
		all.push_back(next);
		assert(all.back() == next);
		return next;
	}

	MemoryPoolPage* page = new (MemoryPage::Get()) MemoryPoolPage(MemoryPage::SIZE, true, false);

	// really add the memory to the pools
	assert(page->available == page->size);
	assert(page->size == MemoryPoolPage::GetDefaultSize());
	assert(!page->next);
	assert(!page->prev);
	assert(page->fromPool);

	all.push_back(page);
	return page;
}

MemoryPool::MemoryPool() {
}

MemoryPool::MemoryPool(size_t size, void* data) {
	addMemory(data, size);
}

void MemoryPool::clear() {
	if (empty())
		return;
	MemoryPoolStatus status;
	clear(status);
	assert(all.empty());
	assert(largePages.empty());
	assert(smallPages.empty());
}

void MemoryPool::rewind() {
	if (empty())
		return;
	MemoryPoolStatus status;
	rewind(status);
}

void* MemoryPool::allocAll(size_t& size) {
	if (rewinding) {
		size = 0;
		return nullptr;
	}
	MemoryPoolPage* current = all.back();
	if (!current) {
		size = 0;
		return nullptr;
	};
	size_t padding = 0;
	void* data = nullptr;
	size = 0;
	MemoryPoolPage::FitsIn<1>(current, size, padding, data);
	if (current->available <= padding)
		return nullptr;
	size = current->available - padding;
	current->available = 0;
	return data;
}


char* MemoryPool::strndup(const char* src, size_t len) {
	if (!src)
		return nullptr;
	char* dst = static_cast<char*>(_alloc<sizeof(char)>(len + 1));
	if (!dst)
		return nullptr;
	memcpy(dst, src, len);
	dst[len] = '\0';
	return dst;
}

char* MemoryPool::strdup(const char* src) {
	if (!src)
		return nullptr;
	//return mpmemdup(src, strlen(src)+1);
	return strndup(src, strlen(src));
}

int MemoryPool::vprintf(char** strPtr, size_t max, const char* format, va_list ap) {
	if (max == 0)
		return -1;
	int size = 0;
	char* str = static_cast<char*>(_alloc<sizeof(char)>(max));
	if (str) {
		//check(str != nullptr);
		//check(p != nullptr);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
		size = vsnprintf(str, max, format, ap);
#pragma clang diagnostic pop
		// check for error
		if (size < 0) {
			reuse(str, max);
			if (strPtr != nullptr)
				*strPtr = nullptr;
			return size;
		}
		// check if there was enough size in the string, otherwise for safety terminate string
		if (size_t(size) >= max) {
			str[max - 1] = '\0';
		} else {
			// some space is left!
			reuse(str + size + 1, max - size_t(size) - 1);
		}
	}
	if (strPtr != nullptr)
		*strPtr = str;
	return size;
}

int MemoryPool::printf(char** strPtr, size_t max, const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	int retval = vprintf(strPtr, max, format, ap);
	va_end(ap);
	return retval;
}

// not terminating the string, useful for generating byte strings
int MemoryPool::vprintf_nt(char** strPtr, size_t max, const char* format, va_list ap) {
	if (max == 0)
		return -1;
	char* str = static_cast<char*>(_alloc<sizeof(char)>(max + 1)); // + 1 due to null terminating
	//check(str != nullptr);
	int size = 0;
	//check(p != nullptr);
	if (str) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
		size = vsnprintf(str, max + 1, format, ap);
#pragma clang diagnostic pop
		// check for error
		if (size < 0) {
			// check if pointer is really alloced inside memorypool
			reuse(str, max);
			if (strPtr != nullptr)
				*strPtr = nullptr;
			return size;
		}
		// some space is always left due to null terminating byte that is not needed
		reuse(str + size, max + 1 - size_t(size));
	}
	if (strPtr != nullptr)
		*strPtr = str;
	return size;
}

bool MemoryPool::reuse(char* buffer, size_t len) {
	return finishedWith(buffer, len) || addMemory(buffer, len);
}

bool MemoryPool::finishedWith(char* buffer, size_t len) {
	MemoryPoolPage* current = all.back();
	if (!current || buffer + len != next())
		return false;
	current->available += len;
	return true;
}

int MemoryPool::printf_nt(char** strPtr, size_t max, const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	int retval = vprintf_nt(strPtr, max, format, ap);
	va_end(ap);
	return retval;
}

MemoryPool::MemoryPool(MemoryPool* from, size_t size) {
	size += sizeof(MemoryPoolPage);
	if (from && from->available() >= size) {
		void* data = from->_alloc(size);
		addMemory(data, size);
	}
}

MemoryPool::MemoryPool(MemoryPool& from, size_t size) {
	size += sizeof(MemoryPoolPage);
	if (from.available() >= size) {
		void* data = from._alloc(size);
		addMemory(data, size);
	}
}

void MemoryPool::reserve(size_t bytes) {
	if (bytes == 0)
		return;
	size_t padding = 0;
	void* data = nullptr;
	if (!all.empty() && MemoryPoolPage::FitsIn<DEFAULT_ALIGN_ON>(all.back(), bytes, padding, data))
		return;
	// look into next pools
	if (bytes > MemoryPage::SIZE - sizeof(MemoryPoolPage)) {
		MemoryPoolPage* page = largePages.front();
		while (page) {
			// if it can fit, exits
			if (MemoryPoolPage::FitsIn<DEFAULT_ALIGN_ON>(page, bytes, padding, data))
				return;
			page = largePages.next(page);
		}

		size_t size = bytes + sizeof(MemoryPoolPage);
		data = malloc_with_minimal_size<char>(size);
		assert(data);
		page = new (data) MemoryPoolPage(size, false, true);
		largePages.push_back(page);
		return;
	}
	MemoryPoolPage* page = smallPages.front();
	while (page) {
		// if it can fit, exits
		if (MemoryPoolPage::FitsIn<DEFAULT_ALIGN_ON>(page, bytes, padding, data))
			return;
		page = smallPages.next(page);
	}
	page = new (MemoryPage::Get()) MemoryPoolPage(MemoryPage::SIZE, true, false);
	smallPages.push_back(page);
}

size_t MemoryPool::allocatedSizeOverestimation() {
	size_t retval = 0;
	for (auto p : all)
		retval += p->size;
	for (auto p : smallPages)
		retval += p->size;
	for (auto p : largePages)
		retval += p->size;
	return retval;
}

}
}
#endif

/**
Copyright 2010-2018 Bernard van Gastel, bvgastel@bitpowder.com.
This file is part of Bit Powder Libraries.

Bit Powder Libraries is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bit Powder Libraries is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bit Powder Libraries.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef _WIN32
#define _USE_MATH_DEFINES // needed for windows
#endif
#include <math.h>
#include <cmath>

#include "parser.h"
#include "simplestring.h"
#include "stringparse.h"

#include <functional>
#include <iterator>
#include <map>
#include <string>
#include <ostream>
#include <iostream>

IGNORE_WARNINGS_START
#include <gtest/gtest.h>
IGNORE_WARNINGS_END

using namespace bitpowder::lib;
using namespace std;

namespace tests {

struct Operator {
	char op;

	Operator(char _op = 0) : op(_op) {
	}

	bool operator==(const Operator& b) const {
		return op == b.op;
	}
};


class LispLexer {
	String original;
	StringParser c;
	Token<String> stringToken;
	Token<int> numberToken;
	Token<Operator> opToken;

	constexpr static auto space = C(' ') + C('\n') + C('\t') + C('\r');

	void whitespace() {
		c.span(space);
	}
 public:
	static const Operator FUNC_OPEN;
	static const Operator FUNC_CLOSE;
	static const Operator NIL_OBJECT;

	LispLexer(const String& str) : original(str), c(str, 0) {
	}
	TokenBase* next() {
		whitespace();
		const char* currentPtr = c.remaining().pointer();
		TokenBase* retval = recognise();
		if (retval != nullptr) {
			retval->start = StringLength(currentPtr - original.pointer());
			retval->length = StringLength(c.remaining().pointer() - currentPtr);
		}
		return retval;
	}
	TokenBase* recognise() {
		if (StringParser(c).accept("nil"_S).remainder(c)) {
			opToken.value = NIL_OBJECT;
			return &opToken;
		}
		if (StringParser(c).accept(FUNC_OPEN.op).remainder(c)) {
			opToken.value = FUNC_OPEN;
			return &opToken;
		}
		if (StringParser(c).accept(FUNC_CLOSE.op).remainder(c)) {
			opToken.value = FUNC_CLOSE;
			return &opToken;
		}
		if (StringParser(c).accept('"').splitOn(stringToken.value, C('"')).remainder(c)) {
			return &stringToken;
		}
		if (StringParser(c).number(numberToken.value).remainder(c))
			return &numberToken;
		if (StringParser(c).splitOn(stringToken.value, space).remainder(c) && stringToken.value.size() > 0)
			return &stringToken;
		return nullptr;
	}
	String getInputString(TokenBase* token) {
		return token ? original.substring(token->start, token->length) : "(none)";
	}
};

constexpr decltype(LispLexer::space) LispLexer::space;

const Operator LispLexer::NIL_OBJECT = 'n';
const Operator LispLexer::FUNC_OPEN = '(';
const Operator LispLexer::FUNC_CLOSE = ')';

template <class Lexer = LispLexer>
struct LispParserState {
	typedef int Data;
	typedef MemoryPool& UserData;
	typedef Parser<Lexer, 1, Data, UserData>& PS;
	static const int ERROR_IN_CONSTANT = 10;

	// nil -> int = Operator(LispLexer::NIL_OBJECT) {{ nil = 0; return 0; }};
	static PS lispNil(PS cont) {
		return cont().accept(LispLexer::NIL_OBJECT).store(0);
	}

	// number -> int = int;
	static int lispNumber(int& retval, const Token<int>& t, UserData) {
		retval = t.value;
		return 0;
	}

	// string -> int = String {{ string = 42; return 0; }};
	static int lispString(int& retval, const Token<String>& t UNUSED, UserData) {
		retval = 42;
		return 0;
	}

	// primary -> int = nil | number | string;
	static PS primary(PS cont) {
		return cont().choose(lispNil, lispNumber, lispString);
	}

	// arguments -> std::vector<int> = a:expr { arguments.push_back(a); return 0; } arguments?;
	static Parser<Lexer, 1, std::vector<int>, UserData>& arguments(Parser<Lexer, 1, std::vector<int>, UserData>& cont) {
		return cont().process(expr, [](std::vector<int>& data, int a, UserData) {
			//std::cout << "parsed " << a << std::endl;
			data.push_back(a);
			return 0;
		}).opt(arguments);
	}

	/*
	 * func -> int = char('(') String:id expr:a [std::vector<int> = (a:expr {{ arguments.push_back(a); return 0; }})*]:arguments char(')') {{
	 * func -> int = char('(') String:id expr:a arguments:t char(')') {{
	 *          if ("-"_S == id) for (int i : t) a -= i;
	 *          else if ("+"_S == id) for (int i : t) a += i;
	 *          else return 1;
	 *          func = a;
	 *          return 0;
	 *      }};
	 */
	static PS func(PS cont) {
		std::vector<int> args;
		String id;
		return cont()
					 .accept(LispLexer::FUNC_OPEN)
					 .fetch(id)
					 .perform(expr)
					 .fetch(arguments, args)
					 .accept(LispLexer::FUNC_CLOSE)
		.modify([&id, &args](int& a, UserData) {
			//std::cout << "function has " << args.size() << " arguments" << std::endl;
			if ("-"_S == id) {
				for (int i : args)
					a -= i;
			} else if ("+"_S == id) {
				for (int i : args)
					a += i;
			} else {
				return -4;
			}
			//std::cout << "result is " << a << std::endl;
			return 0;
		});
	}

	// expr -> int = primary | func;
	static PS expr(PS cont) {
		return cont().choose(primary, func);
	}
	// topLevel -> int = expr $;
	static PS topLevelExpr(PS cont) {
		return cont().choose(expr).end();
	}
	// UserData := MemoryPool&;
};

int ParseLispExpression(const String& str, int& result, MemoryPool& mp) {
	//std::cout << "parsing: " << str << std::endl;
	LispLexer lexer(str);
	ParserState<LispLexer, 1> p = ParserState<LispLexer, 1>(lexer);
	ParserError retval = Parser<LispLexer, 1, int, MemoryPool&>(&p, mp).perform(LispParserState<LispLexer>::topLevelExpr).end().retreive(result);
	if (retval) {
		//std::cout << "parsing error in " << str << " code=" << retval << " part='" << lexer.getInputString(p.getToken()) << "' result=";
		//std::cout << result;
		//result.print(std::cout);
		//std::cout << std::endl;
	}
	return retval.error();
}


TEST(Parser, LispParserState) {
	MemoryPool mp;
	int result = 0;

	EXPECT_EQ(0, ParseLispExpression("nil", result, mp));
	EXPECT_EQ(0, result);

	EXPECT_EQ(0, ParseLispExpression("10", result, mp));
	EXPECT_EQ(10, result);

	EXPECT_EQ(0, ParseLispExpression("\"lala\"", result, mp));
	EXPECT_EQ(42, result);

	EXPECT_EQ(0, ParseLispExpression("(+ 1 2 3 4 5)", result, mp));
	EXPECT_EQ(15, result);

	EXPECT_EQ(0, ParseLispExpression("(+ 1 (- 3 2) 4 5)", result, mp));
	EXPECT_EQ(11, result);

	EXPECT_EQ(0, ParseLispExpression("(+ (- 3 2) (- 5 4) (- 6 5))", result, mp));
	EXPECT_EQ(3, result);

	EXPECT_EQ(-4, ParseLispExpression("(range-in (mod (- in 4) 8) 1 2)", result, mp));
	EXPECT_EQ(3, result);
}

}

namespace std {
inline std::ostream& operator<< (std::ostream& out, const tests::Operator& op) {
	out << "(operator " << op.op << ")";
	return out;
}

}

# The RECA Tool
The REsource Consumption Analysis (RECA) tool aims to inform developers about the resource consumption of software-controlled hardware components by applying statical analysis techniques. When supplied with a resource consumption model for each hardware component, it can predict the amount of resources that will be consumed when running the program. For more information about the analysis, see ch. 5 and 6 of the [PhD thesis of Bernard van Gastel](http://www.sustainablesoftware.info/download/thesis-met-cover.pdf) Note: the thesis is about Energy Consumption Analysis(ECA). The project has been renamed to resource consumption analysis, but the analysis techniques are exactly the same. 

Programs can be defined in the ECA language (see PhD thesis) or in a subset of [Lua 5.3](http://www.lua.org/manual/5.3/). Both languages are parsed to the same AST data structures. Component models can be defined both in ECA and in Lua as well. See `/examples/` for examples.

Multiple analysis techniques are possible. Currently, there is only one analysis technique implemented, which is evaluating the program/runs the program while keeping track of the resource consumed. More analysis techniques will be added later. In order to do the analysis, a timing model for each language construct is needed.

Alternatively, programs in Lua can also be run directly in a simulation mode (where the official Lua interpreter is used).
The [C API](http://www.lua.org/manual/5.3/manual.html#4) of Lua is used to run the simulation of Lua programs and to run Lua defined components.

# How to install/run

We use the files in `/examples/` to demonstrate the tool. The analysis for ECA can be run with `./eca2 soundsystemprogram1.eca soundsystem1.eca`, where `soundsystemprogram1.eca` contains the program to be run and `soundsystem1.eca` contains the model for the sound system component.

ECA programs can be run with Lua defined component models, and vice versa: `./eca2 soundsystemprogram1.eca soundsystem1.lua`.

Lua programs can also be run directly: `./eca2 -simulate soundsystemprogram1.lua soundsystem1.eca`

It is possible to specify initial values of variables as command line arguments: `./eca2 soundsystemprogram_cl_arg.lua soundsystem1.lua max=5`

# Differences with the thesis

### ECA syntax
In the thesis, there is a `begin` keyword before ECA function definitions. This is omitted in the implementation.

### Function definitions
In the thesis, function definitions are allowed at any level. In the implementation, function definitions are only allowed at the top level, before any statement is executed.

### Scoping
In the thesis, assignments are done to the global state. In the implementation, scoping is applied. Assignment are declarations in the innermost scope, unless the variable is already defined in an outer scope:
```
function f(a)
   x := 1;        // <--------Declares x in this scope-----|
   if x then      //                                       |
      y := 42;    // <---Declares y in this scope------|   |
      x := 0;     // Assignment to x from outer scope  |   |
      skip        // <---------------------------------|   |
   else skip end; //                                       |
   x := y         // Error: y not in scope!                |
end               // <-------------------------------------|
```

Note: it is not possible to use comments in ECA, so these need to be removed from this example in order to run.

In the Lua frontend of RECA, scoping works as defined in the [Reference Manual](http://www.lua.org/manual/5.3/manual.html#3.5). Lua has both local variables(that are visible in the scope block they are declared in) and global variables. An assignment `x = y` assigns to a local variable named `x` if this variable is in scope, otherwise it assigns the value of `y` to the global variable `x`.

```
function f(a)
  do
    local loc = 1   -- <---Declares local loc in this scope---|
    glob = "world"  --  Assigns to global glob                |
    loc = "hello"   --  Assigns to local loc                  |
    print(loc)      --  Lookup for local loc                  |
  end               -- <--------------------------------------|
  print(glob)       -- Lookup for global glob
end
```

# Lua frontend
A Lua frontend has been added to the system to be able to analyse programs written in a real world language. Below is the syntax of accepted Lua programs (the set of accepted programs is a strict subset of the programs accepted by the official Lua syntax):
```
program ::= (function)* block

function ::= 'function' funcName '(' varName ')' block 'end'

block ::= (stat)* ['return' expr]

stat ::= ';'
       | 'do' block 'end'
       | varName '=' exp
       | 'local' varName '=' exp
       | 'if' exp 'then' block ('elseif' exp 'then' block)* ['else' block] 'end'
       | 'while' exp 'do' block 'end'
       | functioncall
       | compfunccall

functioncall ::= varName ‘(’ exp ‘)’
compfunccall ::= compName'_'compfuncName ‘(’ exp ‘)’

exp ::= varname | functioncall | compfunccall
      | 'false' | 'true' | Numeral | String
      | '(' exp ')' | exp binop exp

binop ::= 'or' | 'and'
        | '<' | '>' | '<=' | '>=' | '==' | '~='
        | '..'
        | '+' | '-' | '*'
```

For the numerals, only integers are allowed. Literal strings can only be delimited by quotes `"` and `'` (although the official Lua syntax support more ways).

# Implementation details

Lua is a dynamically typed language, although the analysis is statically typed. We assume the programs analysed are type safe. Future work include adding a type checker (possible coupled with type annotations and/or type inference) to enforce this assumption.

As mentioned before, we have introduced scoping in ECA:
```
function f(a)
   x := 1;        // <--------Declares x in this scope-----|
   if x then      //                                       |
      x := 0;     // Assignment to x from outer scope      |
   else skip end; //                                       |
end               // <-------------------------------------|
```
We need to derive if an assigment is a declaration or a 'real' assignment, assigning to an existing variable from an outer scope. This derivation is implemented as a separate step on the AST. This step can be extended to incorporate type checking/type inference as well. Currently, the derivation step reuses the same data structure that is used for scoping in the program state. It is recommended to change this to a new data structure when incorporating a type system.

The scoping rules for ECA are conceptually equivalent to the scoping rules for Lua without use of the global state. This means that we can translate declarations and assignments for both ECA and Lua to the same AST datastructure.

Scoping mechanisms are implemented using a global program state and a stack of local program states. When entering a scope, a new local state is pushed on the stack, and all local declarations are done to this local state. Assignments to and lookups of variables are done to the topmost local state that contains this variable, or to the global state otherwise. When exiting a scope, the topmost local state is popped from the stack.

# Next steps
- Validate the analysis on a (fictitional) use case
- Add more analysis methods, like overestimating ones
- Find a method to determine a good timing model

See the issue list on GitLab for the remaining issues.

#include <gtest/gtest.h>
#include <stdio.h>
#include "LuaLexer.h"
#include "LuaParser.h"
#include "parse.h"
#include "parser.h"
#include "simplestring.h"
#include "stringparse.h"

using namespace bitpowder::lib;

typedef LuaLanguage<LuaLexer> Language;

namespace {

template<class T> string toString(T& t, bool *ok = NULL) {
    ostringstream stream;
    stream << t;
    if(ok != NULL) *ok = stream.fail() == false;
    return stream.str();
}

string toString(Language::Stmt t, bool *ok = NULL){
    ostringstream stream;
    t->print(stream, Lua);
    if(ok != NULL) *ok = stream.fail() == false;
    return stream.str();
}

string toString(Language::FuncDef t, bool *ok = NULL){
    ostringstream stream;
    t->print(stream, Lua);
    if(ok != NULL) *ok = stream.fail() == false;
    return stream.str();
}

string toString(Language::Program t, bool *ok = NULL){
    ostringstream stream;
    t->print(stream, Lua);
    if(ok != NULL) *ok = stream.fail() == false;
    return stream.str();
}

template<class resultType=Language::Stmt, typename Type>
/**
 * @brief TestParse parses a language construct and tests if the parsing succeeds
 * @param input The string to parse
 * @param parse The function of LuaLanguage that parses the language construct
 * @param str The expected toString of the result
 * @param correct Should the parsing give errors?
 * @return The AST object
 */
resultType TestParse(String input, Type& parse, String str, bool correct = true){
    MemoryPool mp;
    LuaLexer lexer(input, mp);
    auto p = ParserState<LuaLexer, LUA_LOOKAHEAD>(lexer);
    resultType result;
    ParserError retval = Parser<LuaLexer, LUA_LOOKAHEAD, resultType, MemoryPool&>(&p, mp)
            .perform(parse)
            .end()
            .retreive(result);
    if(correct) {
        EXPECT_FALSE(retval) << "No parse result";
        if (retval) {
            TokenBase *token = p.getToken();
            std::cout << "error: " << retval.error() << std::endl;
            std::cout << "token: " << token << " at " << (token ? token->start : 0) << std::endl;
            std::cout << "token representing: " << lexer.getInputString(token) << std::endl;
            std::cout << "remaining: {" << lexer.remaining() << "}" << std::endl;
        } else {
            std::cout << toString(result) << std::endl;
            string x = toString(result);
            EXPECT_EQ(toString(result), str);
        }
    } else {
          EXPECT_TRUE(retval);
    }
    return result;
}

TEST(LuaParser, constant){
    Language::Stmt result = TestParse("42", Language::constant, "42");
    EXPECT_TRUE(result->isExpression());
    //result = TestParse("1.0", Language::constant, "1.0");
    //EXPECT_TRUE(result->isExpression());
    result = TestParse("\"hi\"", Language::constant, "\"hi\"");
    EXPECT_TRUE(result->isExpression());
    result = TestParse("true", Language::constant, "true");
    EXPECT_TRUE(result->isExpression());

}

TEST(LuaParser, functionCall) {
    String input = "func(10)";
    TestParse(input, Language::functionCall, "func(10)");
    TestParse("f(g(10))", Language::functionCall, "f(g(10))");
}

TEST(LuaParser, componentFunctionCall) {
    String input = "radio_send(10)";
    TestParse(input, Language::functionCall, "radio_send(10)");
}

TEST(LuaParser, exprPlus){
    String input = "x + y + (1-2)";
    TestParse(input, Language::exprPlus, "((x + y) + (1 - 2))");
}

TEST(LuaParser, exprMul){
    String input = "a*b + c//d*e";
    TestParse(input, Language::exprPlus, "((a * b) + ((c / d) * e))");
}

TEST(LuaParser, exprComp){
    String input = "1 <= 2 == 3 * 4";
    TestParse(input, Language::exprComp, "((1 <= 2) == (3 * 4))");
}

TEST(LuaParser, exprAndOr){
    String input = "x and y or z and a";
    TestParse(input, Language::expr, "((x and y) or (z and a))");
}

TEST(LuaParser, exprPow){
    String input = "x ^ y ^ z";
    TestParse(input, Language::expr, "(x ^ (y ^ z))");
}

TEST(LuaParser, exprEverything){
    String input = "a or b and c ~= d | e ~ f & g >> h .. i + j * k ^ l";
    TestParse(input, Language::expr, "(a or (b and (c ~= (d | (e ~ (f & (g >> (h .. (i + (j * (k ^ l)))))))))))");
}

TEST(LuaParser, block){
    TestParse("b = b c = c", Language::block, "b = b;c = c");
    TestParse("b = b", Language::block, "b = b");
    TestParse(";", Language::block, ";");
    TestParse("", Language::block, ";");
}

TEST(LuaParser, doBlock){
    TestParse("do b = b c = c end", Language::doBlock, "b = b;c = c");
    TestParse("do end", Language::doBlock, ";");
}

TEST(LuaParser, stmt){
    TestParse("c = f(c) * c", Language::stmt, "c = (f(c) * c)");
    TestParse("f(10)", Language::stmt, "f(10)");
}

TEST(LuaParser, stmtIf){
    TestParse(
        "if a then local b = b end", Language::stmtIf,
        "if a then local b = b else ; end");
    TestParse(
        "if a then b = b else c = c end", Language::stmtIf,
        "if a then b = b else c = c end");
    TestParse(
        "if a then b = b elseif c then d = d end", Language::stmtIf,
        "if a then b = b else if c then d = d else ; end end");
    TestParse(
        "if a then b = b elseif c then d = d elseif e then f = f else g = g end", Language::stmtIf,
        "if a then b = b else if c then d = d else if e then f = f else g = g end end end");
}

TEST(LuaParser, stmtWhile){
    TestParse("while x do ;; end", Language::stmtWhile, "while x do ;;; end");
}

TEST(LuaParser, funcDef){
    MemoryPool mp;
    LuaLexer lexer("function f(c) c = f(c) * c return c end", mp);
    auto p = ParserState<LuaLexer, LUA_LOOKAHEAD>(lexer);
    Language::Program result;
    ParserError retval = Parser<LuaLexer, LUA_LOOKAHEAD, Language::Program, MemoryPool&>(&p, mp)
            .store(new ASTProgram)
            .perform(Language::funcDef)
            .end()
            .retreive(result);
    EXPECT_FALSE(retval) << "No parse result";
    if (retval) {
        TokenBase *token = p.getToken();
        std::cout << "error: " << retval.error() << std::endl;
        std::cout << "token: " << token << " at " << (token ? token->start : 0) << std::endl;
        std::cout << "token representing: " << lexer.getInputString(token) << std::endl;
        std::cout << "remaining: {" << lexer.remaining() << "}" << std::endl;
    } else {
        std::cout << toString(result->functions["f"]) << std::endl;
        EXPECT_EQ(toString(result->functions["f"]), "function f(c)\nc = (f(c) * c);return c\nend");
    }
    MemoryPool mp2;
    ASTParseResult res = ParseLuaMemory("function branch(x) if x then return 0 end x = y return 1 end", mp2);
    ASSERT_TRUE(bool(res));
    string resString = toString(res.result());
    EXPECT_EQ(resString, "function branch(x)\nif x then return 0 else ; end;x = y;return 1\nend\n;");

    //return should be on the end of a block
    res = ParseLuaMemory("function branch(x)if x then return 0 y = x end end", mp2);
    EXPECT_FALSE(res);
}

TEST(LuaParser, program){
    TestParse<Language::Program>(
              "function f(c) return c + 1 end "
              "function g(h) return h - 1 end "
              "x = 1 + 1;"
              "f(g(x))"
                ,
              Language::program,
              "function f(c)\n"
                "return (c + 1)\n"
              "end\n"
              "function g(h)\n"
                "return (h - 1)\n"
              "end\n"
              "x = (1 + 1);;;f(g(x))");
}

TEST(LuaParser, parser){
    MemoryPool mp;
    ASTParseResult res = ParseLuaMemory(
                "function f(c) return c + 1 end "
                "function g(h) return h - 1 end "
                "x = 1 + 1;"
                "f(g(x))"
                , mp);
    EXPECT_TRUE(bool(res));
    if(res){
        ASTProgram::Ref ast = res.result();
        EXPECT_EQ(toString(ast), "function f(c)\n"
                                    "return (c + 1)\n"
                                 "end\n"
                                 "function g(h)\n"
                                     "return (h - 1)\n"
                                 "end\n"
                                 "x = (1 + 1);;;f(g(x))");
    }
}

}



























#include <gtest/gtest.h>
#include "ECAComponent.h"
#include "libtypesystem.h"
#include "LuaParser.h"
#include "parse.h"

/* Every test is executed with:
 * - Component: a radio (see SetUp())
 * - Environment: function f(x) x := x + 1; x end
 * - State: magic_number = 42
 *
 * As the Lua components are extensively tested in LuaComponents.test.cpp, these tests are run with
 * an ECA component. ECA components are less complicated then Lua components, so a separate test suite
 * is unnecassary. In this way, they are still tested a little.
 */

static State deriveState;
// Just parses ECA code. It assumes the code is wellformed.
Rule* justParseECA(string code)
{
	MemoryPool mp;
	auto result = ParseECAMemory(code, mp).result();
	result->derive_assignment_sorts(deriveState);
	return result->getTypeRule();
}

static map<string, Component*> components;
static FunctionEnvironment env;
static State programState;


// times differ a factor of 10 to make them more identifiable in the test results
// and to make the chance on a false positive small
const int inputTime  = 1000*1000;
const int constTime  =  100*1000;
const int varTime    =   10*1000;
const int assignTime =      1000;
const int binopTime  =       100;
const int ifTime     =        10;
const int whileTime  =         1;

const int fTime = assignTime + varTime + constTime + binopTime + varTime; // The time that a call to the function f should take

void SetUp()
{
	programState = State();
	// Initialize component radio
	// To do: use readECAComponent with a string stream to define the component in one string
	string compName = "radio";
	State compState = State();
	compState.declareLocalVar("is_on", new ValueInt(0));
	deriveState.declareLocalVar("is_on", nullptr);

	deriveState.declareLocalVar("x", nullptr);
	ECAComponentFunction* on = new ECAComponentFunction(compName, "on", "x",
		justParseECA("is_on := 1"),
		justParseECA("0"),
		0
	);

	ECAComponentFunction* off = new ECAComponentFunction(compName, "off", "x",
		justParseECA("is_on := 0"),
		justParseECA("0"),
		0
	);

	deriveState.declareLocalVar("nr_of_units", nullptr);

	/* send sends a number of units (radio waves/messages/etc.) over the radio.
	 * It returns if the sending succeeded. The sending of a single message
	 * costs 100 energy. Sending always takes 5 time units in total.
	 */
	ECAComponentFunction* send = new ECAComponentFunction(compName, "send", "nr_of_units",
		justParseECA("is_on"),
		justParseECA("nr_of_units * 100 * is_on"),
		5
	);

	// The radio consumes 10 energy per time unit if it is on and 1 if it is off
	Rule* tdec = justParseECA("c := 0; if is_on then c := 10 else c := 1 end, c");

	ECAComponent* radio = new ECAComponent(compName, compState, {{"on", on}, {"off", off}, {"send", send}}, tdec);
	components.insert_or_assign("radio", radio);

	// Initialize environment
	Rule* fDef = justParseECA("x := x + 1, x");
	Function* f = new Function("f", "x", new RuleReturn(fDef, nullptr));

	env = FunctionEnvironment(inputTime, constTime, varTime, assignTime, binopTime, ifTime, whileTime, {f});
}

template<typename Parser>
EvaluationResult* parseAndEvaluate(string code, Parser&& parse)
{
	SetUp();
	MemoryPool mp;
	ASTParseResult res = parse(code, mp);
	if(!res)
		return nullptr;
	programState.declareLocalVar("magic_number", new ValueInt(42));
	State deriveState;
	deriveState.declareLocalVar("magic_number", nullptr);
	res.result()->derive_assignment_sorts(deriveState);
	Rule* typeRule = res.result()->getTypeRule();
	return new EvaluationResult(typeRule->evaluateRule(programState, env, components));
}

void checkValue(Value* v, nullptr_t, string debugInfo) {
	EXPECT_FALSE(v) << "evaluation incorrectly produced value\n" << debugInfo;
}

void checkValue(Value* v, bool referenceVal, string debugInfo) {
	ValueBool* vbool = dynamic_cast<ValueBool*>(v);
	ASSERT_TRUE(vbool) << "Expected boolean value\n"         << debugInfo;
	ASSERT_EQ(vbool->getValue(), referenceVal)               << debugInfo;
}

void checkValue(Value* v, double referenceVal, string debugInfo) {
	ValueFloat* vfloat = dynamic_cast<ValueFloat*>(v);
	ASSERT_TRUE(vfloat) << "Expected floating point value\n" << debugInfo;
	ASSERT_EQ(vfloat->getValue(), referenceVal)              << debugInfo;
}

void checkValue(Value* v, int referenceVal, string debugInfo) {
	ValueInt* vint = dynamic_cast<ValueInt*>(v);
	ASSERT_TRUE(vint) << "Expected integer value\n"          << debugInfo;
	ASSERT_EQ(vint->getValue(), referenceVal)                << debugInfo;
}

void checkValue(Value* v, const char* referenceVal, string debugInfo) {
	ValueString* vstring = dynamic_cast<ValueString*>(v);
	ASSERT_TRUE(vstring) << "Expected string value\n"        << debugInfo;
	ASSERT_EQ(vstring->getValue(), referenceVal)             << debugInfo;
}

template<typename Parser, typename retVal>
void testWithParser(string code, Parser&& parse, string language, retVal referenceVal, int time, int resource)
{
	if(code == "NULL")
		return; //no representation for this parser
	string debugInfo = "language: " + language + "  code: " + code;
	EvaluationResult* sr = parseAndEvaluate(code, parse);
	if(!sr)
		FAIL() << "Error while parsing\n" << debugInfo;
	EXPECT_EQ(sr->getTime(), time) << debugInfo;
	EXPECT_EQ(sr->getResourceAmount(), resource) << debugInfo;
	checkValue(sr->getValue(), referenceVal, debugInfo);
}

void test(string ecaRepr, string luaRepr, int time, int resource)
{
	testWithParser(ecaRepr, ParseECAMemory, "ECA", nullptr, time, resource);
	testWithParser(luaRepr, ParseLuaMemory, "Lua", nullptr, time, resource);
}

template<typename retType>
void test(string ecaRepr, string luaRepr, retType val, int time, int resource)
{
	testWithParser(ecaRepr, ParseECAMemory, "ECA", val, time, resource);
	testWithParser(luaRepr, ParseLuaMemory, "Lua", val, time, resource);
}

void checkState(string varname, int check){
	ValueInt* vint = dynamic_cast<ValueInt*>(programState.lookup(varname));
	ASSERT_TRUE(vint);
	EXPECT_EQ(vint->getValue(), check) << varname << " is " << vint->getValue() << " in stead of " << check;
}

void checkState(string varname, string check){
	ValueString* vstring = dynamic_cast<ValueString*>(programState.lookup(varname));
	ASSERT_TRUE(vstring);
	EXPECT_EQ(vstring->getValue(), check) << varname << " is " << vstring->getValue() << " in stead of " << check;
}



TEST(Analysis, Skip) {
	test("skip", ";", 0, 0);
}

TEST(Analysis, Int) {
	test("42", "NULL", 42, constTime, constTime);
}

TEST(Analysis, Assignment) {
	test("x := 42", "x = 42", 42, constTime + assignTime, constTime + assignTime);
	checkState("x", 42);
}

TEST(Analysis, String) {
	test("NULL", "x = \"hel\\\\lo\"", "hel\\lo", constTime + assignTime, constTime + assignTime);
}

TEST(Analysis, Var) {
	test("magic_number", "NULL", 42, varTime, varTime);

	/* A trick to make testing of expressions with Lua possible. In Lua, a program needs to be a block
	 * of statements, and a statement cannot be an expression. Assignments are statements in Lua, but are
	 * transformed to ECA assignments. In ECA, an assignment is an expression, returning the assigned value,
	 * making things like x := y := 1 + 2 possible. We can use this ECA behaviour to get the value of
	 * Lua expressions by assigning the result of the expression to a variable.
	 */
	test("x := magic_number", "x = magic_number", 42, varTime + assignTime, varTime + assignTime);
}

TEST(Analysis, BinOpBool) {
	test("NULL", "x = true and false" , false  , 2*constTime + binopTime + assignTime, 2*constTime + binopTime + assignTime);
	test("NULL", "x = true ~= false"  , true   , 2*constTime + binopTime + assignTime, 2*constTime + binopTime + assignTime);
	test("1 or 0" , "NULL", 1 , 2*constTime + binopTime, 2*constTime + binopTime);
	test("1 and 0", "NULL", 0 , 2*constTime + binopTime, 2*constTime + binopTime);
	EXPECT_THROW(parseAndEvaluate("x = 1 or 0", ParseLuaMemory), runtime_error);
}

TEST(Analysis, BinOpInt) {
	test("20+22", "NULL", 42, 2*constTime + binopTime, 2*constTime + binopTime);
	test("x := 20+22", "x = 20+22", 42, 2*constTime + binopTime + assignTime, 2*constTime + binopTime + assignTime);
}

TEST(Analysis, BinOpString) {
	test("NULL", "x = \"hel\" .. 'lo'", "hello", 2*constTime + binopTime + assignTime, 2*constTime + binopTime + assignTime);
}

TEST(Analysis, CallCompFunc) {
	test("radio.on(2019)", "radio_on(2019)", 1, constTime, constTime);
	test("x := radio.on(2019)", "x = radio_on(2019)", 1, constTime + assignTime, constTime + assignTime * 10);
}

TEST(Analysis, CallFunc){
	test("f(10)", "f(10)", 11, constTime + fTime, constTime + fTime);
	test("x := f(10)", "x = f(10)", 11, constTime + fTime + assignTime, constTime + fTime + assignTime);
	test("f(magic_number)", "f(magic_number)", 43, varTime + fTime, varTime + fTime);
	checkState("magic_number", 42);
	test("x := 10, f(magic_number)", "x = 10; f(magic_number)", 43, constTime + assignTime + varTime + fTime, constTime + assignTime + varTime + fTime);
	checkState("x", 10);
}

TEST(Analysis, ExprConcat){
	test("magic_number := 43, magic_number", "NULL", 43, constTime + assignTime + varTime, constTime + assignTime + varTime);
}

TEST(Analysis, FuncDef){
	test("function g(x) x - 1 end g(42)", "function g(x) return x - 1 end; g(42)", 2 * constTime + varTime + binopTime, 2 * constTime + varTime + binopTime);
	test("NULL", "function suffix(msg) return msg .. '.txt' end; x = suffix('readme')", 2 * constTime + varTime + binopTime + assignTime, 2 * constTime + varTime + binopTime + assignTime);
	checkState("x", "readme.txt");
}

TEST(Analysis, If){
	test("if(3) then magic_number := 100 else skip end", "NULL", constTime + ifTime + constTime + assignTime, constTime + ifTime + constTime + assignTime);
	checkState("magic_number", 100);
	test("NULL", "if(true) then magic_number = 100 end", constTime + ifTime + constTime + assignTime, constTime + ifTime + constTime + assignTime);
	checkState("magic_number", 100);
	EXPECT_THROW(parseAndEvaluate("if 3 then magic_number = 100 end", ParseLuaMemory), runtime_error);

	test("if(magic_number == 42) then 100 else 0 end", "NULL",
		 varTime + constTime + binopTime + ifTime + constTime, varTime + constTime + binopTime + ifTime + constTime);
	test("if magic_number > 100 then magic_number := 100 else skip end",
		 "if(magic_number > 100) then magic_number = 100 end",
		 varTime + constTime + binopTime + ifTime, varTime + constTime + binopTime + ifTime);
	checkState("magic_number", 42);
}

TEST(Analysis, Return_Statement){
	test("NULL", "function g(x) if x == 1 then x = 2 return x end return 0 end y = g(1)",
		 3 * constTime + 2 * varTime + 2 * assignTime + binopTime + ifTime,
		 3 * constTime + 2 * varTime + 2 * assignTime + binopTime + ifTime);
	checkState("y", 2);
}

TEST(Analysis, Repeat){
	test("repeat 3 begin magic_number := magic_number + 1 end, magic_number", "NULL", 45,
		 constTime + 3 * (assignTime + varTime + constTime + binopTime) + varTime,
		 constTime + 3 * (assignTime + varTime + constTime + binopTime) + varTime);
	checkState("magic_number", 45);
	test("repeat f(0-2) begin magic_number := magic_number + 1 end, magic_number", "NULL", 42, constTime * 2 + binopTime + fTime + varTime, constTime * 2 + binopTime + fTime + varTime);
	checkState("magic_number", 42);
}

TEST(Analysis, StmtConcat){
	test("magic_number := 2019; magic_number", "NULL", 2019, constTime + assignTime + varTime, constTime + assignTime + varTime);
	test("magic_number := 2019; magic_number := magic_number", "magic_number = 2019; magic_number = magic_number",
		 2019, constTime + 2 * assignTime + varTime, constTime + 2 * assignTime + varTime);
}

TEST(Analysis, While){
	int loopBodyTime = constTime + 3 * varTime + 2 * assignTime + 2 * binopTime;
	int checkTime = whileTime + constTime + binopTime + varTime;
	int totalTime = 2 * (assignTime + constTime) + 3 * loopBodyTime + 4 * checkTime + assignTime + varTime;
	//check if loop body time is correct
	test("fib := magic_number * magic_number; fib := magic_number + 1", "NULL", 43, loopBodyTime, loopBodyTime);
	test("x := 2; fib := 1; while x <= 4 begin fib := fib * x; x := x + 1 end, y := fib",
		 "x  = 2; fib  = 1; while(x <= 4) do   fib  = fib * x; x  = x + 1 end; y  = fib",
		 24, totalTime, totalTime);
}

TEST(Analysis, RadioComponentSimple){
	string ecaCode =
			"radio.on(0);"
			"radio.send(5);"
			"radio.off(0)";
	string luaCode =
			"radio_on(0);"
			"radio_send(5);"
			"radio_off(0)";
	int onTime = 5 + 2 * constTime;
	int offTime = constTime;
	int send_energy = 100*5;
	int total_td_ec = onTime * 10 + offTime ;
	int total_energy = send_energy + total_td_ec;
	test(ecaCode, luaCode, 0, onTime + offTime, total_energy);
}

TEST(Analysis, RadioComponentLoop){
	string ecaCode =
			"x := 1;"
			"radio.on(0);"
			"while x < 4 begin"
			"   radio.send(x);"
			"   x := x + 1"
			"end,"
			"radio.off(0)";
	string luaCode =
			"x = 1;"
			"radio_on(0);"
			"while(x < 4) do"
			"   radio_send(x);"
			"   x = x + 1;"
			"end;"
			"radio_off(0)";

	// Determine expected time
	int offTime = constTime + assignTime	// x := 1;
			+ constTime;					// radio.on(0);
	int checkTime = varTime + constTime + binopTime + whileTime; // check x < 4 and jump
	int bodyTime = varTime + 5								// radio.send(x);
			+ varTime + constTime + binopTime + assignTime;	// x := x + 1
	int onTime = 3 * bodyTime + 4 * checkTime
			+ constTime; //radio.off(0)

	// Determine expected energy consumption
	int incidentalEnergy = 600; //100 + 2 * 100 + 3 * 100
	int timeDependentEnergy = offTime + 10 * onTime;

	test(ecaCode, luaCode, 0, offTime + onTime, incidentalEnergy + timeDependentEnergy);
}

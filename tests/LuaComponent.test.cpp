#include <gtest/gtest.h>
#include "LuaComponent.h"
#include "ValueInt.h"
#include "ValueString.h"

/* LuaComponent is tested toroughly, because the Lua-C API can be tricky
 */

TEST(LuaComponent, call_comp_func){
    LuaComponent c("radio", "function send_definition(i) return i end;"
                            "function send_resource() return 1 end;" // If you don't need the argument, you can leave it out. Lua will
                                                                   // throw away extra arguments.
                            "function send_time(i) return 2 end", LuaComponent::Mode::code);
    /* Notice that this way of splitting a string into multiple lines can be tricky:
     * "if"
     * "true"
     * will result in the string "iftrue", without any whitespace.
     */
    EvaluationResult s(nullptr, 0, 0, false);
    ASSERT_NO_THROW(s = c.evaluateFunction("send", new ValueInt(42)));
    EXPECT_EQ(s.getTime(), 2);
    EXPECT_EQ(s.getResourceAmount(), 1);
    ValueInt* v = dynamic_cast<ValueInt*>(s.getValue());
    ASSERT_TRUE(v);
    EXPECT_EQ(v->getValue(), 42);
}

TEST(LuaComponent, dyn_res_cons){
    LuaComponent c("radio", "systemOn = 1; function dyn_res_cons() return 2 * systemOn end", LuaComponent::Mode::code);
    EXPECT_EQ(c.dynResCons(), 2);
}

TEST(LuaComponent, string_values){
    LuaComponent c("radio", "function send_definition(msg) return msg end;"
                            "function send_resource(msg) return msg:len() end;"
                            "function send_time(msg) return msg:len() end", LuaComponent::Mode::code);
    EvaluationResult s(nullptr, 0, 0, false);
    ASSERT_NO_THROW(s = c.evaluateFunction("send", new ValueString("SOS")));
    EXPECT_EQ(s.getTime(), 3);
    EXPECT_EQ(s.getResourceAmount(), 3);
    ValueString* v = dynamic_cast<ValueString*>(s.getValue());
    ASSERT_TRUE(v);
    EXPECT_EQ(v->getValue(), "SOS");
}

TEST(LuaComponent, getComponentFunctions){
    LuaComponent c("radio", "systemOn = 0;"
                            "function send_definition(msg) return msg end;"
                            "function send_resource(msg) return msg:len() end;"
                            "function send_time(msg) return msg:len() end;"
                            "function on_definition() systemOn = 1; return 0 end;"
                            "function on_time() return 0 end;"
                            "function on_resource() return 0 end", LuaComponent::Mode::code);
    unordered_set<string> s = c.getComponentFunctionNames();
    EXPECT_TRUE(s.find("send") != s.end());
    EXPECT_TRUE(s.find("on") != s.end());
    EXPECT_EQ(s.size(), 2);
}

/* Tests for erroneous behaviour
 * Most are disabled, because lua calls abort when it encounters an error. An error message gets printed
 * anyways, so the developer gets error information.
 * One could setup an error handler (see lua_pcall)
 */
TEST(LuaComponent, syntax_error)
{
    EXPECT_THROW(LuaComponent("radio", "function dyn_res_cons() retrun 2 * systemOn end", LuaComponent::Mode::code), runtime_error);
}

TEST(LuaComponent, invalid_return_value)
{
    LuaComponent c("radio", "function send_definition(msg) return nil end;"
                            "function send_resource(msg) return 0 end;"
                            "function send_time(msg) return 0 end", LuaComponent::Mode::code);
    EXPECT_THROW(c.evaluateFunction("send", new ValueInt(0)), runtime_error);
    LuaComponent d("radio", "function send_definition(i) return 0 end;"
                              "function send_resource(i) return \"wrong\" end;"
                              "function send_time(i) return 0 end", LuaComponent::Mode::code);
    EXPECT_THROW(d.evaluateFunction("send", new ValueInt(0)), runtime_error);
    LuaComponent e("radio", "function send_definition(msg) return 0 end;"
                              "function send_resource(msg) return 0 end;"
                              "function send_time(msg) return nil end", LuaComponent::Mode::code);
    EXPECT_THROW(e.evaluateFunction("send", new ValueInt(0)), runtime_error);
}

/* The next tests are disabled, because the implementation of LuaComponent uses lua_call, which
 * just aborts the program when it encounters an error. One could setup a Lua panic function
 * in combination with the use of lua_pcall to let Lua throw an error. See http://www.lua.org/manual/5.3/manual.html#4.6
 * for more information
 */

TEST(LuaComponent, DISABLED_runtime_error){
    LuaComponent c("radio", "function dyn_res_cons() return 2 * systemOn end;", LuaComponent::Mode::code); // systemOn not initialized
    EXPECT_THROW(c.dynResCons(), runtime_error);
}

TEST(LuaComponent, DISABLED_non_existent_function){
    LuaComponent c("radio", "", LuaComponent::Mode::code);
    EXPECT_THROW(c.evaluateFunction("on", new ValueInt(0)), runtime_error);
}

TEST(LuaComponent, DISABLED_incompatible_arg_type){
    LuaComponent c("radio", "function send_definition(msg) return 0 end;"
                            "function send_resource(msg) return msg:len() end;"
                            "function send_time(msg) return msg:len() end", LuaComponent::Mode::code);
    EXPECT_THROW(c.evaluateFunction("send", new ValueInt(0)), runtime_error);
}

TEST(LuaComponent, DISABLED_call_nil){
    LuaComponent c("radio", "function dyn_res_cons() n = nil; n() end;", LuaComponent::Mode::code);
    EXPECT_THROW(c.dynResCons(), runtime_error);
}

TEST(LuaComponent, DISABLED_index_nil){
    LuaComponent c("radio", "function dyn_res_cons() x = nil; return x[1] end;", LuaComponent::Mode::code);
    EXPECT_THROW(c.dynResCons(), runtime_error);
}

TEST(LuaComponent, DISABLED_lua_error){
    LuaComponent c("radio", "function dyn_res_cons() error() end;", LuaComponent::Mode::code);
    EXPECT_THROW(c.dynResCons(), runtime_error);
}



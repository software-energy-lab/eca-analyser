#include <gtest/gtest.h>
#include "ECAComponent.h"
#include "LuaComponent.h"
#include "LuaSimulator.h"

LuaComponent* makeSoundSystem();

TEST(LuaSimulator, compile){
    string prog = "i = 0; while i < 5 do i = i + 1 end";
    map<string, Component*> components;
    components.insert(make_pair("radio", new LuaComponent("radio", "function on_definition(x) return x end", LuaComponent::Mode::code)));
    LuaSimulator* sim = LuaSimulator::createSimulator(prog, LuaSimulator::Mode::code);
    sim->registerComponents(&components);
    EXPECT_TRUE(sim->getInstance());
    sim->~LuaSimulator();
}

TEST(LuaSimulator, simpleTime){
    string prog = "i = 0; while i < 30000 do i = i + 1 end";
    map<string, Component*> components;
    LuaSimulator* sim = LuaSimulator::createSimulator(prog, LuaSimulator::Mode::code);
    sim->registerComponents(&components);
    SimulationResult s(0, 0);

    chrono::time_point tStart = chrono::high_resolution_clock::now();
    ASSERT_NO_THROW(s = sim->run());
    chrono::nanoseconds timePassed = chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now() - tStart);

    EXPECT_EQ(s.resource, 0);
    EXPECT_TRUE(timePassed.count() * 9 / 10 < s.time) << "Warning: to much overhead";
    cout << "Simulated time: " << s.time << " ns\nActual time: " << timePassed.count() << " ns" << endl;
    EXPECT_TRUE(s.time < timePassed.count()) << "Error: simulated time is less than actual time";
    sim->~LuaSimulator();
}

TEST(LuaSimulator, ForgetArg){
    string prog = "comp_doThing()";
    LuaComponent* comp = new LuaComponent("comp",
            "function dyn_res_cons() return 0 end;"
            "function doThing_definition(msg) return 0 end;"
            "function doThing_time(msg) return 0 end;"
            "function doThing_resource(msg) return 0 end;", LuaComponent::Mode::code);
    map<string, Component*> components = {make_pair("comp", comp)};
    LuaSimulator* sim = LuaSimulator::createSimulator(prog, LuaSimulator::Mode::code);
    sim->registerComponents(&components);
    EXPECT_THROW(sim->run(), runtime_error);

    sim->~LuaSimulator();
}

TEST(LuaSimulator, StringValues){
    string prog = "if well_echo('ECHO!') == 'echo!' then "
                  "  well_tossCoin(10) "                   //The function tossCoin is being used to create a detectable side effect
                  "end";
    LuaComponent* comp = new LuaComponent("well",
            "function dyn_res_cons() return 0 end;"
            "function echo_definition(msg) return msg:lower() end;"
            "function echo_time(msg) return 0 end;"
            "function echo_resource(msg) return 0 end;"
            "function tossCoin_definition() return 0 end;"
            "function tossCoin_time() return 0 end;"
            "function tossCoin_resource(x) return x end;", LuaComponent::Mode::code);
    map<string, Component*> components = {make_pair("well", comp)};
    LuaSimulator* sim = LuaSimulator::createSimulator(prog, LuaSimulator::Mode::code);
    sim->registerComponents(&components);
    SimulationResult s = sim->run();
    EXPECT_EQ(s.resource, 10);
    sim->~LuaSimulator();
}

/* The problem with testing this is that the running time of the program is
 * dependent on the system that the test is executed on. To minimize the
 * dependency on the system, SoundSystem_sleep is modeled to take 10 milliseconds = 10 million ns.
 * This means that the time dependent energy consumption of a call to SoundSystem_sleep
 * is 20 million. To make the effect of the incidental energy of playBeep visible,
 * it is set to 30 million.
 * We allow for an overhead of at most 5%
 */
TEST(LuaSimulator, SimulationResult){
    string prog =
            "SoundSystem_on(0) \n"
            "i = 0 \n"
            "while i < 4 do \n"
            "    SoundSystem_playBeep(0) \n"
            "    SoundSystem_sleep(0) \n"       //takes 10 ms
            "   i = i + 1 \n"
            "end \n"
            "SoundSystem_off(0)";
    map<string, Component*> components = {make_pair("SoundSystem", makeSoundSystem())};
    LuaSimulator* sim = LuaSimulator::createSimulator(prog, LuaSimulator::Mode::code);
    sim->registerComponents(&components);
    SimulationResult s(0, 0);

    ASSERT_NO_THROW(s = sim->run());

    cout << "Simulated time: " << s.time << " ns\nSimulated energy: " << s.resource << endl;
    uint32_t sleep_time = 4*10000000;
    EXPECT_LT(sleep_time, s.time) << "Error: simulated time is less than sleep time";
    EXPECT_LT(s.time , sleep_time + sleep_time/20) << "Error: overhead unexpectedly large";

    uint32_t energy_lower_bound = 4*50000000;
    EXPECT_LT(energy_lower_bound, s.resource) << "Error: simulated energy is less than predicted energy";
    EXPECT_LT(s.resource, energy_lower_bound + energy_lower_bound/20) << "Error: overhead unexpectedly large";
}


LuaComponent* makeSoundSystem(){
        return new LuaComponent("SoundSystem",
            "-- @component SoundSystem \n"

            "systemOn = 0 \n"

            "function dyn_res_cons() \n"
            "  return 2*systemOn \n" //2 energy per ns
            "end \n"

            "-- on \n"
            "function on_time(x) \n"
            "  return 0 \n"
            "end \n"

            "function on_resource(x) \n"
            "  return 0 \n"
            "end \n"

            "function on_definition(x) \n"
            "  systemOn = 1 \n"
            "  return 1 \n"
            "end \n"

            "-- off \n"
            "function off_time(x) \n"
            "  return 0 \n"
            "end \n"

            "function off_resource(x) \n"
            "  return 0 \n"
            "end \n"

            "function off_definition() \n"
            "  systemOn = 0 \n"
            "  return 0 \n"
            "end \n"

            "-- playbeep \n"
            "function playBeep_time(x) \n"
            "  return 5 \n"
            "end \n"

            "function playBeep_resource(x) \n"
            "  return 30000000 \n"
            "end \n"

            "function playBeep_definition(x) \n"
            "  if systemOn then \n"
            "    x = 1 \n"
            "  else \n"
            "    x = 0 \n"
            "  end \n"
            "  return x \n"
            "end \n"

            "-- sleep \n"
            "function sleep_time(x) \n"
            "  return 10000000 \n"  //10 ms
            "end \n"

            "function sleep_resource(x) \n"
            "  return 0 \n"
            "end \n"

            "function sleep_definition(x) \n"
            "  return 0 \n"
            "end",
            LuaComponent::Mode::code);
}

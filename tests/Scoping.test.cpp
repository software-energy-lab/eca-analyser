#include <gtest/gtest.h>
#include "ECAComponent.h"
#include "libtypesystem.h"
#include "LuaParser.h"
#include "parse.h"

namespace {

static State programState;

template<typename Parser>
EvaluationResult* parseAndEvaluate(string code, Parser&& parse)
{
	MemoryPool mp;
	ASTParseResult res = parse(code, mp);
	if(!res)
		return nullptr;
	Rule* typeRule = res.result()->deriveAssignmentSorts()->getTypeRule();

	FunctionEnvironment env(0, 0, 0, 0, 0, 0, 0, {});
	map<string, Component*> components;
	cout << typeRule->getASTString(Lua) << endl;
	programState = State();
	return new EvaluationResult(typeRule->evaluateRule(programState, env, components));
}

template<typename Parser>
void testWithParser(string code, Parser&& parse, string language)
{
	string debugInfo = "language: " + language + "  code: " + code;
	EvaluationResult* sr = parseAndEvaluate(code, parse);
	if(!sr)
		FAIL() << "Error while parsing\n" << debugInfo;
}

void testECA(string ecaCode) {
	testWithParser(ecaCode, ParseECAMemory, "ECA");
}

void testLua(string luaCode){
	testWithParser(luaCode, ParseLuaMemory, "Lua");
}


void checkState(string varname, int check){
	ValueInt* vint = dynamic_cast<ValueInt*>(programState.lookup(varname));
	ASSERT_TRUE(vint);
	EXPECT_EQ(vint->getValue(), check) << varname << " is " << vint->getValue() << " in stead of " << check;
}

void not_exists(string varname){
	EXPECT_FALSE(programState.exists(varname));
}

TEST(Scoping, If) {
	testECA("x := 0; if y := 1 then z := 1; x := 42 else skip end, x");
	checkState("x", 42);
	not_exists("y");
	not_exists("z");

	testLua("x = 0 if true then x = 42 y = 10 end");
	checkState("x", 42);
	checkState("y", 10);
	testLua("local x = 0 if true then x = 42 end");
	checkState("x", 42);

	testLua("x = 0 if true then local x = 42 end");
	checkState("x", 0);
	testLua("local x = 0 if true then local x = 42 end");
	checkState("x", 0);
}

TEST(Scoping, If_wrong){
	EXPECT_THROW(testECA("if y := 1 then x := 42 else skip end, x"), runtime_error);
	EXPECT_THROW(testECA("if y := 1 then x := 42 else skip end, y"), runtime_error);
	EXPECT_THROW(testLua("if false then ; else local x = 42 end z = x"), runtime_error);
}

TEST(Scoping, While) {
	testECA("x := 0; while y := 0,x < 2 begin z := 0; x := x + 1 end");
	checkState("x", 2);
	not_exists("y");
	not_exists("z");

	testLua("local x = 0 while x == 0 do x = 1; local x = 42 local y = 3 z = 10 end");
	checkState("x", 1);
	checkState("z", 10);
	not_exists("y");
}

TEST(Scoping, CallFunction) {
	testECA("function f(x) "
			"   x := x + 10; "
			"   y := 42, "
			"	x "
			"end "
			"x := 0; "
			"z := f(1) ");
	checkState("x", 0);
	checkState("z", 11);
	not_exists("y");

	testLua("function f(arg) "
			"   arg = arg + glob "
			"   glob = glob + 1000 "
			"	local glob = 30 "
			"   arg = arg + glob "
			"   local arg = arg + 5000"
			"   newGlob = 1234 "
			"   local fLoc = 90"
			"	return arg "
			"end "
			"arg = 0 "
			"local l = 2 "
			"glob = 100 "
			"z = f(1) ");
	checkState("arg", 0);
	checkState("l", 2);
	checkState("glob", 1100);
	checkState("z", 5131);
	checkState("newGlob", 1234);
	not_exists("fLoc");
}

}

#include <gtest/gtest.h>
#include "LuaLexer.h"
#include "simplestring.h"

using namespace bitpowder::lib;
using namespace std;

namespace {

template<typename tokenType>
void testNext(LuaLexer &lexer, tokenType resultTokenValue){
    TokenBase *tb = lexer.next();
    ASSERT_EQ(tb->type, FastType<Token<tokenType>>::Value);
    //Token<LuaLexer::TOKEN> *t = FastType<Token<LuaLexer::TOKEN>>::Cast<Token<LuaLexer::TOKEN>>(token); //How to do this cast right?
    Token<tokenType> *t = dynamic_cast<Token<tokenType>*>(tb);
    //std::cout << "value of token:" << t->value << std::endl;
    EXPECT_EQ(t->value, resultTokenValue);
}

template<typename valueType>
void testNextValue(LuaLexer &lexer, valueType resultTokenValue){
    TokenBase *tb = lexer.next();
    cout << "FastType<Token<Value*>>::Value     : " << FastType<Token<Value*>>::Value << endl;
    cout << "tb->type                           : " << tb->type << endl;
    cout << "FastType<Token<valueType*>>::Value : " << FastType<Token<valueType*>>::Value << endl;
    cout << "FastType<Token<ValueInt*>>::Value  : " << FastType<Token<ValueInt*>>::Value << endl;
    //ASSERT_EQ(tb->type, FastType<Token<Value*>>::Value);
    Token<Value*>* t = dynamic_cast<Token<Value*>*>(tb);
    valueType* v = dynamic_cast<valueType*>(t->value);
    EXPECT_EQ(v->getValue(), resultTokenValue.getValue());
}

void expectEmpty(LuaLexer &lexer){
    auto tb = lexer.next();
    TokenBase* nullToken = nullptr;
    EXPECT_EQ(tb, nullToken);
}

TEST(LuaLexer, LexTokens) {
    MemoryPool mp;
    LuaLexer lexer("and or", mp);

    TokenBase *tb = lexer.next();
    EXPECT_EQ(tb->type, FastType<Token<Literal>>().Value);
    EXPECT_NE(tb->type, FastType<Token<String>>().Value);
    Token<Literal> *t = static_cast<Token<Literal>*>(tb);
    EXPECT_EQ(t->value, Literal::AND);
    testNext<Literal>(lexer, Literal::OR);

    expectEmpty(lexer);
}

TEST(LuaLexer, LexBool){
    MemoryPool mp;
    LuaLexer lexer("true", mp);
    testNextValue(lexer, ValueBool(true));
    expectEmpty(lexer);
}

TEST(LuaLexer, LexFloat){
    MemoryPool mp;
    LuaLexer lexer("2.01", mp);
    testNextValue(lexer, ValueFloat(2.01));
    expectEmpty(lexer);
}

TEST(LuaLexer, LexInt){
    MemoryPool mp;
    LuaLexer lexer("10 0x0f", mp);
    testNextValue(lexer, ValueInt(10));
    testNextValue(lexer, ValueInt(15));
    expectEmpty(lexer);
}

TEST(LuaLexer, LexString){
    MemoryPool mp;
    LuaLexer lexer("\"abc\\\\\\\"\"", mp);      // "abc\\\""
    testNextValue(lexer, ValueString("abc\\\""));     // abc\"        #
    expectEmpty(lexer);

    MemoryPool mp2;
    LuaLexer lexer2("\'abc\\\\\\\'\'", mp2);    // 'abc\\\''
    testNextValue(lexer2, ValueString("abc\\\'"));    // abc\'        #
    expectEmpty(lexer2);
}

TEST(LuaLexer, LexName){
    MemoryPool mp;
    LuaLexer lexer("my_var elseVar  else_var", mp);
    testNext<LuaName>(lexer, "my_var");
    testNext<LuaName>(lexer, "elseVar");
    testNext<LuaName>(lexer, "else_var");
    expectEmpty(lexer);
}

TEST(LuaLexer, LexIntThenName){
    MemoryPool mp;
    LuaLexer lexer("100name", mp);
    testNextValue(lexer, ValueInt(100));
    testNext<LuaName>(lexer, "name");
    expectEmpty(lexer);
}

TEST(LuaLexer, LexComment){
    MemoryPool mp;
    LuaLexer lexer("  ---[[one line comment\r\n -2--[[\r\nmulti-line comment\r\n---]]42  --?should not be parsed", mp); //TODO: make OS independent
    testNext<Literal>(lexer, Literal::MIN);
    testNextValue(lexer, ValueInt(2));
    testNextValue(lexer, ValueInt(42));
    expectEmpty(lexer);
}

TEST(LuaLexer, LexOps){
    MemoryPool mp;
    LuaLexer lexer("x+(1-2)", mp);
    testNext<LuaName>(lexer, "x");
    testNext<Literal>(lexer, Literal::PLUS);
    testNext<Literal>(lexer, Literal::BRACKET_OPEN);
    testNextValue(lexer, ValueInt(1));
    testNext<Literal>(lexer, Literal::MIN);
    testNextValue(lexer, ValueInt(2));
    testNext<Literal>(lexer, Literal::BRACKET_CLOSE);
    expectEmpty(lexer);
}

}
